name := "js-gentest"

version := "1.0"

scalaVersion := "2.11.12"

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")
scalacOptions += "-target:jvm-1.8"
javaOptions += "-Xmx1G"

//initialize := {
//  val _ = initialize.value
//  if (sys.props("java.specification.version") != "1.8")
//    sys.error("Java 8 is required for this project.")
//}

// https://mvnrepository.com/artifact/com.eclipsesource.j2v8/j2v8_win32_x86_64
val j2v8artifact = (sys.props.get("os.name").get.toLowerCase.split(" ").head, sys.props.get("os.arch").get.toLowerCase) match {
  case ("linux","amd64")|("linux","x86_64") => "j2v8_linux_x86_64"
  case ("mac","adm64")|("mac","x86_64") => "j2v8_macosx_x86_64"
  case ("windows","amd64")|("windows","x86_64") => "j2v8_win32_x86_64"
  case ("windows","x86") => "j2v8_win32_x86"
  case _ => throw new Exception("Find out which j2v8 you need and add yourself here")
}
libraryDependencies += "com.eclipsesource.j2v8" % j2v8artifact % "4.6.0"

// https://mvnrepository.com/artifact/com.eclipsesource.j2v8/j2v8
libraryDependencies += "com.eclipsesource.j2v8" % "j2v8" % "4.8.2"

// https://mvnrepository.com/artifact/org.scalatest/scalatest
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "3.2.0-SNAP7" % "test"

// https://mvnrepository.com/artifact/junit/junit
libraryDependencies += "junit" % "junit" % "4.10" % "test"

// https://mvnrepository.com/artifact/org.antlr/antlr4-runtime
libraryDependencies += "org.antlr" % "antlr4-runtime" % "4.7.1"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"

libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.5"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.12" // or whatever the latest version is
libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.5"