import vocabconfig.parser._
/**
 * Created by hila on 14/11/2017.
 */
class ASTNode (val func : String, val arity : Int, val optionalArgs : Int = 0) {
  override def equals(obj: scala.Any): Boolean = obj.getClass == this.getClass && obj.asInstanceOf[ASTNode].func == func && obj.asInstanceOf[ASTNode].arity == arity && children.zip(obj.asInstanceOf[ASTNode].children).forall(x => x._1 == x._2)
  override def hashCode(): Int = arity.## ^ func.##

  def shallowClone = new ASTNode(func,arity,optionalArgs)
  def deepClone : ASTNode= {
    val curr = shallowClone
    children.zipWithIndex.filter(!_._1.isEmpty).foreach(c => curr.addChild(c._2,c._1.get.deepClone))
    curr
  }
  def deepCloneUntil(node: ASTNode) : (Option[ASTNode],List[Int]) = if (node eq this) (None,Nil) else {
    val res = this.shallowClone
    var loc : List[Int] = Nil
    children.zipWithIndex.filter(!_._1.isEmpty).foreach{c =>
      val newChild = c._1.map(_.deepCloneUntil(node))
      if (!newChild.isEmpty) {
        res.children.update(c._2,newChild.get._1)
        if (newChild.get._1.isEmpty) loc = List(c._2)
        else if (!newChild.get._2.isEmpty) loc = c._2 +: newChild.get._2
      }
    }
    (Some(res),loc)
  }

  def getFirstIncomplete: Option[ASTNode] = ???


  val children : Array[Option[ASTNode]] = Array.fill[Option[ASTNode]](arity){None}
  def addChild(i: Int, node: ASTNode): Unit = {
    children(i) = Some(node)
  }

  def addAt(path: List[Int], node: ASTNode) : Unit = {
    if (!path.isEmpty) {
      getAt(path.take(path.length - 1)).get.addChild(path.last,node)
    }
  }

  def getAt(path: List[Int]) : Option[ASTNode] = if (path.isEmpty) Some(this)
        else if (path.head >= 0 && path.head < children.length) children(path.head).map(_.getAt(path.tail)).getOrElse(None)
        else None

  def removeChild(i: Int): Unit = {
    children(i) = None
  }

  def toCode : String = this.innerToCode(children)

  def innerToCode(children : Seq[Option[ASTNode]]) : String = {
    func + (children.dropRight(optionalArgs).map(c => c.map(_.toCode).getOrElse("?")) ++
            children.takeRight(optionalArgs).reverse.dropWhile(c => c.isEmpty).reverse.map(c => c.map(_.toCode).getOrElse("??"))
           ).mkString("(",",",")")
  }

  def toNodeList: Set[ASTNode] = children.flatMap(c => c.map(d => d.toNodeList)).flatten.toSet + shallowClone
  def toListOfFullNodes : List[ASTNode] = children.flatMap(c => c.map(d => d.toListOfFullNodes)).flatten.toList :+ deepClone
  def innerWalkAllNodes() : Iterable[ASTNode] = children.flatMap(n => n.map(_.innerWalkAllNodes()).getOrElse(Iterable.empty)) ++ Iterable(this)
  def walkAllNodes() : Iterable[ASTNode] = innerWalkAllNodes().toList.zipWithIndex.sortWith(
    (n1,n2) => if (n1._1.height == n2._1.height) n1._2 < n2._2 else n1._1.height < n2._1.height).map(_._1)

  def isFull : Boolean = children.forall(c => !c.isEmpty && c.get.isFull)
  def height : Int = 1 + (if (children.isEmpty) 0 else children.map(c => c.map(_.height).getOrElse(0)).max)
  def heightNoLambdas : Int = 1 + (if (children.isEmpty) 0 else children.map(c => c.map(_.heightNoLambdas).getOrElse(0)).max)

  def usesLiteral(v: String): Boolean = children.exists(c => !c.isEmpty && c.get.usesLiteral(v))

  def accumulateContextVars(alongPath : List[Int]) : Seq[String] = if (alongPath.size < 2) Nil
                                                                   else children(alongPath.head).get.accumulateContextVars(alongPath.tail)
  def contains(subtree: ASTNode): Boolean = {
    if (subtree.isInstanceOf[Literal]) usesLiteral(subtree.func)
    else {
      val thisHeight = this.height
      val subtreeHeight = subtree.height
      if (thisHeight < subtreeHeight) false
      else if (thisHeight == subtreeHeight) this.toCode == subtree.toCode
      else children.exists(c => !c.isEmpty && c.get.contains(subtree))
    }
  }
}

object ASTNode {
  def parseList(vocabText: String) : List[ASTNode] = vocabText.split("\\r\\n|\\n").map(ASTNode(_)).toList

  def fromFile(path : String) = parseList(scala.io.Source.fromFile(path).getLines().mkString("\n"))

  val trenOpRegex = "\\?\\s*\\?\\s*\\?\\s*:\\s*\\?".r
  val macroRegex = raw"MACRO\s+([a-zA-Z][a-zA-Z0-9]*)\s*=>\s*(.*)".r

  def apply(stringRep : String) : ASTNode = stringRep match {
    case trenOpRegex() => TernaryOperator()
    case macroRegex(macroName, macroBody) => ASTMacro(macroName,macroBody.count(_ == '?'),macroBody.split('?').toList)
    case _ => {
      val parsed = VocabParser(stringRep)
      if (parsed.isLeft)
        throw new Exception(stringRep + ":" + parsed.left.get.toString)

      parsed.right.get match {
        case LiteralNode(str) => Literal(str)
        case NewNode(clsName, args) => Constructor(clsName, args.params.length, args.params.count(p => p.isInstanceOf[OptParamNode] || p.isInstanceOf[OptCallbackParamNode]))
        case FuncNode(fname, args) => new ASTNode(fname, args.params.length, args.params.count(_.isInstanceOf[OptParamNode]))
        case McallNode(fname, args) => {
          val argCount = args.params.length
          val optArgs = args.params.count(p => p.isInstanceOf[OptParamNode] || p.isInstanceOf[OptCallbackParamNode])
          if (args.params.exists(p => p.isInstanceOf[CallbackParamNode] || p.isInstanceOf[OptCallbackParamNode])) {
            val cbackIdx = args.params.indexWhere(p => p.isInstanceOf[CallbackParamNode] || p.isInstanceOf[OptCallbackParamNode])
            val cbackArgCount = args.params(cbackIdx) match {
              case CallbackParamNode(args) => args.params.length
              case OptCallbackParamNode(args) => args.params.length
            }
            val cbackOptionalsCount = (args.params(cbackIdx) match {
              case CallbackParamNode(args) => args.params
              case OptCallbackParamNode(args) => args.params
            }).count(p => p.isInstanceOf[OptParamNode] || p.isInstanceOf[OptCallbackParamNode])
            AggregatorMethod(fname, argCount,cbackArgCount,cbackIdx, optArgs, cbackOptionalsCount)
          }
          else
            new MethodCall(fname, argCount, optArgs)
        }
        case BinopNode(op) => BinOperator(op)
        case PrefopNode(op) => UnPrefOperator(op)
        case PostopNode(op) => UnPostOperator(op)
        case ArrDerefNode() => ArrayDeref()
        case PropCallNode(propName) => PropCall(propName)
        case ObjInitNode(numPairs) => ObjectInitializer(numPairs)
      }
    }
  }
}

class MethodCall(func : String, arty : Int, optional : Int = 0) extends ASTNode(func,arty + 1, optional){
  override def shallowClone = new MethodCall(func,arty,optionalArgs)

  override def toCode = children.head.map(n => if (n.isInstanceOf[Literal]) n.toCode else "(" + n.toCode + ")").getOrElse("?") + "." + super.innerToCode(children.drop(1))

  def lhs = children.head
}

class Literal (value : String) extends ASTNode(value, 0){
  override def toCode = func
  override def shallowClone = new Literal(value)

  override def isFull = true
  override def usesLiteral(v: String): Boolean = func == v

}

object Literal {
  def apply(value : String) = new Literal(value)
}

class BinOperator(value : String) extends ASTNode(value,2) {
  override def toCode = children(0).map(n => if (n.isInstanceOf[Literal] || n.isInstanceOf[ObjectInitializer]) n.toCode else "(" + n.toCode + ")").getOrElse("?") +
    " " + value + " " + children(1).map(n => if (n.isInstanceOf[Literal] || n.isInstanceOf[ObjectInitializer]) n.toCode else "(" + n.toCode + ")").getOrElse("?")

  override def shallowClone = new BinOperator(value)
}

object BinOperator{
  def apply(value : String) = new BinOperator(value)
}

class TernaryOperator() extends ASTNode("?:",3) {
  override def toCode = {
    val strs = children.map(child => child.map(n => if (n.isInstanceOf[Literal]) n.toCode else "(" + n.toCode + ")").getOrElse("?"))
    strs(0) + " ? " + strs(1) + " : " + strs(2)
  }
  override def shallowClone = new TernaryOperator()
}

object TernaryOperator{
  def apply() = new TernaryOperator
}

class UnPrefOperator(value : String) extends ASTNode(value,1) {
  override def shallowClone = new UnPrefOperator(value)
}
object UnPrefOperator {
  def apply(value : String) = new UnPrefOperator(value)
}

class UnPostOperator(value : String) extends ASTNode(value,1) {
  override def toCode = "(" + children(0).map(_.toCode).getOrElse("?") + ")" + value
  override def shallowClone = new UnPostOperator(value)
}
object UnPostOperator {
  def apply(value : String) = new UnPostOperator(value)
}

object ArrayDeref{
  def apply() = new ArrayDeref()
}

class ArrayDeref extends ASTNode("[]",2) {
  def lhs = children.head
  def rhs = children.last

  override def shallowClone = new ArrayDeref()
  override def toCode = children.head.map(n => if (n.isInstanceOf[Literal]) n.toCode else "(" + n.toCode + ")").getOrElse("?") + "[" +  children.last.map(_.toCode).getOrElse("?") + "]"

}

class PropCall(propName : String) extends ASTNode(propName,1) {
  def lhs = children.head
  override def shallowClone = new PropCall(propName)
  override def toCode = children.head.map(n => if (n.isInstanceOf[Literal]) n.toCode else "(" + n.toCode + ")").getOrElse("?") + "." + propName
}

object PropCall {
  def apply(propName: String): PropCall = new PropCall(propName)
}

class AggregatorMethod(func : String, args : Int, optional : Int, val callbackIdx : Int, val innerVar : List[String], val innerOptionals : Int = 0) extends MethodCall(func,args,optional) {
  override def shallowClone = new AggregatorMethod(func,args,optional,callbackIdx,innerVar,innerOptionals)
  override def toCode =
    children.head.map(n => if (n.isInstanceOf[Literal]) n.toCode else "(" + n.toCode + ")").getOrElse("?") + "." +
    func + "(" +
      (if (!children(1).isEmpty || arity - optional > 1) makeInnerVarSigStr + " => " + children(1).map(_.toCode).getOrElse("?")
      else "") +
      children.take(Math.max(arity - optionalArgs, children.lastIndexWhere(!_.isEmpty) + 1)).drop(2).map(c => "," + c.map(_.toCode).getOrElse("?")).mkString("") +
    ")"

  override def heightNoLambdas: Int = 1 + Math.max(children.head.map(_.heightNoLambdas).getOrElse(0),
                                          Math.max(children(1).map(_ => 1).getOrElse(0),
                                                   if (children.length > 2) children.drop(2).map(c => c.map(_.height).getOrElse(0)).max else 0))

  def makeInnerVarSigStr = {
    val appearingChildren : List[String] = innerVar.take(innerVar.length - innerOptionals + requiredOptionals)
    appearingChildren.length match {
      case 1 => appearingChildren.head.toString
      case _ => appearingChildren.mkString("(",",",")")
    }
  }

  private def requiredOptionals : Int = innerVar.takeRight(innerOptionals).lastIndexWhere(v => children(callbackIdx).map(_.usesLiteral(v)).getOrElse(false)) + 1

  override def toNodeList: Set[ASTNode] = super.toNodeList.filterNot(n => n.isInstanceOf[Literal] && innerVar.contains(n.func))
  override def accumulateContextVars(alongPath : List[Int]) : Seq[String] = (if (alongPath.head == callbackIdx) innerVar else Nil) ++ super.accumulateContextVars(alongPath)
}
object AggregatorMethod {
  def apply(func : String, args : Int, innerVarNum : Int, callbackIdx : Int, optional : Int, innerOptional : Int) : AggregatorMethod = new AggregatorMethod(func,args,optional,callbackIdx + 1,AggregatorMethod.makeInnerVars(innerVarNum),innerOptional)
  def apply(func : String, args : Int, innerVars : List[String], callbackIdx : Int = 0, optional : Int = 0, innerOptional : Int = 0) : AggregatorMethod = new AggregatorMethod(func,args, optional,callbackIdx + 1, innerVars)
  def apply(func : String, args : Int, innerVarNum : Int) : AggregatorMethod = apply(func,args,innerVarNum,0,0,0)
  var count = 0
  def freshVariable() : String = {
    count = count+1
    "x" + count.toString()
  }

  def makeInnerVars(numInnerVars : Int)  : List[String] = (0 until numInnerVars).map(i => freshVariable()).toList
}

class Constructor(className : String, arity : Int, optional : Int) extends ASTNode(className,arity, optional) {
  override def shallowClone: ASTNode = new Constructor(className,arity,optional)

  override def toCode: String = "new " + (arity match {
    case 0 => className
    case _ => super.toCode
  })
}

object Constructor {
  def apply(className : String, arity : Int, optional : Int = 0) = new Constructor(className,arity, optional)
}

class ObjectInitializer(arity : Int) extends ASTNode("{}",arity) {
  override def shallowClone: ASTNode = new ObjectInitializer(arity)

  override def toCode: String = "{" + children.sliding(2,2).map(pair => pair(0).map(_.toCode).getOrElse("?") + ":" + pair(1).map(_.toCode).getOrElse("?")).mkString(",") + "}"
}

object ObjectInitializer{
  def apply(numPairs : Int) = new ObjectInitializer(2 * numPairs)
}

class ArrayInitializer(arity : Int) extends ASTNode("[,]", arity) {
  override def shallowClone: ASTNode = new ArrayInitializer(arity)

  override def toCode: String = this.children.map(c => c.map(_.toCode).getOrElse("?")).mkString("[",",","]")
}

object ArrayInitializer{
  def apply(arity : Int) = new ArrayInitializer(arity)
}

class ASTMacro(macroName: String, arity: Int, val codeBits : List[String]) extends ASTNode(macroName,arity){
  assert(codeBits.length == arity + 1)

  override def shallowClone: ASTNode = new ASTMacro(macroName,arity,codeBits)

  override def toCode : String = codeBits.zip(this.children.map(c => c.map(n => if (n.isInstanceOf[Literal]) n.toCode else "(" + n.toCode + ")").getOrElse("?"))).map(pair => pair._1 + pair._2).mkString("") + codeBits.last
}

object ASTMacro{
  def apply(macroName: String, arity: Int, codeBits : List[String]) = new ASTMacro(macroName,arity,codeBits)
}