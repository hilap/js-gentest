import ConstraintFileReader.{ConstraintKey, ConstraintValue}
import DataType.DataType

import scala.collection.mutable

/**
 * Created by hila on 09/03/2018.
 */
class AggregatorParamsIter(
                            val funcName : String,
                            val allPrev : List[Enumerator.SeenProgram],
                            newVocab: List[ASTNode],
                            val rp: ResetInputsRetainPolicy,
                            val executor : Executor,
                            val numArgs : Int,
                            val optionalArgs : Int,
                            val prevLevelHeight : Int,
                            val nspPreds: List[ProgramPredicate],
                            val rhsTypeConstraints : List[Set[DataType.DataType]],
                            val lambdaConstraints : Map[ConstraintKey,ConstraintValue]
                          ) extends ParamsIter {
  override def toString(): String = "AggregatorParamsIter"
  val lhsIterator = allPrev.iterator
  var lhs : ASTNode = null
  var lhsValues : List[JSArrayWrapper] = null
  var rhs : Iterator[List[ASTNode]] = Iterator.empty
  var currRhs : List[ASTNode] = Nil
  var lambda : Option[ASTNode] = None

  var nxt : Option[List[ASTNode]] = None
  var done = false
  var enumerator : Enumerator = changeLhs()

  def changeLhs() = {
    var enum : Enumerator = null
    lambda = None
    lhs = null
    rhs = new SimpleParamsIter((1 until numArgs - 1).map(_ => allPrev).toList,Math.min(optionalArgs,numArgs - 1), 0,rhsTypeConstraints)
    currRhs = if (rhs.hasNext) rhs.next() else Nil
    while (null == lhs && lhsIterator.hasNext) {
      val l = lhsIterator.next()
      if (l.types.size == 1 && l.types.contains(DataType.array)) {
        val lhs_vals = executor.execute(l.prog)
        //TODO: this should maybe be exists one nonempty...
        if (lhs_vals.allNonempty && lhs_vals.get.forall(lhs_val => lhs_val.isInstanceOf[JSArrayWrapper] && lhs_val.asInstanceOf[JSArrayWrapper].jsonifiedValue != "[]")) {
          lhs = l.prog
          lhsValues = lhs_vals.get.map(v => v.asInstanceOf[JSArrayWrapper])
          rp.reset(AggregatorParamsIter.getCallbackValues(
            lhsValues,
            rp.outerVarsVals,
            rp.outerVars,
            funcName,
            rp.innerVars.length,
            if (numArgs > 2) currRhs else Nil
          ))
          enum = Enumerator(newVocab, rp, 3, nspPreds, Nil, lambdaConstraints)
        }
        //lhs_vals.release()
      }
    }
    if (null == lhs)
      nxt = None
    else if (optionalArgs == numArgs - 1) //If the lambda is also optional
      nxt = Some(List(lhs))
    else
      nxt = None
    enum
  }

  def changeRhs() : Enumerator = {
    currRhs = rhs.next()
    rp.reset(AggregatorParamsIter.getCallbackValues(
      lhsValues,
      rp.outerVarsVals,
      rp.outerVars,
      funcName,
      rp.innerVars.length,
      if (numArgs > 2) currRhs else Nil
    ))
    Enumerator(newVocab, rp, 3, nspPreds, Nil, lambdaConstraints)
  }


  override def hasNext: Boolean = {
    if (done) return false
    if (!nxt.isEmpty) return true
    nxt = do_next()
    if (nxt.isEmpty) done = true
    !nxt.isEmpty
  }
  override def next(): List[ASTNode] = {
    if (nxt.isEmpty) {
      nxt = do_next()
      if (!nxt.isEmpty) {
        val res = nxt.get
        nxt = None
        res
      }
      else throw new NoSuchElementException("next on empty iterator")
    }
    else {
      val res = nxt.get
      nxt = None
      res
    }
  }

  def do_next() : Option[List[ASTNode]] = {
    var res: Option[List[ASTNode]] = None
    while (res.isEmpty && !done) {
      if (enumerator == null)
        return None
      if (numArgs > 2) {
//        if (lambda.isEmpty && enumerator.hasNext) {
//          lambda = Some(enumerator.next())
//        }
//        if (rhs.hasNext) {
//          res = Some(List(lhs, lambda.get) ++ rhs.next())
//        }
//        else if (enumerator.hasNext){
//          lambda = Some(enumerator.next())
//          rhs = new SimpleParamsIter((1 until numArgs - 1).map(_ => allPrev).toList,Math.min(optionalArgs,numArgs - 1), 0,rhsTypeConstraints)
//        }
//        else {
//          lambda = None
//          enumerator = changeLevel()
//          if (enumerator != null && enumerator.hasNext) {
//            lambda = Some(enumerator.next())
//            rhs = new SimpleParamsIter((1 until numArgs - 1).map(_ => allPrev).toList,Math.min(optionalArgs,numArgs - 1), 0,rhsTypeConstraints)
//          }
//        }
        if (enumerator.hasNext) {
          val lambda = enumerator.next()
          res = Some(List(lhs,lambda) ++ currRhs)
        }
        else if (rhs.hasNext){
          enumerator = changeRhs()
        }
        else {
          enumerator = changeLhs()
        }
      }
      else {
        if (enumerator.hasNext) {
          lambda = Some(enumerator.next())
          res = Some(List(lhs,lambda.get))
        }
        else {
          if (enumerator != null) enumerator.releaseValues()
          enumerator = changeLhs()
          res = if (enumerator != null && enumerator.hasNext)
            Some(List(lhs,enumerator.next()))
          else None
        }
      }
      if (!res.isEmpty && !res.get.isEmpty) {
        val h = Math.max(res.get.head.heightNoLambdas, if (res.get.length > 2) res.get.drop(2).map(_.heightNoLambdas).max else 0) //don't care about the lambda
        if (h < prevLevelHeight) {
          res = None
        }
      }
    }
    res

  }

  def releaseValues() : Unit = {
    if (enumerator != null) enumerator.releaseValues()
    rp.releaseValues()
  }
}

object AggregatorParamsIter{
  def getCallbackValues(lhs_vals : List[JSArrayWrapper], lhs_inputs : List[VarInputs], varNames : List[String], funcName : String, varNum : Int, additionalArgs : Seq[ASTNode] = Nil) : List[VarInputs] = {
    val varValuesWithLhs = lhs_vals.zipWithIndex.map(arrWrapper => VarInputs(List(arrWrapper._1.toCode) ++ lhs_inputs(arrWrapper._2).inputs))
    val accumulate_script = "out = " + (0 until varNum).map(_ => "[]").mkString("[",",","]") + ";\n" +
    "lhs." + funcName + "( function (" + (0 until varNum).map("x" + _).mkString(",") + ") {" +
      (0 until varNum).map(i => "if (x" + i.toString + " !== undefined) out[" + i.toString + "].push("+ExecUtils.resultWrapper("x"+i)+");").mkString("\n") +
    "}" + additionalArgs.map(a => "," + a.toCode).mkString("") + ");\n"

    val values_wrapped = V8Runtime.runArrayAccumulator(accumulate_script,List("lhs") ++ varNames,varValuesWithLhs,varNum,"out")
    lhs_inputs.zip(values_wrapped).flatMap{case (lhs,wrapped) => wrapped.map(w => lhs.addInputs(w.map(_.toCode)))}
  }
}