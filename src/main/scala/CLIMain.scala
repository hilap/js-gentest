import SingleSubtreeSnipMain.args

import scala.collection.mutable

object CLIMain extends App{
  val inputOutputs = MainUtils.readIoFile(args(0))
  val vocab = ASTNode.fromFile(args(1))
  val prog : ASTNode = if (args.length >= 3) JSParser.deconstruct(scala.io.Source.fromFile(args(2)).mkString) else Literal("input")

def doMain(vocab : List[ASTNode], inputOutputs : List[(String,String)] = Nil, initialProgram : ASTNode = Literal("input"))  = {
  var prog = initialProgram
  var (inputs,outexec) = {
    val executorNoVal = new ExecutorNoVal
    (inputOutputs.map(_._1),inputOutputs.map(io => executorNoVal.execute(io._2).get.head))
  }
  var executor : ExecutorWithVal = null;
  def updateExecutor() : Unit = {
    executor = new ExecutorWithVal(inputs.map(x => VarInputs(List(x))))
  }
  updateExecutor()
  val retains : mutable.MutableList[ASTNode] = mutable.MutableList()
  var removeSubtree : Option[List[Int]] = None

  def setProg(node: ASTNode) = {
    prog = node
    removeSubtree = None
  }

  def printMiniUsage() = println("p/r/i/h/a/?")

  def printUsage() = {
    println("To rewrite program: p")
    println("To retain: r")
    println("To add input: i")
    println("To create a hole: h")
    println("To run active repair: a")
    println("To print help menu: ?")
    //printMiniUsage()
  }

  printUsage()

  def doNewProgram() = {
    println("Enter program:")
    setProg(JSParser.deconstruct(Console.in.readLine()))

    val res = executor.execute(prog)
    Range(0,inputs.size).foreach{ i =>
      val input = inputs(i)
      val execResult = res.items(i)
      val execOut = outexec(i)
      print(Console.YELLOW + input + "\t")
      if (execResult.isEmpty) print(Console.RED + "<ERROR>" + Console.GREEN + " (" + execOut.value + ")")
      else if (execResult.get == execOut) print(Console.GREEN + execOut.value)
      else print(Console.RED + execResult.get.value + Console.GREEN + " (" + execOut.value + ")")
      println(Console.RESET)
    }
  }

  def doAddInput() = {
    println("Enter input:")
    val input = Console.in.readLine()

    val newExec = new ExecutorWithVal(List(VarInputs(List(input))))
    val res = newExec.execute(prog)
    if (res.emptyOrUndefined()) {
      println(Console.RED + "Error on input" + Console.RESET)
    }
    else {
      println(Console.YELLOW + input + "\t" + Console.GREEN + res.get.head.value + Console.RESET)
      println("Desired output? (y/n)")
      val desiredOutput = Console.in.readLine() match {
        case "y" => res.get.head
        case "n" => {
          println("Enter output:")
          val out = Console.in.readLine()
          val executorNoVal = new ExecutorNoVal
          val res = executorNoVal.execute(out)
          res.get.head
        }
      }
      inputs = inputs :+ input
      outexec = outexec :+ desiredOutput
      updateExecutor()
    }
  }

  def makeAllPaths(tree: ASTNode) : List[List[Int]] =
    if (tree.children.isEmpty) List(List())
    else tree.children.zipWithIndex.filter(!_._1.isEmpty).flatMap(c => makeAllPaths(c._1.get).map(l => c._2 +: l)).toList :+ List()

  def doAddRetain() = {
    println("Select number of subtree to retain:")
    val paths : List[List[Int]] = makeAllPaths(prog)
    paths.zipWithIndex.foreach { path =>
      println(path._2 + ")" + prog.getAt(path._1).get.toCode)
    }
    val selected = Console.in.readLine().toInt
    retains += prog.getAt(paths(selected)).get
    println("Retaining: ")
    retains.foreach(r => println(r.toCode))
  }

  def doMakeHole() = {
    println("Select number of subtree to re-code:")
    val paths : List[List[Int]] = makeAllPaths(prog)
    paths.zipWithIndex.foreach { path =>
      println(path._2 + ")" + prog.getAt(path._1).get.toCode)
    }
    val selected = Console.in.readLine().toInt
    removeSubtree = Some(paths(selected))
  }

  def getRetainPaths(): List[List[Int]] = {
    val retainCodes = retains.map(r => r.toCode)
    makeAllPaths(prog).filter(path => retainCodes.contains(prog.getAt(path).get.toCode))
  }

  while (true) {
    println("Program: " + Console.BLUE + removeSubtree.map{s =>
      if (s.isEmpty) "?"
      else {
        val remProg = prog.deepClone
        remProg.getAt(s.dropRight(1)).get.removeChild(s.last)
        remProg.toCode
      }
    }.getOrElse(prog.toCode) + Console.RESET)
    if (!retains.isEmpty) {
      println("Retaining:")
      retains.foreach(r => println("\t" + Console.GREEN + r.toCode + Console.RESET))
    }
    printMiniUsage()
    val inLine = Console.in.readLine()
    inLine match {
      case "p" => doNewProgram()
      case "r" => doAddRetain()
      case "i" => doAddInput()
      case "h" => doMakeHole()
      case "a" => {
        val newProg = SingleSubtreeSnipMain.doSynthesize(/*executor,outexec*/inputs,inputOutputs.map(e => e._2),vocab,Map.empty,prog,removeSubtree.get,getRetainPaths())
        if (newProg.isEmpty) {
          println(Console.RED + "No program found." + Console.RESET)
          setProg(prog)
          retains.clear()
        }
        else setProg(newProg.get)
      }
      case "?" => printUsage()
      case _ => {}
    }
  }
}
  doMain(vocab,inputOutputs,prog)
}
