object ConstraintFileReader {
  def readFile(path: String) = processAll(scala.io.Source.fromFile(path).getLines())


  case class ConstraintKey(func: String, nodeType: String, arity: Int)
  case class ConstraintValue(args : List[Set[DataType.DataType]])
  def process(line: String) = try {
    //func;nodeType;arity;argIdx;types csl
    val fields = line.split(";")
    val types = DataType.values.filter(d => fields(4).split(",").toSet.contains(d.toString))
    (ConstraintKey(fields(0),fields(1),fields(2).toInt) -> (fields(3).toInt,types.toSet))
  }
  catch {
    case e : Exception => throw new Exception("Malformed constraint line: " + line, e)
  }

  def processAll(fileData: String) : Map[ConstraintKey,ConstraintValue] = processAll(fileData.lines)
  def processAll(fileData: Iterator[String]) : Map[ConstraintKey,ConstraintValue] = {
    val lines = fileData.filter(x => !x.isEmpty && !x.startsWith("#")).map(l => process(l)).toList
    val byKey = lines.groupBy(kv => kv._1)
    byKey.mapValues{ valuesList =>
      val argsMap = valuesList.map(_._2).toMap
      val arity = valuesList.head._1.arity
      ConstraintValue((for (i <- 0 until arity) yield argsMap.getOrElse(i,Set())).toList)
    }
  }
}
