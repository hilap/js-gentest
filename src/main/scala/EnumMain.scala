import com.eclipsesource.v8.{V8, V8Array, V8Object, V8Value}
import scala.util.matching.Regex

/**
 * Created by hila on 14/11/2017.
 */

object EnumMain extends App{

  def enumVocabOverIO(vocab : List[ASTNode], inputOutputs : List[(String,String)]): (Option[String],Int) = {
    val executorVal = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    val outputs: List[FromJSWrapper] = outexec
    outexec.foreach(e => if (e.isInstanceOf[V8Value]) e.asInstanceOf[V8Value].release())
    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(executorVal,Nil))
    var progsSeen = 0
    while (enumerator.hasNext) {
      val next = enumerator.next()
      progsSeen = progsSeen + 1
      //if (next.toCode.startsWith("input.filter(")) println(next.toCode)
      if (progsSeen % 100 == 0) {
        println(java.util.Calendar.getInstance().getTime + ": " + progsSeen)
        println(next.toCode)
      }
      val execed = executorVal.execute(next)
      val res: List[FromJSWrapper] = execed.get
      if (res == outputs) {
        enumerator.releaseValues()
        //outputs.foreach(o => o.release())
        execed.release()
        V8Runtime.release()
        //System.exit(0)
        return (Some(next.toCode),progsSeen)
      }
      execed.release()

    }
    return (None,progsSeen)
  }

  val inputOutputs = MainUtils.readIoFile(args(0))
  val vocab = ASTNode.fromFile(args(1))
  val (prog, seen) = enumVocabOverIO(vocab,inputOutputs)
  println(prog.map(x => "Program found: " + x).getOrElse("No programFound"))
  println("Programs seen: " + seen)
}
