import ConstraintFileReader.{ConstraintKey, ConstraintValue}
import DataType.DataType

import scala.collection.mutable

/**
 * Created by hila on 06/12/2017.
 */

trait RetainPolicy {
  def shouldAdd(expr: ASTNode): (Boolean,Set[DataType.DataType])
  def releaseValues() : Unit
}

class Enumerator (
                   val vocab : List[ASTNode],
                   val retainPolicy: RetainPolicy,
                   val maxLevels : Int = Int.MaxValue,
                   root : Option[ASTNode] = None,
                   path : List[Int] = Nil,
                   nspPreds : List[ProgramPredicate]= Nil,
                   nonNspPreds : List[ProgramPredicate] = Nil,
                   val constraints : Map[ConstraintKey,ConstraintValue] = Map.empty
                 ) extends Iterator[ASTNode]{
  override def toString(): String = "Enumerator for " + vocab.map(f => f.func)

  val (leaves, nodes) = {
    val (l,n) = vocab.distinct.partition(n => n.isFull)
    (l.sortBy(n => -n.height),n.sortBy(n => if (n.isInstanceOf[AggregatorMethod]) 1 else 0))
  }
 // val nodes = vocab.filter(_.arity != 0)

  import Enumerator.SeenProgram
  val allPrev : mutable.MutableList[SeenProgram] = mutable.MutableList()
  //var immediatePrev: mutable.MutableList[ASTNode] = mutable.MutableList()
  var curr: mutable.MutableList[SeenProgram] = mutable.MutableList()
  var rootIterator = leaves.iterator
  //var prevIterator = immediatePrev.iterator
  //var allPrevIter = allPrev.iterator
  var currRoot: ASTNode = rootIterator.next()
  var paramsIter : Iterator[List[ASTNode]] = Iterator.empty

  def resetParamIter(constraints : List[Set[DataType.DataType]]) : Unit = {
    if (paramsIter.isInstanceOf[AggregatorParamsIter])
      paramsIter.asInstanceOf[AggregatorParamsIter].releaseValues()
    if (currRoot.isInstanceOf[AggregatorMethod]) {
      val newVocab = vocab.filterNot(n => n.isInstanceOf[AggregatorMethod]) ++ currRoot.asInstanceOf[AggregatorMethod].innerVar.map(v => Literal(v))
      val prevExecutor = if (retainPolicy.isInstanceOf[ValuesRetainPolicy])
          retainPolicy.asInstanceOf[ValuesRetainPolicy].executor
        else new ExecutorNoVal()
      val rp = if (prevExecutor.isInstanceOf[ExecutorWithVal]) {
        new ResetInputsRetainPolicy(currRoot.asInstanceOf[AggregatorMethod].innerVar, prevExecutor.asInstanceOf[ExecutorWithVal].varNames , prevExecutor.asInstanceOf[ExecutorWithVal].inputVals, nonNspPreds)
      }
      else
      {
        new ResetInputsRetainPolicy(currRoot.asInstanceOf[AggregatorMethod].innerVar, List("input") , List(VarInputs(List("null"))),nonNspPreds)
      }
      //val newEnum = new Enumerator(newVocab, rp ,3)

      paramsIter = new AggregatorParamsIter(
        currRoot.func,
        allPrev.toList,
        newVocab,
        rp,
        prevExecutor,
        currRoot.arity,
        currRoot.optionalArgs,
        currLevel,
        nspPreds,
        constraints.drop(2),
        this.constraints //for child enumerator
      )//,inputValues)
    }
    else
      paramsIter = new SimpleParamsIter((0 until (currRoot.arity)).map(x => allPrev).toList, currRoot.optionalArgs, currLevel,constraints)
  }

  var nxt : Option[ASTNode] = None
  var done = false
  override def hasNext: Boolean = {
    if (!nxt.isEmpty) return true
    if (done) return false
    nxt = do_next()
    if (nxt.isEmpty) done = true
    !nxt.isEmpty
  }
  override def next(): ASTNode = {
    if (nxt.isEmpty) {
      nxt = do_next()
      if (!nxt.isEmpty) {
        val res = nxt.get
        nxt = None
        res
      }
      else throw new NoSuchElementException("next on empty iterator")
    }
    else {
      val res = nxt.get
      nxt = None
      res
    }
  }

  def getConstraintsOrEmpty = constraints.getOrElse(
    ConstraintKey(currRoot.func,currRoot.getClass.getSimpleName,currRoot.arity),
    ConstraintValue((0 until currRoot.arity).map(_ => Set.empty[DataType.DataType]).toList)
  )

  def do_next() : Option[ASTNode] = {
    var retVal : Option[ASTNode] = None
    while (retVal.isEmpty && !done) {
      if (!currRoot.isFull) {
        if (!paramsIter.hasNext) {
          if (!rootIterator.hasNext) changeLevel()
          if (done || !rootIterator.hasNext) return None
          while(rootIterator.hasNext && !paramsIter.hasNext) {
            currRoot = rootIterator.next()
            resetParamIter(getConstraintsOrEmpty.args)
          }
          if (!paramsIter.hasNext) return None
        }
        val headNode = currRoot.shallowClone
        paramsIter.next.zipWithIndex.foreach(rrr => headNode.addChild(rrr._2, rrr._1))
        val ret = if (root.isEmpty) headNode else {
          val r = root.get.deepClone
          r.addAt(path,headNode)
          r
        }
        if (nspPreds.forall(p => p(ret))) {
          val (should,types) = retainPolicy.shouldAdd(ret)
          if (should) {
            curr += SeenProgram(headNode, types)
            retVal = Some(ret)
          }
        }
      }
      else {
        //first iter
        val res = if (root.isEmpty) currRoot else {
          val r = root.get.deepClone
          r.addAt(path,currRoot)
          r
        }
        if (nspPreds.forall(p => p(res))){
          val (should,types) = retainPolicy.shouldAdd(res)
          if (should) {
            retVal = Some(res)
            curr += SeenProgram(currRoot, types)
          }
        }
        if (!rootIterator.hasNext) changeLevel() //no (done || ...) here because we're prefetching
        if (rootIterator.hasNext) {
          currRoot = rootIterator.next()
          if (!currRoot.isFull) resetParamIter(getConstraintsOrEmpty.args)
        }
        else {
          done = true
        }
      }
    }
    retVal
  }

  var currLevel = 0
  def changeLevel() : Unit = {
    if (curr.isEmpty) {
      done = true
    }
    allPrev ++= curr
    //immediatePrev = curr
    curr = mutable.MutableList()
    rootIterator = nodes.iterator
    //prevIterator = immediatePrev.iterator
    //allPrevIter = allPrev.iterator
    currLevel = currLevel + 1
    if (currLevel >= maxLevels) {
      done = true
    }
  }

  def releaseValues() : Unit = {
    retainPolicy.releaseValues()
    if (paramsIter.isInstanceOf[AggregatorParamsIter])
      paramsIter.asInstanceOf[AggregatorParamsIter].releaseValues()
  }

}

object Enumerator {
  def apply(vocabulary: List[ASTNode], retainPolicy: RetainPolicy) = new Enumerator(vocabulary, retainPolicy)
  def apply(vocabulary: List[ASTNode], retainPolicy: RetainPolicy, maxLevels : Int) = new Enumerator(vocabulary, retainPolicy,maxLevels)
  def apply(vocabulary: List[ASTNode], retainPolicy: RetainPolicy, maxLevels : Int, nspPredicates : List[ProgramPredicate], nonNspPredicates: List[ProgramPredicate] = Nil) = new Enumerator(vocabulary,retainPolicy,maxLevels, nspPreds = nspPredicates, nonNspPreds = nonNspPredicates)
  def apply(vocabulary: List[ASTNode], retainPolicy: RetainPolicy, maxLevels : Int, nspPredicates : List[ProgramPredicate], nonNspPredicates: List[ProgramPredicate], constraints: Map[ConstraintKey,ConstraintValue]) = new Enumerator(vocabulary,retainPolicy,maxLevels, nspPreds = nspPredicates, nonNspPreds = nonNspPredicates,  constraints = constraints)

  case class SeenProgram(prog : ASTNode, types : Set[DataType.DataType])
}
