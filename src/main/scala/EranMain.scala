object EranMain extends App{
  //SingleSubtreeSnipMain.doMain(
    /************ KATA 1 ****************/
//    ASTNode.parseList("""new Set(?)
//                        |Number(?)
//                        |?.sort()
//                        |?.indexOf(?)
//                        |?.toString()
//                        |? == ?
//                        |? + ?
//                        |input""".stripMargin),
//    MainUtils.readIoFile("src/test/resources/io2"),
//    "input.reduce(((a, b) => a.indexOf(b) == -1 ? a.concat([b]) : a), []).sort() + []",
//    Nil,
//    List(List(0))

    /************ KATA 2 ****************/
//    ASTNode.parseList(
//      """Object.assign(?,?)
//        |input
//        |{? : ?}
//        |{? : ?, ?: ?}""".stripMargin),
//    MainUtils.readIoFile("src/test/resources/kata2-io"),
//    "input.map(x => Object.create(x, { greeting: { value: \"Hi \" + x.firstName + \", what do you like the most about \" + x.language + \"?\", writable: true } }))",
//    List(1),
//    List(List(1,2,1,1))


  //)

  CLIMain.doMain(ASTNode.parseList(scala.io.Source.fromFile("src/test/resources/big-lambda-benchmarks/grep-vocab").mkString), MainUtils.readIoFile("src/test/resources/big-lambda-benchmarks/grep-io").take(1))
}
