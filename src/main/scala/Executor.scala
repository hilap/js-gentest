import com.eclipsesource.v8._

case class ExecResult(items : List[Option[FromJSWrapper]]){
  def get = items.flatten
  def allNonempty = items.forall(i => !i.isEmpty)
  def release(): Unit = {}//get.filter(l => l.isInstanceOf[JSValueWrapper]).foreach(l => l.asInstanceOf[V8Value].release())
  def emptyOrUndefined(): Boolean = items.exists(r => r.isEmpty || (r.get.isInstanceOf[JSUndefined]))
}

trait Executor {
  def execute(expr: ASTNode): ExecResult = execute(expr.toCode)

  def execute(expr: String): ExecResult
}
object ExecUtils{

  def resultWrapper(varRef : String): Any = "[typeof " + varRef + " == \"number\"  || typeof " + varRef + " == \"string\" || typeof " + varRef + " == \"boolean\" ? "+ varRef +" : "+varRef+" instanceof Map ? sortedJsonify([..."+varRef+"]) : sortedJsonify("+varRef+"), Array.isArray("+varRef+") ? \"array\" : "+varRef+" instanceof Map ? \"map\" : typeof "+varRef+"]"

  def wrapForJsonification(code : String) = "__res=" + code + ";\n" + resultWrapper("__res")
}
class ExecutorNoVal extends Executor {
  override def execute(expr: String): ExecResult = try {
    val arr = V8Runtime.runtime.executeArrayScript(ExecUtils.wrapForJsonification(expr))
    ExecResult(List(Some(FromJSWrapper(arr))))
    //arr.release() inside FromJSWrapper
  } catch {
    case ce: V8ScriptCompilationException => ExecResult(List(None))
    case seerr : V8ScriptExecutionException => ExecResult(List(None))
  }
}
case class VarInputs(inputs : List[String]) {
  def addInputs(newValues: List[String]): VarInputs = VarInputs(inputs ++ newValues)
}

class ExecutorWithVal (val inputVals : List[VarInputs], val varNames : List[String] = List("input")) extends Executor {
  def execute(expr: String): ExecResult = {
    val valsOrdered = inputVals.map(i => i.inputs) //Utils.cross(inputVals)
    ExecResult(valsOrdered.par.map { inputs =>
      try {
        val code = varNames.zipWithIndex.map(varname => varname._1 + " = " + inputs(varname._2) + "; ")
          .mkString("\n") + ExecUtils.wrapForJsonification(expr)
        Some(FromJSWrapper(V8Runtime.runtime.executeArrayScript(code)))
      }
      catch {
        case ce: V8ScriptCompilationException => None
        case seerr: V8ScriptExecutionException => None
      }
    }.toList)
  }
}
  object Utils {
    def cross[T](inputVals: List[List[T]]) = inputVals.length match {
      case 1 => inputVals.head.map(x => List(x))
      case _ => cartesianProduct(inputVals:_*)
    }

    def cartesianProduct[T](lst: List[T]*): List[List[T]] = {

      /**
        * Prepend single element to all lists of list
        * @param e single elemetn
        * @param ll list of list
        * @param a accumulator for tail recursive implementation
        * @return list of lists with prepended element e
        */
      def pel(e: T,
              ll: List[List[T]],
              a: List[List[T]] = Nil): List[List[T]] =
        ll match {
          case Nil => a.reverse
          case x :: xs => pel(e, xs, (e :: x) :: a )
        }

      lst.toList match {
        case Nil => Nil
        case x :: Nil => List(x)
        case x :: _ =>
          x match {
            case Nil => Nil
            case _ =>
              lst.par.foldRight(List(x))( (l, a) =>
                l.flatMap(pel(_, a))
              ).map(_.dropRight(x.size))
          }
      }
    }
  }


