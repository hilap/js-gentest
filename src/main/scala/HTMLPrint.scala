import scala.collection.mutable

class HTMLPrint {
  val programSB = new StringBuilder()
  val debugSB = new StringBuilder()
  var counter = 0

  def parenIfNeeded(n: ASTNode, exec : ExecutorWithVal): Unit = if (n.isInstanceOf[Literal] || n.isInstanceOf[ObjectInitializer]) htmlize(n, exec) else {
    programSB ++= "("
    htmlize(n, exec)
    programSB ++= ")"
  }

  def htmlizeArglist(args: Array[ASTNode], exec : ExecutorWithVal): Unit = {
    programSB ++= "("
    args.dropRight(1).foreach{ arg =>
      htmlize(arg, exec)
      programSB ++= ","
    }
    args.takeRight(1).foreach(a => htmlize(a, exec))

    programSB ++= ")"
  }

  def getCallbackRunValues(lhs_vals: List[JSArrayWrapper], lhs_inputs: List[VarInputs], funcName: String, innerCode : ASTNode, varNames: List[String], additionalArgs: Seq[ASTNode]) = {
    val lhs_jsons = lhs_vals.map(arrWrapper => VarInputs(List(MainUtils.escape(arrWrapper.jsonifiedValue))))
    val accumulate_script = "out = [];\n" +
      "lhs." + funcName + "( function (" + varNames.mkString(",") + ") {" +
      "out.push(Array.of(" +
      varNames.map(i => ExecUtils.resultWrapper(i)).mkString(",") +
      "));\n" +
      "return " + innerCode.toCode +
      "}" + additionalArgs.map(a => "," + a.toCode).mkString("") + ");\n" +
      "out"

    val res = mutable.ListBuffer[VarInputs]()
    for(lhs <- lhs_vals.zipWithIndex){
      val out = V8Runtime.runtime.executeArrayScript(varNames.zipWithIndex.map(v =>  "lhs = " + lhs._1.toCode + ";").mkString("\n") + accumulate_script);
      for (i <- 0 until out.length()) {
        val paramVals = out.getArray(i)
        val step = Array.ofDim[String](varNames.length)
        for (j <- 0 until paramVals.length()) {
          val v = paramVals.getArray(j)
          step.update(j,FromJSWrapper(v).toCode)
        }
        res += lhs_inputs(lhs._2).addInputs(step.toList)
      }
      out.release()
    }
    res.toList
  }

  def htmlize(prog: ASTNode, exec : ExecutorWithVal) : Unit = {
    counter = counter + 1
    val nodeId = "n" + counter
    val rootSpan = "<span class=\"root\" onmouseover=\"highlight(" + nodeId + "); showDiv(event," + nodeId + "io)\" onmouseout=\"unhighlight(" + nodeId + "); hideDiv(event," + nodeId + "io)\">"
    programSB ++= "<span id=\"" + nodeId + "\">"

    prog match {
      case l : Literal => {
        programSB ++= rootSpan
        programSB ++= l.func
        programSB ++= "</span>"
      }
      case b : BinOperator => {
        b.children(0).foreach{n => parenIfNeeded(n, exec) }
        programSB ++= rootSpan
        programSB ++= b.func
        programSB ++= "</span>"
        b.children(1).foreach{n => parenIfNeeded(n, exec)}
      }
      case a : AggregatorMethod => {
        parenIfNeeded(a.children.head.get, exec)
        programSB ++= "."
        programSB ++= rootSpan
        programSB ++= a.func
        programSB ++= "</span>"
        programSB ++= "("
        programSB ++= a.makeInnerVarSigStr
        programSB ++= " => "
        val lhs_vals = exec.execute(a.children.head.get)
        val newValues = getCallbackRunValues(
          if (lhs_vals.emptyOrUndefined() || lhs_vals.get.exists(!_.isInstanceOf[JSArrayWrapper])) List() else lhs_vals.get.map(_.asInstanceOf[JSArrayWrapper]),
          exec.inputVals,
          a.func,
          a.children(1).get,
          a.innerVar,
          a.children.drop(2).flatten.toSeq
        )
        htmlize(a.children(1).get, new ExecutorWithVal(newValues,exec.varNames ++ a.innerVar))
        a.children.drop(2).foreach{ c =>
          programSB ++= ","
          htmlize(c.get, exec)
        }
        programSB ++= ")"
      }
      case m : MethodCall => {

        parenIfNeeded(m.children.head.get, exec)
        programSB ++= "."
        programSB ++= rootSpan
        programSB ++= m.func
        programSB ++= "</span>"
        htmlizeArglist(m.children.drop(1).flatten, exec)

      }
      case t : TernaryOperator => {

        htmlize(t.children(0).get, exec)
        programSB ++= rootSpan
        programSB ++= " ? "
        programSB ++= "</span>"
        htmlize(t.children(1).get, exec)
        programSB ++= rootSpan
        programSB ++= " : "
        programSB ++= "</span>"
        htmlize(t.children(2).get, exec)

      }
      case u : UnPrefOperator => {
        programSB ++= rootSpan
        programSB ++= u.func
        programSB ++= "</span>"
        parenIfNeeded(u.children.head.get, exec)
      }
      case u : UnPostOperator => {
        programSB ++= "("
        htmlize(u.children.head.get, exec)
        programSB ++= ")"
        programSB ++= rootSpan
        programSB ++= u.func
        programSB ++= "</span>"
      }
      case a : ArrayInitializer => {
        programSB ++= rootSpan
        programSB ++= "["
        programSB ++= "</span>"
        a.children.drop(1).foreach { c =>
          htmlize(c.get, exec)
          programSB ++= ","
        }
        htmlize(a.children.last.get, exec)
        programSB ++= rootSpan
        programSB ++= "]"
        programSB ++= "</span>"
      }
      case c : Constructor => {
        programSB ++= rootSpan
        programSB ++= "new "
        programSB ++= c.func
        programSB ++= "</span>"
        htmlizeArglist(c.children.flatten,exec)
      }
      case a : ASTNode => {
        assert(a.getClass == classOf[ASTNode], a.getClass)

        programSB ++= rootSpan
        programSB ++= a.func
        programSB ++= "</span>"
        htmlizeArglist(a.children.flatten, exec)

      }
    }

    programSB ++= "</span>"
    debugSB ++= "<div class=\"tooltip\" id=\"" + nodeId + "io\">"
    val res = exec.execute(prog.toCode)
    exec.inputVals.zip(res.items).foreach{ ios =>
      exec.varNames.zip(ios._1.inputs).foreach{vi =>
        debugSB ++= vi._1
        debugSB ++= " = "
        debugSB ++= vi._2
        debugSB ++= "<br>"
      }
      debugSB ++= "result = "
      debugSB ++= ios._2.map(x => x.prettyPrint).getOrElse("<Error>")
      debugSB ++= "<br>"
      debugSB ++= "<hr>"
    }
    debugSB ++= "</div>"
  }

}

object PrintMain extends App{
  val prog = "foome(1,new Map)"//"input.split(' ').reduce((a,e) => a.set(e,a.has(e) ? a.get(e) + 1: 1),new Map())"//"Number(input.reduce(((a, b) => a.indexOf(b) == -1 ? a.concat([b]) : a), []).sort() + [])"
  val decons = JSParser.deconstruct(prog)
  val mainexec = new ExecutorWithVal(List(VarInputs(List("\"what WHAT the what the what\""))))//new ExecutorWithVal(List(VarInputs(List("[5, 7, 9, 9]")),VarInputs(List("[1,1,1,3]"))))
  val h = new HTMLPrint
  h.htmlize(decons,mainexec)
  println(h.debugSB.toString)
  println("<p>")
  println(h.programSB.toString)
  println()
}
