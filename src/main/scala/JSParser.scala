

import JavaScriptParser._
import org.antlr.v4.runtime.tree.{ErrorNode, ParseTree, RuleNode, TerminalNode}
import org.antlr.v4.runtime.{BailErrorStrategy, CharStreams, CommonTokenStream}

import scala.collection.JavaConversions._

object JSParser {


  val jsBuiltInNames = Set("Object","Array", "Number", "Date","JSON","Math","NaN")
  class JSVisitor extends JavaScriptParserBaseVisitor[ASTNode] {
    override def aggregateResult (aggregate: ASTNode, nextResult: ASTNode): ASTNode =
      if (aggregate == null)
        nextResult
      else if (nextResult == null)
        aggregate
      else throw new Exception()
    override def visitLiteralExpression(ctx: JavaScriptParser.LiteralExpressionContext) = {
      Literal(ctx.literal().getText)
    }

    override def visitArrayLiteral(ctx: JavaScriptParser.ArrayLiteralContext): ASTNode = {
      class NonConstantVisitor extends JavaScriptParserBaseVisitor[Boolean]{
        override def defaultResult(): Boolean = false
        override def aggregateResult(aggregate: Boolean, nextResult: Boolean): Boolean = aggregate || nextResult

        override def visitVariableDeclaration(ctx: VariableDeclarationContext): Boolean = true
        override def visitIfStatement(ctx: IfStatementContext): Boolean = true
        override def visitDoStatement(ctx: DoStatementContext): Boolean = true
        override def visitWhileStatement(ctx: WhileStatementContext): Boolean = true
        override def visitForStatement(ctx: ForStatementContext): Boolean = true
        override def visitForVarStatement(ctx: ForVarStatementContext): Boolean = true
        override def visitForInStatement(ctx: ForInStatementContext): Boolean = true
        override def visitForVarInStatement(ctx: ForVarInStatementContext): Boolean = true
        override def visitVarModifier(ctx: VarModifierContext): Boolean = ???
        override def visitContinueStatement(ctx: ContinueStatementContext): Boolean = true
        override def visitBreakStatement(ctx: BreakStatementContext): Boolean = true
        override def visitReturnStatement(ctx: ReturnStatementContext): Boolean = true
        override def visitWithStatement(ctx: WithStatementContext): Boolean = ???
        override def visitSwitchStatement(ctx: SwitchStatementContext): Boolean = true
        override def visitThrowStatement(ctx: ThrowStatementContext): Boolean = true
        override def visitPropertyGetter(ctx: PropertyGetterContext): Boolean = true
        override def visitPropertySetter(ctx: PropertySetterContext): Boolean = true
        override def visitMethodProperty(ctx: MethodPropertyContext): Boolean = true
        override def visitPropertyShorthand(ctx: PropertyShorthandContext): Boolean = true
        override def visitPropertyName(ctx: PropertyNameContext): Boolean = true

        override def visitArguments(ctx: ArgumentsContext): Boolean = ???

        override def visitLastArgument(ctx: LastArgumentContext): Boolean = ???

        override def visitExpressionSequence(ctx: ExpressionSequenceContext): Boolean = ???

        override def visitTemplateStringExpression(ctx: TemplateStringExpressionContext): Boolean = ???

        override def visitTernaryExpression(ctx: TernaryExpressionContext): Boolean = true
        override def visitLogicalAndExpression(ctx: LogicalAndExpressionContext): Boolean = true
        override def visitPreIncrementExpression(ctx: PreIncrementExpressionContext): Boolean = true
        override def visitInExpression(ctx: InExpressionContext): Boolean = true
        override def visitLogicalOrExpression(ctx: LogicalOrExpressionContext): Boolean = true
        override def visitNotExpression(ctx: NotExpressionContext): Boolean = true
        override def visitPreDecreaseExpression(ctx: PreDecreaseExpressionContext): Boolean = true

        override def visitArgumentsExpression(ctx: ArgumentsExpressionContext): Boolean = ???

        override def visitThisExpression(ctx: ThisExpressionContext): Boolean = true
        override def visitFunctionExpression(ctx: FunctionExpressionContext): Boolean = true
        override def visitUnaryMinusExpression(ctx: UnaryMinusExpressionContext): Boolean = true
        override def visitAssignmentExpression(ctx: AssignmentExpressionContext): Boolean = true
        override def visitPostDecreaseExpression(ctx: PostDecreaseExpressionContext): Boolean = true
        override def visitTypeofExpression(ctx: TypeofExpressionContext): Boolean = true
        override def visitInstanceofExpression(ctx: InstanceofExpressionContext): Boolean = true
        override def visitUnaryPlusExpression(ctx: UnaryPlusExpressionContext): Boolean = true
        override def visitDeleteExpression(ctx: DeleteExpressionContext): Boolean = true
        override def visitArrowFunctionExpression(ctx: ArrowFunctionExpressionContext): Boolean = true
        override def visitEqualityExpression(ctx: EqualityExpressionContext): Boolean = true
        override def visitBitXOrExpression(ctx: BitXOrExpressionContext): Boolean = true
        override def visitSuperExpression(ctx: SuperExpressionContext): Boolean = true
        override def visitMultiplicativeExpression(ctx: MultiplicativeExpressionContext): Boolean = true
        override def visitBitShiftExpression(ctx: BitShiftExpressionContext): Boolean = true
        override def visitAdditiveExpression(ctx: AdditiveExpressionContext): Boolean = true
        override def visitRelationalExpression(ctx: RelationalExpressionContext): Boolean = ???
        override def visitPostIncrementExpression(ctx: PostIncrementExpressionContext): Boolean = true
        override def visitBitNotExpression(ctx: BitNotExpressionContext): Boolean = true
        override def visitNewExpression(ctx: NewExpressionContext): Boolean = true
        override def visitMemberDotExpression(ctx: MemberDotExpressionContext): Boolean = true
        override def visitClassExpression(ctx: ClassExpressionContext): Boolean = true
        override def visitMemberIndexExpression(ctx: MemberIndexExpressionContext): Boolean = true
        override def visitIdentifierExpression(ctx: IdentifierExpressionContext): Boolean = true
        override def visitBitAndExpression(ctx: BitAndExpressionContext): Boolean = true
        override def visitBitOrExpression(ctx: BitOrExpressionContext): Boolean = true
        override def visitAssignmentOperatorExpression(ctx: AssignmentOperatorExpressionContext): Boolean = true

        override def visitIdentifierName(ctx: IdentifierNameContext): Boolean = true

        override def visitObjectLiteral(ctx: ObjectLiteralContext): Boolean = (ctx.propertyAssignment() != null && ctx.propertyAssignment().size() > 0)

      }
      if (ctx.elementList() != null && ctx.elementList().accept(new NonConstantVisitor)) {
        val res = ArrayInitializer(ctx.elementList().singleExpression().size())
        (0 until res.arity).foreach(i => res.addChild(i, ctx.elementList().singleExpression(i).accept(this)))
        res
      }
      else Literal(ctx.getText)
    }

    override def visitIdentifierExpression(ctx: JavaScriptParser.IdentifierExpressionContext): ASTNode = Literal(ctx.getText)

    override def visitIdentifierName(ctx: IdentifierNameContext): ASTNode = Literal(ctx.getText)


    case class FuncInfo(ast : ASTNode, vars : List[String])
    def getFunction(func : SingleExpressionContext) : FuncInfo = {
      val paramsList = func match {
        case fe : FunctionExpressionContext => fe.formalParameterList().formalParameterArg().map(id => id.Identifier().getText).toList
        case af : ArrowFunctionExpressionContext => if (af.arrowFunctionParameters().formalParameterList() == null)
          if (af.arrowFunctionParameters() == null || af.arrowFunctionParameters().Identifier() == null) Nil else List(af.arrowFunctionParameters().Identifier().getText)
        else af.arrowFunctionParameters().formalParameterList().formalParameterArg().map(id => id.Identifier().getText).toList
        case pf : ParenthesizedExpressionContext => return getFunction(pf.expressionSequence().singleExpression(0))
      }
      val body = func match {
        case fe: FunctionExpressionContext => {
          val funcRet = fe.functionBody().children(0).getChild(0).getChild(0).getChild(0).asInstanceOf[ReturnStatementContext] //if this fails, the func is not just an expr
          funcRet.expressionSequence().accept(this)
        }
        case af: ArrowFunctionExpressionContext => {
          af.arrowFunctionBody().singleExpression().accept(this)
        }
      }
      FuncInfo(body, paramsList)
    }

    override def visitArgumentsExpression(ctx: JavaScriptParser.ArgumentsExpressionContext): ASTNode = {
      val arity = ctx.arguments().singleExpression().size()
      ctx.singleExpression() match {
      case fcall : MemberDotExpressionContext => {
        val lhs = fcall.singleExpression().accept(this)
        val fname = fcall.identifierName().getText
        val cbackidx = ctx.arguments().singleExpression().indexWhere(ex =>
          ex.isInstanceOf[ArrowFunctionExpressionContext] || ex.isInstanceOf[FunctionExpressionContext] ||
            (ex.isInstanceOf[ParenthesizedExpressionContext] && {
              val inPars = ex.asInstanceOf[ParenthesizedExpressionContext].expressionSequence()
              if (inPars.children.length == 1) {
                val exprInPars = inPars.singleExpression(0)
                exprInPars.isInstanceOf[ArrowFunctionExpressionContext] || exprInPars.isInstanceOf[FunctionExpressionContext]
              }
              else false
            })
        )
        if (cbackidx != -1) {
          val fi = getFunction(ctx.arguments().singleExpression(cbackidx))
          val args = ctx.arguments().singleExpression().map(ex =>
            if (ex.isInstanceOf[ArrowFunctionExpressionContext] || ex.isInstanceOf[FunctionExpressionContext]) fi.ast //yes, this will only work if there's one.
            else ex.accept(this)
          )
          val agg = AggregatorMethod(fname,args.length,fi.vars,cbackidx) //get this from vocab?? maybe you have 1param map and want 2
          agg.addChild(0,lhs)
          args.zipWithIndex.foreach(child => agg.addChild(child._2 + 1, child._1))
          agg
        }
        else if (lhs.isInstanceOf[Literal] && jsBuiltInNames.contains(lhs.func)) {
          val func = new ASTNode(lhs.func + "." + fname,arity)
          ctx.arguments().singleExpression().zipWithIndex.foreach(child => func.addChild(child._2, child._1.accept(this)))
          func
        }
        else{
          val method = new MethodCall(fname, arity)
          method.addChild(0, lhs)
          ctx.arguments().singleExpression().zipWithIndex.foreach(child => method.addChild(child._2 + 1, child._1.accept(this)))
          method
        }
      }
      case _ => {
        val func = new ASTNode(ctx.singleExpression().getText,arity)
        ctx.arguments().singleExpression().zipWithIndex.foreach(child => func.addChild(child._2, child._1.accept(this)))
        func
      }
    }
    }

    override def visitAdditiveExpression(ctx: JavaScriptParser.AdditiveExpressionContext): ASTNode = doBinop(ctx)

    def doBinop(ctx : SingleExpressionContext) = {
      val op = BinOperator(ctx.children(1).getText)
      op.addChild(0,ctx.children(0).accept(this))
      op.addChild(1,ctx.children(2).accept(this))
      op
    }

    override def visitMultiplicativeExpression(ctx: JavaScriptParser.MultiplicativeExpressionContext): ASTNode = doBinop(ctx)

    override def visitNotExpression(ctx: JavaScriptParser.NotExpressionContext): ASTNode = doUnPrefOp(ctx)

    def doUnPrefOp(ctx : SingleExpressionContext) = {
      val op = UnPrefOperator(ctx.children(0).getText)
      op.addChild(0,ctx.children(1).accept(this))
      op
    }

    def doUnPostOp(ctx : SingleExpressionContext) = {
      val op = UnPostOperator(ctx.children(1).getText)
      op.addChild(0,ctx.children(0).accept(this))
      op
    }

    override def visitBitNotExpression(ctx: JavaScriptParser.BitNotExpressionContext): ASTNode = doUnPrefOp(ctx)

    //override def visitComputedPropertyExpressionAssignment(ctx: JavaScriptParser.ComputedPropertyExpressionAssignmentContext): ASTNode = visitChildren(ctx)

    //def visitExpressionSequence(ctx: JavaScriptParser.ExpressionSequenceContext): Nothing = visitChildren(ctx)

    override def visitTemplateStringExpression(ctx: JavaScriptParser.TemplateStringExpressionContext): ASTNode = null

    override def visitTernaryExpression(ctx: JavaScriptParser.TernaryExpressionContext): ASTNode = {
      val op = TernaryOperator()
      ctx.singleExpression().zipWithIndex.foreach(child => op.addChild(child._2,child._1.accept(this)))
      op
    }

    override def visitLogicalAndExpression(ctx: JavaScriptParser.LogicalAndExpressionContext): ASTNode = doBinop(ctx)

    override def visitPreIncrementExpression(ctx: JavaScriptParser.PreIncrementExpressionContext): ASTNode = doUnPrefOp(ctx)

    override def visitInExpression(ctx: JavaScriptParser.InExpressionContext): ASTNode = doBinop(ctx)

    override def visitLogicalOrExpression(ctx: JavaScriptParser.LogicalOrExpressionContext): ASTNode = doBinop(ctx)

    override def visitPreDecreaseExpression(ctx: JavaScriptParser.PreDecreaseExpressionContext): ASTNode = doUnPrefOp(ctx)

    override def visitThisExpression(ctx: JavaScriptParser.ThisExpressionContext): ASTNode = Literal(ctx.getText)

    //def visitFunctionExpression(ctx: JavaScriptParser.FunctionExpressionContext): Nothing = visitChildren(ctx)

    override def visitUnaryMinusExpression(ctx: JavaScriptParser.UnaryMinusExpressionContext): ASTNode = doUnPrefOp(ctx)

    override def visitPostDecreaseExpression(ctx: JavaScriptParser.PostDecreaseExpressionContext): ASTNode = doUnPostOp(ctx)

    override def visitTypeofExpression(ctx: JavaScriptParser.TypeofExpressionContext): ASTNode = doUnPrefOp(ctx)

    override def visitInstanceofExpression(ctx: JavaScriptParser.InstanceofExpressionContext): ASTNode = doBinop(ctx)

    override def visitUnaryPlusExpression(ctx: JavaScriptParser.UnaryPlusExpressionContext): ASTNode = doUnPrefOp(ctx)

    //def visitDeleteExpression(ctx: JavaScriptParser.DeleteExpressionContext): Nothing = visitChildren(ctx)

    //def visitArrowFunctionExpression(ctx: JavaScriptParser.ArrowFunctionExpressionContext): Nothing = visitChildren(ctx)

    override def visitEqualityExpression(ctx: JavaScriptParser.EqualityExpressionContext): ASTNode = doBinop(ctx)

    override def visitBitXOrExpression(ctx: JavaScriptParser.BitXOrExpressionContext): ASTNode = doBinop(ctx)

    override def visitSuperExpression(ctx: JavaScriptParser.SuperExpressionContext): ASTNode = Literal(ctx.getText)

    override def visitBitShiftExpression(ctx: JavaScriptParser.BitShiftExpressionContext): ASTNode = doBinop(ctx)

    override def visitRelationalExpression(ctx: JavaScriptParser.RelationalExpressionContext): ASTNode = doBinop(ctx)

    override def visitPostIncrementExpression(ctx: JavaScriptParser.PostIncrementExpressionContext): ASTNode = doUnPostOp(ctx)

    override def visitNewExpression(ctx: JavaScriptParser.NewExpressionContext): ASTNode = {
      ctx.singleExpression() match {
        case args : ArgumentsExpressionContext => {
          val arity = args.arguments().singleExpression().size()
          val ctor = Constructor(args.singleExpression().getText, arity) //ctx.singleExpression().children.size() - 1)
          args.arguments().singleExpression().zipWithIndex.foreach(child => ctor.addChild(child._2, child._1.accept(this)))
          ctor
        }
        case id : IdentifierExpressionContext => {
          val ctor = Constructor(id.children(0).getText,id.children.size() - 1)
          id.children.tail.zipWithIndex.foreach(child => ctor.addChild(child._2,child._1.accept(this)))
          ctor
        }
    }}

    override def visitMemberDotExpression(ctx: JavaScriptParser.MemberDotExpressionContext): ASTNode = {
      val pc = PropCall(ctx.identifierName().getText)
      pc.addChild(0,ctx.children(0).accept(this))
      pc
    }

    //def visitClassExpression(ctx: JavaScriptParser.ClassExpressionContext): Nothing = visitChildren(ctx)

    override def visitMemberIndexExpression(ctx: JavaScriptParser.MemberIndexExpressionContext): ASTNode = {
      val ad = ArrayDeref()
      ad.addChild(0,ctx.children(0).accept(this))
      ad.addChild(1,ctx.children(2).accept(this))
      ad
    }

    override def visitBitAndExpression(ctx: JavaScriptParser.BitAndExpressionContext): ASTNode = doBinop(ctx)

    override def visitBitOrExpression(ctx: JavaScriptParser.BitOrExpressionContext): ASTNode = doBinop(ctx)

    override def visitPropertyName(ctx: PropertyNameContext): ASTNode = Literal(ctx.getText)

    override def visitObjectLiteral(ctx: JavaScriptParser.ObjectLiteralContext): ASTNode = {
      if (ctx.propertyAssignment().length == 0) return Literal("{}")

      val res = ObjectInitializer(ctx.propertyAssignment.length)
      (0 until ctx.propertyAssignment.length).foreach{i =>
        val pa = ctx.propertyAssignment(i)
        val pname = pa.children(0).accept(this)
        res.addChild(2*i,if (!pname.isInstanceOf[Literal] || (pname.func.startsWith("'") || pname.func.startsWith("\""))) pname else Literal("'" + MainUtils.escape(pname.func) + "'"))
        res.addChild(2*i + 1, pa.children(2).accept(this))
      }
      res

    }


    override def visitBlock(ctx: BlockContext): ASTNode = {
      if (null == ctx.statementList())
        Literal("{}")
      else if (ctx.statementList().statement().forall(s => s.labelledStatement() != null && (s.labelledStatement().statement().expressionStatement() != null || s.labelledStatement().statement().block() != null))) {
        //is obj initializer
        val res = ObjectInitializer(ctx.statementList().statement().size())
        (0 until ctx.statementList().statement().size()).foreach{i =>
          val pname = ctx.statementList().statement(i).labelledStatement().Identifier().getText
          res.addChild(2 * i, Literal(if (pname.startsWith("'") || pname.startsWith("\"")) pname else "'" + pname + "'"))
          res.addChild(2 * i + 1, ctx.statementList().statement(i).labelledStatement().statement().accept(this))
        }
        res
      }
      else if (ctx.children.size() == 1)
        ctx.children(0).accept(this)
      else null
    }

    //def visitAssignmentOperatorExpression(ctx: JavaScriptParser.AssignmentOperatorExpressionContext): Nothing = visitChildren(ctx)

    //def visitVoidExpression(ctx: JavaScriptParser.VoidExpressionContext): Nothing = visitChildren(ctx)
  }

  def deconstruct(jsExpr: String): ASTNode = {
    val lex = new JavaScriptLexer(CharStreams.fromString(jsExpr))
    val parser = new JavaScriptParser(new CommonTokenStream(lex))
    parser.setErrorHandler(new BailErrorStrategy)
    parser.program.accept(new JSVisitor)
  }

}
