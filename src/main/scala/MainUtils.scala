import scala.util.matching.Regex

object MainUtils {
  val strRegex : Regex = "\\\'((?:[^\'\\\\]|\\\\\\\'|\\\\\\\"|\\\\\\\\)*)\\\'.*".r
  def readIoFile(path: String) : List[Tuple2[String,String]] = {
    val lines = scala.io.Source.fromFile(path).getLines()
    parseIOLines(lines)
  }
  def parseIOLines(lines : Iterator[String]) : List[Tuple2[String,String]] = {
    lines.map {line =>
      val beg = line.dropWhile(c => c.isWhitespace)
      if (beg.head != '\'') throw new Exception("Malformed I/O file")
      val input : String = beg match {
        case strRegex(str) => str
      }
      if (beg.drop(input.size + 2).head != '\t' || beg.drop(input.size + 3).head != '\'')
        throw new Exception("Malformed I/O file")

      val rest = beg.drop(input.size + 3)
      val output : String = rest match {
        case strRegex(str) => str
      }
      if (!(rest.drop(output.size + 2).dropWhile(c => c.isWhitespace).isEmpty))
        throw new Exception()//"Malformed I/O file")

      (input,output)
    }.toList
  }

  def unescape(s : String) : String = {
    var str = s
    val sb = new StringBuilder(str.length)
    var inEscape = false
    while(!str.isEmpty) {
      val c = str.head
      str = str.tail
      if (!inEscape && c != '\\') {
        sb.append(c)
      }
      else if (inEscape) {
        if (c != '"') sb.append('\\')
        sb.append(c)
        inEscape = false
      }
      else { // == \
        inEscape = true
      }
    }
    sb.toString
  }

  def escape(s : String) : String = {
    var str = s
    val sb = new StringBuilder(s.length)
    while (!str.isEmpty) {
      val c = str.head
      str = str.tail
      if (c == '\'') {
        sb.append("\\'")
      }
      else sb.append(c)
    }
    sb.toString
  }
}
