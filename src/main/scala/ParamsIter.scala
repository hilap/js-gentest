import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import Enumerator.SeenProgram
/**
 * Created by hila on 02/03/2018.
 */

trait ParamsIter extends Iterator[List[ASTNode]]{}

class SimpleParamsIter(val paramsLists: List[Iterable[Enumerator.SeenProgram]], val optionalArgs : Int, val prevLevelHeight : Int, argConstraints : List[Set[DataType.DataType]]) extends ParamsIter{
  override def toString(): String = "SimplParamsIter (nxt = " + nxt.toString + ")"
  var iters : Array[Iterator[Enumerator.SeenProgram]] = paramsLists.slice(0,paramsLists.length - optionalArgs).map(x => x.iterator).toArray
  val currs : mutable.ListBuffer[SeenProgram] = {
    val lb = new ListBuffer[SeenProgram]
    iters.slice(0,iters.length - 1).map(i => i.next()).foreach(x => lb += x)
    lb
  }

  override def hasNext: Boolean = if (!nxt.isEmpty) true
                                  else if (iters.reverseIterator.forall(i => !i.hasNext) && !(iters.length < paramsLists.length)) false
                                  else {
                                    nxt = do_next()
                                    !nxt.isEmpty
                                  }
  var nxt : Option[List[ASTNode]] = None
  override def next : List[ASTNode] = {
    if (nxt.isEmpty) {
      nxt = do_next()
      if (!nxt.isEmpty) {
        val res = nxt.get
        nxt = None
        res
      }
      else throw new NoSuchElementException("next on empty iterator")
    }
    else {
      val res = nxt.get
      nxt = None
      res
    }
  }

  def do_next(): Option[List[ASTNode]] = {
    var res : Option[List[ASTNode]] = None
    while (res.isEmpty && (iters.reverseIterator.exists(i => i.hasNext) || (iters.length < paramsLists.length))) {
      val ir : List[SeenProgram] = if (iters.isEmpty) {
        //prep the next iter
        currs.clear()
        iters = paramsLists.slice(0, 1).map(x => x.iterator).toArray
        List()
      }
      else if (iters.last.hasNext) {
        currs.toList :+ iters.last.next()
      }
      else if (iters.exists(i => i.hasNext)) {
        var idx: Int = iters.length - 1
        while (!iters(idx).hasNext) {
          idx = idx - 1
        }
        currs.update(idx, iters(idx).next())
        (idx + 1 until iters.length - 1).foreach { i =>
          iters(i) = paramsLists(i).iterator
          currs.update(i, iters(i).next())
        }
        iters(iters.length - 1) = paramsLists.last.iterator
        currs.toList :+ iters.last.next()
      }
      else {
        iters = paramsLists.slice(0, iters.length + 1).map(x => x.iterator).toArray
        iters.slice(0, iters.length - 2).map(i => i.next()).zipWithIndex.foreach(x => currs.update(x._2, x._1))
        currs += iters.takeRight(2).head.next()
        currs.toList :+ iters.last.next()
      }

      if (ir.isEmpty || (ir.exists(_.prog.heightNoLambdas >= prevLevelHeight) && ir.zipWithIndex.forall{case (p,i) => (p.types & argConstraints(i)).isEmpty})) {
        res = Some(ir.map(p => p.prog))
      }
    }
    res
  }
}
