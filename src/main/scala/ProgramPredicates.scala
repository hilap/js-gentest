import DataType.DataType

trait ProgramPredicate {
  def apply(program : ASTNode) : Boolean
}

case class RetainPredicate(toRetain : ASTNode) extends ProgramPredicate
{
  override def apply(program : ASTNode) : Boolean = program.contains(toRetain)
}

case class ExcludePredicate(toExclude : ASTNode) extends ProgramPredicate {
  override def apply(program : ASTNode) : Boolean = !program.contains(toExclude)
}


trait SubexpressionDataPredicate extends ProgramPredicate {
  val jsonifiedInput: VarInputs
  val inputVarnames: List[String]
  val positivePredicate: Boolean
  val executor: Executor = new ExecutorWithVal(List(jsonifiedInput),inputVarnames)
  override def apply(program: ASTNode): Boolean = {
    val iter = program.innerWalkAllNodes()
    val f : (ASTNode => Boolean) => Boolean = if (positivePredicate) iter.exists else iter.forall
    f { subexpr =>
      val execResult = executor.execute(subexpr)
      dataPredicate(execResult.get.head)
    }
  }
  def dataPredicate(subexprResult: FromJSWrapper) : Boolean
}
case class SubexpressionEquals(jsonifiedInput: VarInputs, inputVarnames: List[String], jsonifiedOutput: String) extends  SubexpressionDataPredicate {
  override val positivePredicate: Boolean = true
  val output = new ExecutorNoVal().execute(jsonifiedOutput).get.head
  override def dataPredicate(subexprResult: FromJSWrapper): Boolean = subexprResult == output
}

case class NoSubexpressionEquals(jsonifiedInput: VarInputs, inputVarnames: List[String], jsonifiedOutput: String) extends  SubexpressionDataPredicate {
  override val positivePredicate: Boolean = false
  val output = new ExecutorNoVal().execute(jsonifiedOutput).get.head
  override def dataPredicate(subexprResult: FromJSWrapper): Boolean = subexprResult != output
}

case class NoSubexpressionOfType(jsonifiedInput: VarInputs, inputVarnames: List[String], dataType: DataType) extends SubexpressionDataPredicate {
  override val positivePredicate: Boolean = false
  override def dataPredicate(subexprResult: FromJSWrapper): Boolean = subexprResult.getType != dataType
}

case class SubexpressionOfType(jsonifiedInput: VarInputs, inputVarnames: List[String], dataType: DataType) extends SubexpressionDataPredicate {
  override val positivePredicate: Boolean = true

  override def dataPredicate(subexprResult: FromJSWrapper): Boolean = subexprResult.getType == dataType
}