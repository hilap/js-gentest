import spray.json._
import spray.json.DefaultJsonProtocol._

import scala.collection.mutable

object ASTNodeJson {

  def needsParen(n: ASTNode): Boolean = !(n.isInstanceOf[Literal] || n.isInstanceOf[ObjectInitializer] || n.isInstanceOf[MethodCall] || n.isInstanceOf[AggregatorMethod] ||
    n.isInstanceOf[ArrayDeref] || n.isInstanceOf[ArrayInitializer] || n.getClass == classOf[ASTNode])

  def csl(args: Array[ASTNode], dropped : Int = 0): TraversableOnce[TreeNode] = (0 until args.length).flatMap { i =>
    if (i + 1 < args.length)
      List(toNode(args(i), i+dropped), TreeNode(",", -1, Nil))
    else List(toNode(args(i), i+dropped))
  }

  def parenthesize(origNode: TreeNode): TreeNode = origNode.copy(subProg = "(" + origNode.subProg + ")", children = TreeNode("(",-1,Nil) +: origNode.children :+ TreeNode(")", -1, Nil))

  def toNode(prog: ASTNode, ordinal: Int): TreeNode = {
    val children = mutable.MutableList[TreeNode]()
    prog match {
      case a : AggregatorMethod=> {
        val lhs = toNode(a.children(0).get,0)
        if (needsParen(a.children(0).get)) {
          children += parenthesize(lhs)
        }
        else children += lhs
        children += TreeNode("." + a.func + "(",-1,Nil)
        children += TreeNode(a.makeInnerVarSigStr + " => ",-2,Nil)
        children ++= csl(a.children.drop(1).flatten,1)
        children += TreeNode(")", -1, Nil)
      }
      case m : MethodCall=> {
        val lhs = toNode(m.children(0).get,0)
        if (needsParen(m.children(0).get)) {
          children += parenthesize(lhs)
        }
        else children += lhs
        children += TreeNode("." + m.func + "(",-1,Nil)
        children ++= csl(m.children.drop(1).flatten,1)
        children += TreeNode(")",-1,Nil)
      }
      case l : Literal => {
        return TreeNode(l.toCode, ordinal, Nil)
      }
      case b : BinOperator => {
        val lhs = toNode(b.children(0).get,0)
        if (needsParen(b.children(0).get)) {
          children += parenthesize(lhs)
        }
        else children += lhs
        children += TreeNode(" " + b.func + " ", -1, Nil)
        val rhs = toNode(b.children(1).get,1)

        if (needsParen(b.children(1).get)) {
          children += parenthesize(rhs)
        }
        else children += rhs
      }
      case t : TernaryOperator=> {
        val c1 = toNode(t.children(0).get,0)
        children += c1
        children += TreeNode(" ? ", -1, Nil)
        val c2 = toNode(t.children(1).get,1)
        children += c2
        children += TreeNode(" : ", -1, Nil)
        val c3 = toNode(t.children(2).get, 2)
        children += c3
      }
      case p : UnPrefOperator => {
        children += TreeNode(p.func,-1,Nil)
        val child = toNode(p.children(0).get,0)
        if (needsParen(p.children(0).get)) {
          children += parenthesize(child)
        }
        else children += child
      }
      case p : UnPostOperator => {
        val child = toNode(p.children(0).get,0)
        if (needsParen(p.children(0).get)) {
          children += parenthesize(child)
        }
        else children += child
        children += TreeNode(p.func,-1, Nil)
      }
      case a : ArrayDeref=> {
        val lhs = toNode(a.children(0).get,0)
        if (needsParen(a.children(0).get))
          children += parenthesize(lhs)
        else children += lhs

        children += TreeNode("[",-1,Nil)

        val rhs = toNode(a.children(1).get,1)
        children += rhs
        children += TreeNode("]",-1,Nil)
      }
      case p : PropCall=> {
        val child = toNode(p.children(0).get, 0)
        if (needsParen(p.children(0).get)) {
          children += parenthesize(child)
        }
        else children += child

        children += TreeNode("." +  p.func, -1, Nil)
      }
      case c : Constructor => {
        children += TreeNode("new " + c.func + (if (c.children.isEmpty) "" else "("),-1, Nil)
        children ++= csl(c.children.flatten)
        if (!c.children.isEmpty) {
          children += TreeNode(")", -1, Nil)
        }
      }
      case o : ObjectInitializer=> {
        children += TreeNode("{",-1,Nil)
        for(i <- 0 until o.arity/2) {
          children += toNode(o.children(2 * i).get, 2 * i)
          children += TreeNode(" : ", -1, Nil)
          children += toNode(o.children(2*i + 1).get,2*i+1)
          if (i + 1 < o.arity/2) children += TreeNode(",",-1,Nil)
        }
        children += TreeNode("}",-1,Nil)
      }
      case a : ArrayInitializer => {
        children += TreeNode("[", -1, Nil)
        for(i <- 0 until a.arity) {
          children += toNode(a.children(i).get, i)
          if (i + 1 < a.arity) children += TreeNode(",",-1,Nil)
        }
        children += TreeNode("]", -1, Nil)
      }
      case a : ASTNode => {
        assert(a.getClass == classOf[ASTNode])
        children += TreeNode(a.func + "(", -1, Nil)
        children ++= csl(a.children.flatten)
        children += TreeNode(")", -1, Nil)
      }
    }
    if (children.forall(c => c.ord == -1))
      TreeNode(children.map(c => c.subProg).mkString,ordinal,Nil)
    else
      TreeNode(children.map(c => c.subProg).mkString,ordinal,children.toList)
  }

  def construct(prog: ASTNode): TreeNode = {
    //val fullCode = prog.toCode
    toNode(prog, 0)
  }


  case class TreeNode(subProg : String, ord : Int, children : List[TreeNode])

  implicit val programTreeFormat : JsonFormat[TreeNode] = lazyFormat(jsonFormat3(TreeNode))
}