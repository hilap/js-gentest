import spray.json.DefaultJsonProtocol.{jsonFormat2, lazyFormat}
import spray.json.JsonFormat
import spray.json.DefaultJsonProtocol._

import scala.collection.mutable

object DebugInfoJson {

  def getCallbackRunValues(lhs_codes: List[String], lhs_inputs: List[VarInputs], inputVarnames : List[String], inputPrettyPrints : List[List[String]], funcName: String, innerCode : ASTNode, varNames: List[String], additionalArgs: Seq[ASTNode]) = {
    assert(lhs_codes.length == lhs_inputs.length)
    val lhs_jsons = lhs_codes.zip(lhs_inputs).map{case (arrCode : String,input: VarInputs) => input.addInputs(List(arrCode))}
    val accumulate_script = "out = [];\n" +
      "lhs." + funcName + "( function (" + varNames.mkString(",") + ") {" +
      "out.push(Array.of(" +
      varNames.map(i => ExecUtils.resultWrapper(i)).mkString(",") +
      "));\n" +
      "try {return " + innerCode.toCode + ";} catch(err) {return undefined}"+
      "}" + additionalArgs.map(a => "," + a.toCode).mkString("") + ");\n" +
      "out"

    val res = mutable.ListBuffer[VarInputs]()
    val res2 = mutable.ListBuffer[List[String]]()
    for(lhs <- lhs_codes.zipWithIndex){
      val currVarVals = lhs_jsons(lhs._2).inputs
      try {
        val out = V8Runtime.runtime.executeArrayScript((currVarVals.dropRight(1).zip(inputVarnames) :+ (currVarVals.last, "lhs")).map(v => v._2 + "=" + v._1 + ";").mkString("\n") + accumulate_script)
        for (i <- 0 until out.length()) {
          val paramVals = out.getArray(i)
          val step = Array.ofDim[String](varNames.length)
          val stepPretty = Array.ofDim[String](varNames.length)
          for (j <- 0 until paramVals.length()) {
            val v = paramVals.getArray(j)
            val wrapper = FromJSWrapper(v)
            step.update(j, wrapper.toCode)
            stepPretty.update(j, wrapper.prettyPrint)
            v.release()
          }
          paramVals.release()
          res += lhs_inputs(lhs._2).addInputs(step.toList)
          res2 += inputPrettyPrints(lhs._2) ++ stepPretty.toList
        }
        out.release()
      }
      catch {
        case e: Exception => {
          res += lhs_inputs(lhs._2).addInputs(varNames.map(n => "undefined"))
          res2 += inputPrettyPrints(lhs._2) ++ varNames.map(n => "undefined")
        }
      }
    }
    (res.toList,res2.toList)
  }

  final case class ContextResult(inputVals : List[String], output : String)
  final case class Context(vars : List[String], contextResults : List[ContextResult])
  final case class DebugInfo(current : Context, children : List[DebugInfo])
  implicit val ctxResultFormat : JsonFormat[ContextResult] = jsonFormat2(ContextResult)
  implicit val ctxFormat : JsonFormat[Context] = jsonFormat2(Context)
  implicit val debugInfoFormat : JsonFormat[DebugInfo] = lazyFormat(jsonFormat2(DebugInfo))

  def innerAccumulate(p: ASTNode, exec: ExecutorWithVal, inputPrettyPrints : List[List[String]]): DebugInfo  = {
    val debugLines = mutable.ArrayBuffer[ContextResult]()
    val res = exec.execute(p.toCode)
    inputPrettyPrints.zip(res.items).foreach{ ios =>
      debugLines += ContextResult(ios._1,ios._2.map(v => v.prettyPrint).getOrElse("ERROR"))
    }

    if (p.isInstanceOf[AggregatorMethod]){
      val lhs_vals = exec.execute(p.children(0).get)
      val (newValues,newPrettyPrints) = getCallbackRunValues(
        lhs_vals.items.map(lhs => lhs.map(l => l.toCode).getOrElse("undefined")),
        exec.inputVals,
        exec.varNames,
        inputPrettyPrints,
        p.func,
        p.children(1).get,
        p.asInstanceOf[AggregatorMethod].innerVar,
        p.children.drop(2).flatten.toSeq
      )
      val newExec = new ExecutorWithVal(newValues,exec.varNames ++ p.asInstanceOf[AggregatorMethod].innerVar)
      DebugInfo(Context(exec.varNames,debugLines.toList), innerAccumulate(p.children(0).get,exec, inputPrettyPrints) :: innerAccumulate(p.children(1).get,newExec,newPrettyPrints) +: p.children.drop(2).flatten.map(c => innerAccumulate(c,exec,inputPrettyPrints)).toList)
    }
    else
      DebugInfo(Context(exec.varNames,debugLines.toList),p.children.flatten.map(c => innerAccumulate(c,exec, inputPrettyPrints)).toList)
  }

  def accumulate(p: ASTNode, inputs : List[String]): DebugInfo  = {
    val exec = new ExecutorWithVal(inputs.map(i => VarInputs(List(i))))
    innerAccumulate(p,exec, inputs.map(i => List(i)))
  }


}
