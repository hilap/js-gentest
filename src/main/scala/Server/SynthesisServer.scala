import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import spray.json.DefaultJsonProtocol._

import akka.http.scaladsl.model.HttpHeader
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.model.headers.`Access-Control-Allow-Credentials`
import akka.http.scaladsl.model.headers.`Access-Control-Allow-Methods`
import akka.http.scaladsl.model.headers.`Access-Control-Allow-Origin`
import akka.http.scaladsl.model.headers.Origin
import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.MethodRejection
import akka.http.scaladsl.server.RejectionHandler

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.io.StdIn

trait CorsSupport {

  protected def corsAllowOrigins: List[String]

  protected def corsAllowedHeaders: List[String]

  protected def corsAllowCredentials: Boolean

  protected def optionsCorsHeaders: List[HttpHeader]

  protected def corsRejectionHandler(allowOrigin: `Access-Control-Allow-Origin`) = RejectionHandler
    .newBuilder().handle {
      case MethodRejection(supported) =>
        complete(HttpResponse().withHeaders(
          `Access-Control-Allow-Methods`(OPTIONS, supported) ::
            allowOrigin ::
            optionsCorsHeaders
        ))
    }
    .result()

  private def originToAllowOrigin(origin: Origin): Option[`Access-Control-Allow-Origin`] =
    if (corsAllowOrigins.contains("*") || corsAllowOrigins.contains(origin.value))
      origin.origins.headOption.map(`Access-Control-Allow-Origin`.apply)
    else
      None

  def cors[T]: Directive0 = mapInnerRoute { route => context =>
    ((context.request.method, context.request.header[Origin].flatMap(originToAllowOrigin)) match {
      case (OPTIONS, Some(allowOrigin)) =>
        handleRejections(corsRejectionHandler(allowOrigin)) {
          respondWithHeaders(allowOrigin, `Access-Control-Allow-Credentials`(corsAllowCredentials)) {
            route
          }
        }
      case (_, Some(allowOrigin)) =>
        respondWithHeaders(allowOrigin, `Access-Control-Allow-Credentials`(corsAllowCredentials)) {
          route
        }
      case (_, _) =>
        route
    })(context)
  }
}

object SynthesisServer extends CorsSupport {

  override val corsAllowOrigins: List[String] = List("*")
  override val corsAllowedHeaders: List[String] = List("Origin", "X-Requested-With", "Content-Type", "Accept", "Accept-Encoding", "Accept-Language", "Host", "Referer", "User-Agent")

  override val corsAllowCredentials: Boolean = true

  override val optionsCorsHeaders: List[HttpHeader] = List[HttpHeader](
      `Access-Control-Allow-Headers`(corsAllowedHeaders.mkString(", ")),
      `Access-Control-Max-Age`(60 * 60 * 24 * 20), // cache pre-flight response for 20 days
      `Access-Control-Allow-Credentials`(corsAllowCredentials)
    )

  def main(args: Array[String]) {

    implicit val system = ActorSystem("my-system")
    implicit val materializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.dispatcher


    final case class DebugInfoRequest(program: String, inputs: List[String])
    final case class DebugInfoResponse(programTree : ASTNodeJson.TreeNode, debugValues : DebugInfoJson.DebugInfo)
    final case class SynthesizeRequest(origProgram : String, inputs : List[String], outputs : List[String], holePath : List[Int], retainTrees : List[List[Int]], excludeTrees : List[List[Int]], vocabName : String)
    final case class SynthesizeResponse(success: Boolean, newProgram : Option[ASTNodeJson.TreeNode], debugInfo : Option[DebugInfoJson.DebugInfo])

    implicit val dirFormat = jsonFormat2(DebugInfoRequest)
    implicit val dirsFormat = jsonFormat2(DebugInfoResponse)
    implicit val sreqFormat = jsonFormat7(SynthesizeRequest)
    implicit val sresFormat = jsonFormat3(SynthesizeResponse)

    val vocabs : Map[String,List[ASTNode]] = Map("vocab1" -> ASTNode.fromFile("src/test/resources/vocab2"))//ASTNode.fromFile("src\\test\\resources\\big-lambda-benchmarks\\max-vocab"))
    val vocabConstraints = ConstraintFileReader.readFile("src/test/resources/vocabTypeConstraints")

    val routes = cors {
//      path("") {
//          get {
//            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say hello to akka-http</h1>"))
//          }
//        } ~
//        path("hello") {
//          get {
//            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say goodbye to akka-http</h1>"))
//          }
//        }~
  //      get {
  //        pathPrefix("item" / LongNumber) { id =>
  //          // there might be no item for a given id
  //          val maybeItem: Future[Option[Item]] = fetchItem(id)
  //
  //          onSuccess(maybeItem) {
  //            case Some(item) => complete(item)
  //            case None       => complete(StatusCodes.NotFound)
  //          }
  //        }
  //      }~
        post {
          path("get-debug") {
            entity(as[DebugInfoRequest]) { req =>
              val decons = JSParser.deconstruct(req.program)
              complete(DebugInfoResponse(ASTNodeJson.construct(decons),DebugInfoJson.accumulate(decons,req.inputs)))
            }
          }
        } ~
        post {
          path("synthesize") {
            withRequestTimeout(5.minutes) {
              entity(as[SynthesizeRequest]) { req =>
                val vocab = vocabs(req.vocabName)
                val progTree = JSParser.deconstruct(req.origProgram)
                val res = SingleSubtreeSnipMain.doSynthesize(req.inputs, req.outputs, vocab, vocabConstraints, progTree, req.holePath, req.retainTrees, req.excludeTrees,60.seconds)
                val response = res.map{pOrig =>
                  val p = JSParser.deconstruct(pOrig.toCode) //there might be macros here
                  SynthesizeResponse(true, Some(ASTNodeJson.construct(p)), Some(DebugInfoJson.accumulate(p, req.inputs)))
                }.getOrElse(SynthesizeResponse(false, None, None))
                complete(response)
              }
            }
          }
        }
    }

//    val route =
//      path("") {
//        get {
//          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say hello to akka-http</h1>"))
//        }
//      } ~
//      path("hello") {
//        get {
//          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say goodbye to akka-http</h1>"))
//        }
//      }~
////      get {
////        pathPrefix("item" / LongNumber) { id =>
////          // there might be no item for a given id
////          val maybeItem: Future[Option[Item]] = fetchItem(id)
////
////          onSuccess(maybeItem) {
////            case Some(item) => complete(item)
////            case None       => complete(StatusCodes.NotFound)
////          }
////        }
////      }~
//      post {
//        path("get-debug") {
//          entity(as[DebugInfoRequest]) { req =>
//            val decons = JSParser.deconstruct(req.program)
//            complete(DebugInfoResponse(ASTNodeJson.construct(decons),DebugInfoJson.accumulate(decons,req.inputs)))
//          }
//        }
//      } ~
//      post {
//        path("synthesize") {
//          entity(as[SynthesizeRequest]) {req =>
//            val vocab = vocabs(req.vocabName)
//            val progTree = JSParser.deconstruct(req.origProgram)
//            val res = SingleSubtreeSnipMain.doSynthesize(req.inputs,req.outputs,vocab,progTree,req.holePath,req.retainTrees)
//            val response = res.map(p => SynthesizeResponse(ASTNodeJson.construct(p),DebugInfoJson.accumulate(p,req.inputs))).getOrElse(SynthesizeResponse(null,null))
//            complete(response)
//          }
//        }
//      }

    val bindingFuture = Http().bindAndHandle(routes, "localhost", 8080)

    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}