import ConstraintFileReader.{ConstraintKey, ConstraintValue}
import SubtreeSnipMain.inputOutputs
import com.eclipsesource.v8.V8Value

import scala.collection.mutable
import scala.concurrent.duration._

object SingleSubtreeSnipMain extends App {

  val inputOutputs = MainUtils.readIoFile(args(0))
  val vocab = ASTNode.fromFile(args(1))
  val prog = scala.io.Source.fromFile(args(2)).mkString


  def doMain(vocab : List[ASTNode], inputOutputs : List[Tuple2[String,String]] , prog : String, removePath : List[Int] = Nil, retainPaths : List[List[Int]] = Nil): Unit = {

    print("Trying to fix ")
    println(prog)
    val inputProg = JSParser.deconstruct(prog)

    val res = doSynthesize(inputOutputs.map(x => x._1),inputOutputs.map(x => x._2),vocab,Map.empty,inputProg,removePath,retainPaths)
  }

  def doSynthesize(inputStrings : List[String],
                   outputStrings: List[String],
                   vocab: List[ASTNode],
                   constraints: Map[ConstraintKey,ConstraintValue],
                   inputProg: ASTNode,
                   holePath: List[Int],
                   retainPaths: List[List[Int]],
                   excludePaths: List[List[Int]] = Nil,
                   duration: FiniteDuration = 10.hours
                  ) : Option[ASTNode] = {
    val mainExec = new ExecutorWithVal(inputStrings.map(x => VarInputs(List(x))))
    val executorNoVal = new ExecutorNoVal
    val outexec = outputStrings.map(io => executorNoVal.execute(io).get.head)
    val outputs: List[FromJSWrapper] = outexec
    //retain what needs retaining
    val additionalVocabRetain = (for (retainPath <- retainPaths; if (retainPath.startsWith(holePath))) yield inputProg.getAt(retainPath).map(_.deepClone)).flatten
    //additionalVocabRetain.foreach(n => n.setRetain())
    val nonNspPredicates = additionalVocabRetain.map(r => RetainPredicate(r))

    val excludes = (for (excludePath <- excludePaths; if (excludePath.startsWith(holePath))) yield inputProg.getAt(excludePath).map(_.deepClone)).flatten
    val nspPredicates = excludes.filter(e => e.height > 1).map(e => ExcludePredicate(e))
    //TODO: try with deeper than one removeList
    val inputMinus : Option[ASTNode] =
      if (holePath.isEmpty) None
      else {
        val clone = inputProg.deepClone
        clone.getAt(holePath.dropRight(1)).foreach{ n => n.removeChild(holePath.last)}
        Some(clone)
      }


    val iterRoot = inputProg.getAt(holePath).get
    val (ctxVarnames,varValues) = if (inputMinus.isEmpty)
      //(0 until mainExec.varNames.length).map(i => mainExec.varNames(i) -> mainExec.inputVals(i)).toMap /*just inputs*/
      (mainExec.varNames, mainExec.inputVals)
      else SubtreeSnipMain.hookAllValues(inputMinus.get,iterRoot,holePath, mainExec)
    if (varValues.isEmpty) return None

    println(inputMinus.map(_.toCode).getOrElse("?"))
    print("Replacing ")
    println(iterRoot.toCode)
    print("Possible var values: ")
    println(varValues)
    val new_max = iterRoot.height + 3
    print("Max height searched for: ")
    println(new_max)
    val ctxVars = inputMinus.map(_.accumulateContextVars(holePath).map(x => Literal(x))).getOrElse(Nil)
    val reducedRemove : Option[ASTNode] = if (retainPaths.contains(List())) None else Some(iterRoot.deepClone)
    retainPaths.filter(p => p.startsWith(holePath)).map(p => p.drop(holePath.length)).
      foreach(p => reducedRemove.foreach(r => r.getAt(p.dropRight(1)).map(h => p.lastOption.map(l => h.removeChild(l)))))
    val additionalVocab = (reducedRemove.map(r => r.toNodeList).getOrElse(Set()) ++ reducedRemove.map(r => r.toListOfFullNodes.filter(_.isFull)).getOrElse(Set()) ++ ctxVars ++ additionalVocabRetain) -- excludes.filter(e => e.height == 1)
    println("Additional vocab elements: " + additionalVocab.map(_.toCode).mkString("{",",","}"))

    val exec = new ExecutorWithVal(varValues,ctxVarnames)
    val valuesPolicy = new ValuesRetainPolicy(exec,nonNspPredicates) //OE on all scoped variables
    val enum = new Enumerator((additionalVocab.toList ++ vocab).diff(excludes.filter(e => e.height == 1)), valuesPolicy, new_max, nspPreds = nspPredicates,nonNspPreds = nonNspPredicates,constraints = constraints)
    var i = 0
    val seenProgs = mutable.Set[String](inputProg.toCode)
    val deadline = duration.fromNow
    for (newSubtree <- enum) {
      val newProg = if (inputMinus.isEmpty) newSubtree
        else {
          val r = inputMinus.get.deepClone
          r.addAt(holePath,newSubtree)
          r
        }

      if (!seenProgs.contains(newProg.toCode)) {
        seenProgs += newProg.toCode
        i = i + 1
        if (i % 100 == 0) {
          println(i.toString + "(" + enum.currLevel + "): " + newProg.toCode)
        }
        //test newProg
        if (nonNspPredicates.forall(p => p(newProg))) {
          val execed = mainExec.execute(newProg)
          val res: List[FromJSWrapper] = execed.get
          //println(res)
          if (res == outputs) {
            print("Found program\nSeen programs: ")
            println(i)
            println(newProg.toCode)
            enum.releaseValues()
            //outputs.foreach(o => o.release())
            execed.release()
            V8Runtime.release()
            return Some(newProg)
          }
        }
      }
      if (!deadline.hasTimeLeft()) {
        print("Timeout after ")
        print(i)
        println(" programs")
        return None
      }
    }
    print("New programs seen for sketch: ")
    println(i)
    println("--")



    V8Runtime.release()
    None
  }

  doMain(vocab,inputOutputs,prog)
}
