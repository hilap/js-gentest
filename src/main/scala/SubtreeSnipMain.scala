import SubtreeSnipMain.inputOutputs
import com.eclipsesource.v8.{V8ScriptExecutionException, V8Value}

import scala.collection.mutable

object SubtreeSnipMain extends App {

  val inputOutputs = MainUtils.readIoFile(args(0))
  val vocab = ASTNode.fromFile(args(1))
  val prog = scala.io.Source.fromFile(args(2)).mkString

  def hookAllValues (reducedProg : ASTNode, origSubtree : ASTNode, path : List[Int], exec : ExecutorWithVal) = {
    val inputVals : List[VarInputs] = exec.inputVals
    val varsList = "input" +: reducedProg.accumulateContextVars(path).toSet.toList
    val clone = reducedProg.deepClone
    clone.addAt(path,Literal("(f(" + varsList.mkString(",") + ")," + origSubtree.toCode + ")"))
    val code = "out = [" + varsList.map(x => "[]").mkString(",") + "]\n" +
               "function f(" + varsList.mkString(",") + ") {" +
                  varsList.zipWithIndex.map(v => "if (" + v._1 + "!== undefined) out[" + v._2 + "].push("+ExecUtils.resultWrapper(v._1)+");").mkString("\n","\n","\n") +
               "}\n" + clone.toCode + ";\n"


    //"if (typeof x" + i.toString + " != 'undefined') out[" + i.toString + "].push(JSON.stringify(x" + i.toString + "))
    val res = try {
      V8Runtime.runArrayAccumulator(code,exec.varNames,inputVals,varsList.length,"out")
    }
    catch {
      case err : V8ScriptExecutionException => {
        //handle the case where there's an error within the hooked part
        val oneOverHole = clone.getAt(path.dropRight(1)).getOrElse(clone)
        //path.lastOption.foreach(i => clone.removeChild(i))
        //clone.addAt(path,Literal("f(" + varsList.mkString(",") + ")"))

        val backupCode = "out = [" + varsList.map(x => "[]").mkString(",") + "]\n" +
          "function f(" + varsList.mkString(",") + ") {" +
          varsList.zipWithIndex.map(v => "if (" + v._1 + "!== undefined) out[" + v._2 + "].push("+ExecUtils.resultWrapper(v._1)+");").mkString("\n","\n","\n") +
          "}\n" + oneOverHole.toCode + ";\n"

        try {V8Runtime.runArrayAccumulator(backupCode,exec.varNames,inputVals,varsList.length,"out")}
        catch {
          case err : V8ScriptExecutionException => Nil
        }
      }
    }
    val vals = res.flatMap(r => r.map(w => VarInputs(w.map(_.toCode))))
    (varsList,vals)
  }


  def doMain(vocab : List[ASTNode], inputOutputs : List[Tuple2[String,String]] , prog : String): Unit = {

    print("Trying to fix ")
    println(prog)
    val mainExec = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    val outputs: List[FromJSWrapper] = outexec
    outexec.foreach(e => if (e.isInstanceOf[V8Value]) e.asInstanceOf[V8Value].release())

    val inputProg = JSParser.deconstruct(prog)
    val seenProgs = mutable.Set[String](inputProg.toCode)


    for (iterRoot <- inputProg.walkAllNodes()) {
      val (reduced, path) = inputProg.deepCloneUntil(iterRoot)
      val (ctxVarnames,varValues) = hookAllValues(reduced.get,iterRoot,path, mainExec)
      println(reduced.map(_.toCode).getOrElse("?"))
      print("Replacing ")
      println(iterRoot.toCode)
      print("Possible var values: ")
      println(varValues)
      val new_max = iterRoot.height + 2
      print("Max height searched for: ")
      println(new_max)
      val ctxVars = reduced.map(_.accumulateContextVars(path).map(x => Literal(x))).getOrElse(Nil)
      val additionalVocab = iterRoot.toNodeList ++ iterRoot.toListOfFullNodes ++ ctxVars
      println("Additional vocab elements: " + additionalVocab.map(_.toCode).mkString("{",",","}"))
      //val ctxVarnames = varValues.keys.toList
      val exec = new ExecutorWithVal(varValues,ctxVarnames)
      val retain = new ValuesRetainPolicy(exec,Nil) //OE on all scoped variables
      val enum = new Enumerator(vocab ++ additionalVocab, retain, new_max)
      var i = 0
      for (newSubtree <- enum) {
        val newProg = if (reduced.isEmpty) newSubtree
          else {
            val r = reduced.get.deepClone
            r.addAt(path,newSubtree)
            r
          }

        if (!seenProgs.contains(newProg.toCode)) {
          seenProgs += newProg.toCode
          i = i + 1
          //test newProg
          val execed = mainExec.execute(newProg)
          val res: List[FromJSWrapper] = execed.get
          //println(res)
          if (res == outputs) {
            print("Found program\nSeen programs: ")
            println(i)
            println(newProg.toCode)
            enum.releaseValues()
            //outputs.foreach(o => o.release())
            execed.release()
            V8Runtime.release()
            return
          }
        }
      }
      print("New programs seen for sketch: ")
      println(i)
      println("--")


    }
    V8Runtime.release()
  }

  doMain(vocab,inputOutputs,prog)
}
