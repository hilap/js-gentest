import com.eclipsesource.v8.V8
import scala.collection.mutable

/**
 * Created by hila on 26/03/2018.
 */
object V8Runtime {
 private var rt = new ThreadLocal[V8]
 def runtime = {
  if (rt.get() == null) {
    val newRt = V8.createV8Runtime()
    newRt.executeScript(globalDefs)
    rt.set(newRt)
  }
  rt.get()
 }
 def release() : Unit = {
  runtime.release()
  rt.remove()
  //rt.set(V8.createV8Runtime())
 }

 val globalDefs : String = List(
   """sortedJsonify = (tojson) => JSON.stringify(tojson, function(key,value) {
     |    if (typeof value == 'object'){
     | 		  if (Array.isArray(value)) return value;
     |	  	else {
     |          const ordered = {};
     |          Object.keys(value).sort().forEach(function(k) {
     |            ordered[k] = value[k];
     |          });
     |		  return ordered;
     |      }
     |    }
     |	  else return value;
     |})""".stripMargin,
   "undefined"
 ).mkString(";\n")

 def runArrayAccumulator(accumulatorCode : String,
                         inVarNames : List[String],
                         inVarValues : List[VarInputs],
                         outVarNum : Int,
                         accumulationVarName : String) = {
  val values : List[mutable.ListBuffer[List[FromJSWrapper]]] = inVarValues.map(x => mutable.ListBuffer[List[FromJSWrapper]]())

  val runtime = V8Runtime.runtime//TODO: par?

  for(vars <- inVarValues.zipWithIndex){
   val out = runtime.executeArrayScript("{\n" +
     inVarNames.zipWithIndex.map(v =>  v._1 + " = " + vars._1.inputs(v._2) + ";").mkString("\n") +
     accumulatorCode + "\n" +
     inVarNames.map(v => "delete " + v + ";").mkString("\n") +
     accumulationVarName +
     "\n}");
   val newValues = (0 until outVarNum).map(v => mutable.Set[FromJSWrapper]()).toList
   for (i <- 0 until outVarNum) {
    val paramVals = out.getArray(i)
    for (j <- 0 until paramVals.length()) {
     val v = paramVals.getArray(j)
     newValues(i).add(FromJSWrapper(v))
    }
    paramVals.release()
   }
   out.release()
   Utils.cross(newValues.map(_.toList)).foreach{nv => values(vars._2) += nv}
  }
  values.map(_.toList)
 }
}
