import DataType.DataType
import com.eclipsesource.v8._

import scala.collection.mutable

/**
 * Created by hila on 02/03/2018.
 */

object DataType extends Enumeration {
  type DataType = Value
  val number, boolean, string, undefined, obj, array, map = Value
}

trait FromJSWrapper{
  def value : Any;
  def toCode : String
  def prettyPrint : String
  def getType : DataType
}

object FromJSWrapper {
  def apply(toWrap : V8Array) : FromJSWrapper = {
    val typeStr = toWrap.getString(1)
    val res = typeStr match {
      case "number" | "boolean" | "string" => NonV8AnyRef(toWrap.get(0))
      case "undefined" => JSUndefined()
      case "function" => JSUndefined()
      case "object" => JSValueWrapper(toWrap.getString(0))
      case "array" => JSArrayWrapper(toWrap.getString(0))
      case "map" => JSMapWrapper(toWrap.getString(0))
    }
    toWrap.release()
    res
  }
}

case class NonV8AnyRef(wrapped : AnyRef) extends FromJSWrapper {
  override def equals(obj: Any): Boolean = obj match {
    case NonV8AnyRef(rhs) => {
      if (wrapped.isInstanceOf[java.lang.Double] && rhs.isInstanceOf[java.lang.Double]) {
        val double1 = wrapped.asInstanceOf[java.lang.Double]
        val double2 = rhs.asInstanceOf[java.lang.Double]
        if (double1.isNaN && double2.isNaN) true
        else double1 == double2
      }
      else wrapped == rhs
    }
    case _ => false
  }

  override def value: Any = wrapped

  override def toCode: String = if (wrapped.isInstanceOf[String] || wrapped.isInstanceOf[java.lang.String]) "'" + MainUtils.escape(wrapped.toString) + "'"
    else wrapped.toString

  override def prettyPrint: String = toCode

  override def getType : DataType = wrapped match {
    case _: java.lang.Boolean => DataType.boolean
    case _ : String | _: java.lang.String => DataType.string
    case _ => DataType.number
  }
}
case class JSValueWrapper(jsonifiedValue : String) extends FromJSWrapper{
  override def value: Any = jsonifiedValue

  override def toCode: String = "JSON.parse('" + MainUtils.escape(jsonifiedValue) + "')"

  override def prettyPrint: String = jsonifiedValue

  override def getType : DataType = DataType.obj
}

case class JSArrayWrapper(jsonifiedValue : String) extends FromJSWrapper{
  override def value : Any = jsonifiedValue

  override def toCode: String = "JSON.parse('" + MainUtils.escape(jsonifiedValue) + "')"

  override def prettyPrint: String = jsonifiedValue

  override def getType : DataType = DataType.array
}

case class JSMapWrapper(jsonifiedValue: String) extends FromJSWrapper{
  override def value: Any = jsonifiedValue

  override def toCode: String = "new Map(JSON.parse('" + jsonifiedValue + "'))"

  override def prettyPrint: String = "Map(" + jsonifiedValue + ")"

  override def getType : DataType = DataType.map
}
case class JSUndefined() extends FromJSWrapper {
  override def value: Any = ???

  override def toCode: String = "undefined"

  override def prettyPrint: String = "undefined"

  override def getType : DataType = DataType.undefined
}

//object JSAnyRefWrapper {
//  implicit def anyref2Wrapped(toWrap : AnyRef) = new JSAnyRefWrapper(toWrap)
//  implicit def list2Wrapped(toWrap : List[AnyRef]) : List[JSAnyRefWrapper] = toWrap.map(x => anyref2Wrapped(x))
//}

class ValuesRetainPolicy(val executor : Executor, val nonNspPredicates : List[ProgramPredicate]) extends RetainPolicy{
    val values : mutable.Set[List[FromJSWrapper]] = mutable.Set()

  case class PredicateResultWrapper(res : Boolean) extends FromJSWrapper {
    override def value: Any = res
    override def toCode: String = ???
    override def prettyPrint: String = ???
    override def getType: DataType = ???
  }

  override def shouldAdd(expr : ASTNode): (Boolean,Set[DataType.DataType]) = {
    val reses = executor.execute(expr)
    val shouldAndTypes: (Boolean,Set[DataType.DataType]) = if (reses.emptyOrUndefined()) {
      (false,Set())
    }
//    else if (expr.isRetain()) {
//      (true,reses.get.map(w => w.getType).toSet)
//    }
    else {
      val predicateResults = nonNspPredicates.map(p => p(expr)).map(PredicateResultWrapper)
      val res = values.add(reses.get ++ predicateResults)
      (res,reses.get.map(w => w.getType).toSet)
    }
    reses.release()
    shouldAndTypes
  }

  def releaseValues() = {
    //val valsList = values.toList
    values.clear()
    //for(v <- valsList; e <- v) e.release()
  }
}

class ResetInputsRetainPolicy(val innerVars : List[String], val outerVars : List[String], val outerVarsVals : List[VarInputs], nonNspPredicates: List[ProgramPredicate]) extends RetainPolicy {

  //var value_jsons  = List[List[String]]()
  var innerPolicy = new ValuesRetainPolicy(new ExecutorWithVal(List(),outerVars ++ innerVars),nonNspPredicates)
  def reset(new_value_tuples: List[VarInputs]) = {
    //value_jsons = new_value_jsons
    innerPolicy.releaseValues()
    innerPolicy = new ValuesRetainPolicy(new ExecutorWithVal(new_value_tuples/*outerVarsVals ++ value_jsons*/,outerVars ++ innerVars),nonNspPredicates)
  }

  override def shouldAdd(expr: ASTNode): (Boolean,Set[DataType.DataType]) = innerPolicy.shouldAdd(expr)

  override def releaseValues(): Unit = innerPolicy.releaseValues()
}