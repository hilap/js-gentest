package vocabconfig.lexer

import vocabconfig.parser.{Location, VocabLexerError}

import scala.util.parsing.combinator.RegexParsers


import scala.util.parsing.input.Positional

sealed trait VocabToken extends Positional

case class IDENTIFIER(str: String) extends VocabToken
case class LITERAL(str: String) extends VocabToken
case class OP(str: String) extends VocabToken
case class QMARK() extends VocabToken
case class DOUBLEQMARK() extends VocabToken
case class LPAR() extends VocabToken
case class RPAR() extends VocabToken
case class NEW() extends VocabToken
case class DOT() extends VocabToken
case class COMMA() extends VocabToken
case class ARROW() extends VocabToken
case class LBRAC() extends VocabToken
case class RBRAC() extends VocabToken
case class COLON() extends VocabToken
case class LBRACE() extends VocabToken
case class RBRACE() extends VocabToken

object VocabLexer extends RegexParsers {
  override def skipWhitespace = true

  def apply(code: String): Either[VocabLexerError, List[VocabToken]] = {
    parse(tokens, code) match {
      case NoSuccess(msg, next) => Left(VocabLexerError(Location(next.pos.line, next.pos.column), msg))
      case Success(result, next) => Right(result)
    }
  }

  def tokens: Parser[List[VocabToken]] = {
    phrase(rep1(lpar | rpar | dqmark | qmark | newkeyword | dot | arrow
      | comma | lbrac | rbrac | colon | lbrace | rbrace | strLiteral | numLiteral | identifier | op))
  }



  def identifier: Parser[IDENTIFIER] = positioned {
    "[a-zA-Z_][a-zA-Z0-9_]*".r ^^ { str => IDENTIFIER(str) }
  }

  def strLiteral: Parser[LITERAL] = positioned {
    "'[^']*'".r ^^ { str =>      LITERAL(str)    }
  }

  def numLiteral: Parser[LITERAL] = positioned{
    "(-)?[0-9]+(\\.[0-9]+)?".r ^^ { str => LITERAL(str)}
  }

  def op: Parser[OP] = {
    "[~!%^&*+\\-+=/|<>]+".r ^^ { str => OP(str) }
  }


  def lpar = positioned{"(" ^^ (_ => LPAR())}
  def rpar = positioned{")" ^^ (_ => RPAR())}
  def qmark = positioned{"?" ^^ (_ => QMARK())}
  def dqmark = positioned{"??" ^^ (_ => DOUBLEQMARK())}
  def newkeyword = positioned{"new" ^^ (_ => NEW())}
  def dot = positioned{"." ^^ (_ => DOT())}
  def arrow         = positioned { "=>"            ^^ (_ => ARROW()) }
  def comma         = positioned { ","             ^^ (_ => COMMA()) }
  def lbrac = positioned {"[" ^^ (_ => LBRAC())}
  def rbrac = positioned {"]" ^^ (_ => RBRAC())}
  def colon = positioned{":" ^^ (_ => COLON())}
  def lbrace = positioned("{" ^^ (_ => LBRACE()))
  def rbrace = positioned("}" ^^ (_ => RBRACE()))
}