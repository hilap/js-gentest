package vocabconfig.parser

import vocabconfig.lexer._

import scala.util.parsing.input.Positional



sealed trait VocabCompilationError

case class VocabLexerError(location: Location, msg: String) extends VocabCompilationError
case class VocabParserError(location: Location, msg: String) extends VocabCompilationError

case class Location(line: Int, column: Int) {
  override def toString = s"$line:$column"
}

sealed trait VocabAST extends Positional
case class LiteralNode(str : String) extends VocabAST
case class NewNode(clsName : String,args : ParamListNode) extends VocabAST
case class FuncNode(fname : String, args : ParamListNode) extends VocabAST
case class McallNode(fname : String, args : ParamListNode) extends VocabAST
case class ParamListNode(params : List[ParamNode]) extends VocabAST

trait ParamNode extends VocabAST
case class ReqParamNode() extends ParamNode
case class OptParamNode() extends ParamNode
case class CallbackParamNode(args : ParamListNode) extends ParamNode
case class OptCallbackParamNode(args : ParamListNode) extends ParamNode

case class BinopNode(op: String) extends VocabAST
case class PrefopNode(op : String) extends VocabAST
case class PostopNode(op : String) extends VocabAST

case class ArrDerefNode() extends VocabAST
case class PropCallNode(propName : String) extends VocabAST
case class ObjInitNode(numPairs : Int) extends VocabAST

import scala.util.parsing.combinator.Parsers
import scala.util.parsing.input.{NoPosition, Position, Reader}

object VocabParser extends Parsers {
  override type Elem = VocabToken

  class VocabTokenReader(tokens: Seq[VocabToken]) extends Reader[VocabToken] {
    override def first: VocabToken = tokens.head
    override def atEnd: Boolean = tokens.isEmpty
    override def pos: Position = tokens.headOption.map(_.pos).getOrElse(NoPosition)
    override def rest: Reader[VocabToken] = new VocabTokenReader(tokens.tail)
  }


  def apply(tokens: Seq[VocabToken]): Either[VocabParserError, VocabAST] = {
    val reader = new VocabTokenReader(tokens)
    line(reader) match {
      case NoSuccess(msg, next) => Left(VocabParserError(Location(next.pos.line, next.pos.column), msg))
      case Success(result, next) => Right(result)
    }
  }

  def apply(code: String): Either[VocabCompilationError, VocabAST] = {
    for {
      tokens <- VocabLexer(code).right
      ast <- VocabParser(tokens).right
    } yield ast
  }

  def line: Parser[VocabAST] = positioned {
    phrase(statement)
  }

  def statement: Parser[VocabAST] = positioned {

    val varlit = identifier ^^ { id => LiteralNode(id.str)}

    val lit = literal ^^ {lit => LiteralNode(lit.str)}

    val arraylit = (LBRAC() ~ repsep(literal,COMMA()) ~ RBRAC() ^^ {
      case _ ~ list ~ _ => LiteralNode(list.map(_.str).mkString("[",",","]"))
    })

    val ctor = NEW() ~ identifier ~ opt(paramList) ^^ {
      case _ ~ clsName ~ None => NewNode(clsName.str, ParamListNode(List()))
      case _ ~ clsName ~ Some(args) => NewNode(clsName.str,args)
    }

    val funcCall = rep(identifier ~ DOT()) ~ identifier ~ paramList ^^ { case fqList ~ clsName ~ args => FuncNode(fqList.map(id => id._1.str + ".").mkString("") + clsName.str,args)}

    val methodCall = QMARK() ~ DOT() ~ funcCall ^^ {case _ ~ _ ~ func => McallNode(func.fname,func.args)}

    val binop = QMARK() ~ op ~ QMARK() ^^ {case _ ~ op ~ _ => BinopNode(op.str)}
    val prefop = op ~ QMARK() ^^ {case op ~ _ => PrefopNode(op.str)}
    val postop = QMARK() ~ op ^^ {case _ ~ op => PostopNode(op.str)}
    val arrderef = QMARK() ~ LBRAC() ~ QMARK() ~ RBRAC() ^^ {_ => ArrDerefNode()}
    val propcall = QMARK() ~ DOT() ~ identifier ^^ {case _ ~ _ ~ id => PropCallNode(id.str)}

    val objInitializer = LBRACE() ~ rep1sep(QMARK() ~ COLON() ~ QMARK(),COMMA()) ~ RBRACE() ^^ {case _ ~ kvList ~ _ => ObjInitNode(kvList.length)}

    funcCall | methodCall | binop | prefop | postop | arrderef | propcall | varlit | lit | arraylit | ctor | objInitializer
  }


  def paramList = (LPAR()~ repsep(reqParam,COMMA()) ~ opt(COMMA() ~ repsep(optParam, COMMA())) ~ RPAR() ^^ {
    case _ ~ list ~ None ~ _ => ParamListNode(list)
    case _ ~ list ~ Some(_ ~ optionals) ~ _ => ParamListNode(list ++ optionals)
  }) | (LPAR() ~ rep1sep(optParam,COMMA()) ~ RPAR() ^^ {case _ ~ list ~ _ => ParamListNode(list)})

  def reqParam : Parser[ParamNode] =
      (QMARK() ~ ARROW() ~ QMARK() ^^ {_ => CallbackParamNode(ParamListNode(List(ReqParamNode())))} ) |
      (LPAR() ~ innerParamList ~ RPAR() ~ ARROW() ~ QMARK() ^^ {case _ ~ list ~ _ ~ _ ~ _ => CallbackParamNode(list)}) |
      (QMARK() ^^ {_ => ReqParamNode()})

  def optParam : Parser[ParamNode] = (QMARK() ~ ARROW() ~ DOUBLEQMARK() ^^ {_ => OptCallbackParamNode(ParamListNode(List(ReqParamNode())))} ) |
    (LPAR() ~ innerParamList ~ RPAR() ~ ARROW() ~ DOUBLEQMARK() ^^ {case _ ~ list ~ _ ~ _ ~ _ => OptCallbackParamNode(list)}) |
    (DOUBLEQMARK() ^^ {_ => OptParamNode()})

  def innerParamList : Parser[ParamListNode] = repsep(QMARK(),COMMA()) ~ opt(COMMA() ~ rep1sep(DOUBLEQMARK(),COMMA())) ^^ {
    case list ~ None => ParamListNode(list.map(_ => ReqParamNode()))
    case list ~ Some(_ ~ optsList) => ParamListNode(list.map(_ => ReqParamNode()) ++ optsList.map(_ => OptParamNode()))
  }

  private def identifier: Parser[IDENTIFIER] = positioned {
    accept("identifier", { case id @ IDENTIFIER(name) => id })
  }

  private def literal: Parser[LITERAL] = positioned {
    accept("literal", { case lit @ LITERAL(name) => lit })
  }

  private def op : Parser[OP] = positioned{
    accept("op",{case op @ OP(o) => op})
  }

}