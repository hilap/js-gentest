import Enumerator.SeenProgram
import org.junit.Test
import org.junit.Assert._
import org.scalatest.junit.JUnitSuite

/**
 * Created by hila on 26/03/2018.
 */
class AggsParamIterTests extends JUnitSuite{

  @Test def enumParams1() : Unit = {
    val api = new AggregatorParamsIter(
      "map",
      List[Enumerator.SeenProgram](Enumerator.SeenProgram(Literal("2"),Set(DataType.number)),Enumerator.SeenProgram(Literal("[1,2,3]"),Set(DataType.array))),
      List[ASTNode](Literal("a"),ASTNode("?+?")),
      new ResetInputsRetainPolicy(List("a"),List("input"),List(VarInputs(List("null"))),Nil),
      new ExecutorWithVal(List(VarInputs(List("null")))),
      2, //lhs + func args
      0,
      0,
      Nil,
      Nil,
      Map.empty
    )
    assertEquals("[1,2,3]",api.lhs.toCode)
    assertEquals("a",api.next()(1).toCode)
  }

  @Test def enumParams2() : Unit = {
    val concat = ASTNode("?.concat(?)")
    concat.addChild(0,Literal("input"))
    concat.addChild(1,Literal("input"))
    val lhs = List(Enumerator.SeenProgram(concat,Set()))

    val api = new AggregatorParamsIter(
      "map",
      lhs,
      ASTNode.parseList("""1
                          |[]
                          |? + ?
                          |? - ?
                          |?.concat(?)
                          |a""".stripMargin),
      new ResetInputsRetainPolicy(List("a"),List(),List(),Nil),
      new ExecutorWithVal(List(VarInputs(List("[1,2,3,4]")))),
      2,
      0,
      0,
      Nil,
      Nil,
      Map.empty
    )

    while (api.hasNext) {
      api.next()
    }
    assertFalse(api.hasNext)
  }

  @Test def getCallbackValues1Param() : Unit = {
    val runtime = V8Runtime.runtime
    val array = List(JSArrayWrapper( "[1,3,5,7,9]"))
    val res = AggregatorParamsIter.getCallbackValues(array,array.map(x => VarInputs(List(x.jsonifiedValue))),List("input"),"map",1)
    //array.foreach(_.release())
    assertEquals(5,res.length)
    assertEquals(Set(VarInputs(List("[1,3,5,7,9]","1")),VarInputs(List("[1,3,5,7,9]","3")),VarInputs(List("[1,3,5,7,9]","5")),VarInputs(List("[1,3,5,7,9]","7")),VarInputs(List("[1,3,5,7,9]","9"))),res.toSet)
    V8Runtime.release()
  }

  @Test def getCallbackValues2ParamMap() : Unit = {
    val runtime = V8Runtime.runtime
    val array = List(JSArrayWrapper("[1,3,5,7,9]"))
    val res = AggregatorParamsIter.getCallbackValues(array,array.map(x => VarInputs(List(x.jsonifiedValue))),List("input"),"map",2)
    //array.foreach(_.release())
    assertEquals(25,res.length)
    assertEquals(Set("1","3","5","7","9"),res.map(x => x.inputs(1)).toSet)
    assertEquals(Set("0","1","2","3","4"),res.map(x => x.inputs(2)).toSet)
    V8Runtime.release()
  }

  @Test def getCallbackValues2ParamMapStrs() : Unit = {
    val runtime = V8Runtime.runtime
    val array = List(JSArrayWrapper("[1,\"a\",5,\"c\",9]"))
    val res = AggregatorParamsIter.getCallbackValues(array,array.map(x => VarInputs(List(x.jsonifiedValue))),List("input"),"map",2)
    //array.foreach(_.release())
    assertEquals(25,res.length)
    assertEquals(Set("1","'a'","5","'c'","9"),res.map(x => x.inputs(1)).toSet)
    assertEquals(Set("0","1","2","3","4"),res.map(x => x.inputs(2)).toSet)
    V8Runtime.release()
  }

  @Test def getCallbackValues2ParamMap2Inputs() : Unit = {
    val runtime = V8Runtime.runtime
    val array = List(JSArrayWrapper("[1,\"a\",5,\"c\",9]"),JSArrayWrapper("[20,6,\"a\",\"cde\",2]"))
    val res = AggregatorParamsIter.getCallbackValues(array,array.map(x => VarInputs(List(x.jsonifiedValue))),List("input"),"map",2)
    assertEquals(50,res.length)
    assertEquals(Set("1","'a'","5","'c'","9","20","6","'cde'","2"),res.map(x => x.inputs(1)).toSet)
    assertEquals(Set("0","1","2","3","4"),res.map(x => x.inputs(2)).toSet)
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","1", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","1", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","1", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","1", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","1", "4" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'a'", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'a'", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'a'", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'a'", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'a'", "4" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","5", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","5", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","5", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","5", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","5", "4" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'c'", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'c'", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'c'", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'c'", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","'c'", "4" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","9", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","9", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","9", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","9", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,\"a\",5,\"c\",9]","9", "4" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","20", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","20", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","20", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","20", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","20", "4" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","6", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","6", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","6", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","6", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","6", "4" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'a'", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'a'", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'a'", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'a'", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'a'", "4" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'cde'", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'cde'", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'cde'", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'cde'", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","'cde'", "4" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","2", "0" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","2", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","2", "2" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","2", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[20,6,\"a\",\"cde\",2]","2", "4" ))))

    V8Runtime.release()
  }

  @Test def getCallbackValues2ParamSort() : Unit = {
    val array = List(JSArrayWrapper("[1,3,5,7,9]"))
    val res = AggregatorParamsIter.getCallbackValues(array,array.map(x => VarInputs(List(x.jsonifiedValue))),List("input"),"sort",2)
    assertEquals(16,res.length)
    assertEquals(Set(VarInputs(List("[1,3,5,7,9]", "1" , "3" )),
      VarInputs(List("[1,3,5,7,9]", "1" , "5" )),
      VarInputs(List("[1,3,5,7,9]", "1" , "7" )),
      VarInputs(List("[1,3,5,7,9]", "1" , "9" )),
      VarInputs(List("[1,3,5,7,9]", "3" , "3" )),
      VarInputs(List("[1,3,5,7,9]", "3" , "5" )),
      VarInputs(List("[1,3,5,7,9]", "3" , "7" )),
      VarInputs(List("[1,3,5,7,9]", "3" , "9" )),
      VarInputs(List("[1,3,5,7,9]", "5" , "3" )),
      VarInputs(List("[1,3,5,7,9]", "5" , "5" )),
      VarInputs(List("[1,3,5,7,9]", "5" , "7" )),
      VarInputs(List("[1,3,5,7,9]", "5" , "9" )),
      VarInputs(List("[1,3,5,7,9]", "7" , "3" )),
      VarInputs(List("[1,3,5,7,9]", "7" , "5" )),
      VarInputs(List("[1,3,5,7,9]", "7" , "7" )),
      VarInputs(List("[1,3,5,7,9]", "7" , "9" ))),res.toSet)
    V8Runtime.release()
  }

  @Test def skipEmptyLhs() : Unit = {
    val array = List(JSArrayWrapper("[]"))
    val vocab = ASTNode.parseList("""1
                                    |[]
                                    |? + ?
                                    |? - ?
                                    |?.concat(?)
                                    |a""".stripMargin)
    val exec = new ExecutorNoVal
    val paramIter = new AggregatorParamsIter("reduce",List(Enumerator.SeenProgram(Literal("[]"),Set())),vocab,new ResetInputsRetainPolicy(List(),List(),List(),Nil),exec,2,0,0, Nil,Nil,Map.empty)
    assertFalse(paramIter.hasNext)
  }

  @Test def reduceWithTypeConstraints() : Unit = {
    val vocab = ASTNode.parseList("""1
                                    |[1,2,3]
                                    |? + ?""".stripMargin)
    val exec = new ExecutorNoVal
    val paramsIter = new AggregatorParamsIter(
      "reduce",
      List(SeenProgram(Literal("1"),Set(DataType.number)),SeenProgram(Literal("[1,2,3]"),Set(DataType.array))),
      vocab,
      new ResetInputsRetainPolicy(List("x","y"),List("input"),List(VarInputs(List("null"))),Nil),
      exec,
      4,
      0,
      1,
      Nil,
      List(Set(DataType.array),Set(DataType.string,DataType.number)),
      Map.empty
    )
    assertTrue(paramsIter.hasNext)
    //assertEquals("[1,2,3],1,1,1",paramsIter.next.map(_.toCode).mkString(","))
    assertEquals("[1,2,3],1,1,[1,2,3]",paramsIter.next.map(_.toCode).mkString(","))
    //assertEquals("[1,2,3],1,[1,2,3],1",paramsIter.next.map(_.toCode).mkString(","))
    //assertEquals("[1,2,3],1,[1,2,3],[1,2,3]",paramsIter.next.map(_.toCode).mkString(","))
    //assertEquals("[1,2,3],[1,2,3],1,1",paramsIter.next.map(_.toCode).mkString(","))
    assertEquals("[1,2,3],[1,2,3],1,[1,2,3]",paramsIter.next.map(_.toCode).mkString(","))
    //assertEquals("[1,2,3],[1,2,3],[1,2,3],1",paramsIter.next.map(_.toCode).mkString(","))
    //assertEquals("[1,2,3],[1,2,3],[1,2,3],[1,2,3]",paramsIter.next.map(_.toCode).mkString(","))
  }

  @Test def iterateReduceBasedOnInitialParamType() : Unit = {
    val vocab = ASTNode.parseList(
      """?+?
        |?.concat(?)
        |1
        |[1]
        |acc""".stripMargin)

    val exec = new ExecutorNoVal
    val paramsIter = new AggregatorParamsIter(
      "reduce",
      List(SeenProgram(Literal("1"),Set(DataType.number)),SeenProgram(Literal("[1,2,3]"),Set(DataType.array))),
      vocab,
      new ResetInputsRetainPolicy(List("acc","e"),List("input"),List(VarInputs(List("null"))),Nil),
      exec,
      3,
      0,
      1,
      Nil,
      List(Set()),
      //need constraints on plus, concat
      ConstraintFileReader.processAll(
        """+;BinOperator;2;0;boolean,obj,array,map
          |+;BinOperator;2;1;boolean,obj,array,map
          |concat;MethodCall;2;0;number,boolean,obj,string,map
          |concat;MethodCall;2;1;number,boolean,obj,string,map""".stripMargin)
    )

    def nextParamsStr = paramsIter.next().map(_.toCode).mkString(",")
    assertTrue(paramsIter.hasNext)
    assertEquals("[1,2,3],1,1",nextParamsStr)
    assertEquals("[1,2,3],[1],1",nextParamsStr)
    assertEquals("[1,2,3],1 + 1,1",nextParamsStr)
    assertEquals("[1,2,3],[1].concat([1]),1",nextParamsStr)
    assertEquals("[1,2,3],1 + (1 + 1),1",nextParamsStr)
    assertEquals("[1,2,3],(1 + 1) + (1 + 1),1",nextParamsStr)
    assertEquals("[1,2,3],[1].concat([1].concat([1])),1",nextParamsStr)
    assertEquals("[1,2,3],([1].concat([1])).concat([1].concat([1])),1",nextParamsStr)
    assertEquals("[1,2,3],1,[1,2,3]",nextParamsStr)
    assertEquals("[1,2,3],[1],[1,2,3]",nextParamsStr)
    assertEquals("[1,2,3],acc,[1,2,3]",nextParamsStr)
    assertEquals("[1,2,3],1 + 1,[1,2,3]",nextParamsStr)
    assertEquals("[1,2,3],[1].concat([1]),[1,2,3]",nextParamsStr)
    assertEquals("[1,2,3],[1].concat(acc),[1,2,3]",nextParamsStr)
    assertEquals("[1,2,3],acc.concat([1]),[1,2,3]",nextParamsStr)
    assertEquals("[1,2,3],acc.concat(acc),[1,2,3]",nextParamsStr)
  }

  @Test def callbackValuesWithExtraParam() : Unit = {
    //get CB values for input.reduce((x,y) => ?, input)
    val array = List(JSArrayWrapper("[1,3,5,7,9]"))
    val varVals = List(VarInputs(List("[1,3,5,7,9]")))
    val varNames = List("input")
    val res = AggregatorParamsIter.getCallbackValues(
      array,
      varVals,
      varNames,
      "reduce",
      2,
      List(Literal("input"))
    )
    assertEquals(5,res.length)

    //input, acc, e
    assertTrue(res.toSet.contains(VarInputs(List("[1,3,5,7,9]","JSON.parse('[1,3,5,7,9]')", "1" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,3,5,7,9]","JSON.parse('[1,3,5,7,9]')", "3" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,3,5,7,9]","JSON.parse('[1,3,5,7,9]')", "5" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,3,5,7,9]","JSON.parse('[1,3,5,7,9]')", "7" ))))
    assertTrue(res.toSet.contains(VarInputs(List("[1,3,5,7,9]","JSON.parse('[1,3,5,7,9]')", "9" ))))

  }
}
