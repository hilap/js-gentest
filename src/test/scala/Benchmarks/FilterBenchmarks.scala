import SketchBenchmarksDriver.{runBenchmark, vocabulary}

object FilterBenchmarks {
  def noLongWords(): Unit = {
    //https://codingbat.com/prob/p194496
    val sketch = {
      val map = JSParser.deconstruct("input.filter(e => e)")
      map.removeChild(1)
      map.asInstanceOf[AggregatorMethod]
    }

    val examples = List(
      "[\"this\", \"not\", \"too\", \"long\"]" -> "[\"not\", \"too\"]",
      "[\"a\", \"bbb\", \"cccc\"]" -> "[\"a\", \"bbb\"]",
      "[\"cccc\", \"cccc\", \"cccc\"]" -> "[]"
    )

    val res = runBenchmark(sketch, vocabulary, examples,"(e.length) < 4")
    assert(res.toCode == "input.filter(e => (e.length) < 4)")
  }

  def noTeen(): Unit = {
    //https://codingbat.com/prob/p137274
    val sketch = {
      val map = JSParser.deconstruct("input.filter(e => e)")
      map.removeChild(1)
      map.asInstanceOf[AggregatorMethod]
    }

    val examples = List(
      "[12, 13, 19, 20]" -> "[12, 20]",
      "[1, 14, 1]" -> "[1, 1]",
      "[15]" -> "[]"
    )

    val res = runBenchmark(sketch, vocabulary, examples,"(19 < e) + (e < 13)")
    //grumble grumble, not res.toCode == "input.filter(e => (e < 13) || (e > 19))"
    assert(res.toCode == "input.filter(e => (19 < e) + (e < 13))")
  }

  def dedupInOrder(): Unit = {
    val sketch = {
      val map = JSParser.deconstruct("input.filter((e,i,arr) => e)")
      map.removeChild(1)
      map.asInstanceOf[AggregatorMethod]
    }

    val examples = List(
      "[1,1,3,3,5]" -> "[1,3,5]",
      "[ 'i', 'n', 'd', 'i', 'v', 'i', 's', 'i', 'b', 'i', 'l', 'i', 't', 'y' ]" -> "[ 'i', 'n', 'd', 'v', 's', 'b', 'l', 't', 'y' ]",
      "['a','b','c']" -> "['a','b','c']"
    )

    val res = runBenchmark(sketch, vocabulary, examples,"i == (arr.indexOf(e))")
    assert(res.toCode == "input.filter((e,i,arr) => i == (arr.indexOf(e)))")
  }

}
