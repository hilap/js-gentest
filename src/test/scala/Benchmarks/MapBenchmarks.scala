import SketchBenchmarksDriver.{runBenchmark, vocabulary}

object MapBenchmarks {
  def plusOneTimesTen(): Unit = {
    //https://codingbat.com/prob/p103869
    val sketch = {
      val map = JSParser.deconstruct("input.map((e, i) => e)")
      map.removeChild(1)
      map.asInstanceOf[AggregatorMethod]
    }

    val examples = List(
      "[1, 2, 3]" -> "[20, 30, 40]",
      "[6, 8, 6, 8, 1]" -> "[70, 90, 70, 90, 20]",
      "[10]" -> "[110]"
    )

    val res = runBenchmark(sketch, vocabulary, examples,"10 + (10 * e)")
    assert(res.toCode == "input.map((e,i) => 10 + (10 * e))")
  }

  def noX(): Unit = {
    //https://codingbat.com/prob/p105967
    val sketch = {
      val map = JSParser.deconstruct("input.map(e => e)")
      map.removeChild(1)
      map.asInstanceOf[AggregatorMethod]
    }

    val examples = List(
      "[\"ax\", \"bb\", \"cx\"]" -> "[\"a\", \"bb\", \"c\"]",
      "[\"xxax\", \"xbxbx\", \"xxcx\"]" -> "[\"a\", \"bb\", \"c\"]",
      "[\"x\"]" -> "[\"\"]"
    )

    val res = runBenchmark(sketch,vocabulary,examples,"(e.split('x')).join('')")
    assert(res.toCode == "input.map(e => (e.split('x')).join(''))")
  }
}
