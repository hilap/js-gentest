import scala.collection.mutable

object SketchBenchmarksDriver extends App{
  val vocabulary = ASTNode.parseList(
    """? + ?
      |? * ?
      |0
      |1
      |10
      |''
      |'x'
      |? < ?
      |? > ?
      |? || ?
      |? && ?
      |? == ?
      |13
      |19
      |?.replace(?,?)
      |? % ?
      |?.concat(?)
      |Number(?)
      |true
      |false
      |?.split(?)
      |?.join(?)
      |4
      |?.length
      |?.indexOf(?)
      |? - ?""".stripMargin)

//  MapBenchmarks.plusOneTimesTen()
//  MapBenchmarks.noX()
//  FilterBenchmarks.noLongWords()
//  FilterBenchmarks.noTeen()
//  FilterBenchmarks.dedupInOrder()
//  SortBenchmarks.sortNumbers()
  SortBenchmarks.sortLength()

  def runBenchmark(sketch: AggregatorMethod, vocabulary: List[ASTNode], examples: List[(String,String)], expectedSubtree: String): ASTNode = {
    val inputs = examples.map(_._1)
    val outputs = examples.map(_._2)

    val outexec = {
      val executorNoVal = new ExecutorNoVal
      outputs.map(io => executorNoVal.execute(io).get.head)
    }

    val expectedPieces = JSParser.deconstruct(expectedSubtree).walkAllNodes().map(node => node.toCode).toSet - expectedSubtree

    def extend(inputs: List[String], hof: AggregatorMethod): List[VarInputs] = {
      val varNum = if (hof.func == "sort") 1 else hof.innerVar.length
      val accumulate_script = "out = [];\n" +
        "input." + "map" + "( function (" + (0 until varNum).map("x" + _).mkString(",") + ") {" +
        //(0 until varNum).map(i => "if (x" + i.toString + " !== undefined) out[" + i.toString + "].push("+ExecUtils.resultWrapper("x"+i)+");").mkString("\n") +
        "out.push([" + (0 until varNum).map(i => ExecUtils.resultWrapper("x" + i)).mkString(",") + "]);" +
        "}" + ");\n"

      val accumulated = for (input <- inputs) yield {
        val out = V8Runtime.runtime.executeArrayScript("{\n" +
          "input=" + input + ";\n" +
          accumulate_script + "\n" +
          "delete input;\n" +
          "out" +
          "\n}");
        val inputExtend = if (hof.func == "sort") {
          val elements = for (i <- 0 until out.length()) yield {
            val paramVals = out.getArray(i)
            val v = FromJSWrapper(paramVals.getArray(0))
            paramVals.release()
            v
          }
          val elemsSet = elements.toSet.toList
          Utils.cross(List(elemsSet,elemsSet))
        }
        else for (i <- 0 until out.length()) yield {
          val paramVals = out.getArray(i)
          assert(paramVals.length() == varNum)
          val newVals = mutable.ListBuffer[FromJSWrapper]()
          for (j <- 0 until paramVals.length()) {
            val v = paramVals.getArray(j)
            newVals += FromJSWrapper(v)
          }
          paramVals.release()
          newVals.toList
        }
        out.release()
        inputExtend
      }
      //val accumulated = V8Runtime.runArrayAccumulator(accumulate_script,List("input"),inputs.map(input => VarInputs(List(input))),varNum,"out")
      val varValues: List[List[FromJSWrapper]] = accumulated.flatMap(identity).toSet.toList
      varValues.map(vals =>
        VarInputs(vals.map(wrapper => wrapper.toCode))
      )
    }

    println("new context size=" + sketch.innerVar.length)
    val vocab = vocabulary ++ sketch.innerVar.map(varName => Literal(varName))
    println("|E|=" + examples.length)
    val executor = new ExecutorWithVal(extend(inputs, sketch), sketch.innerVar)
    println("|ext(E)|=" + executor.inputVals.length)
    val mainExec = new ExecutorWithVal(inputs.map(input => VarInputs(List(input))))
    val retainPolicy = new ValuesRetainPolicy(executor, Nil)
    val enumerator = Enumerator(vocab, retainPolicy)

    val seenValues: mutable.Set[List[FromJSWrapper]] = mutable.Set()
    val discardedSubtrees: mutable.Set[String] = mutable.Set()
    for (newSubtree <- enumerator) {
      val completedSketch = sketch.deepClone
      completedSketch.addChild(1, newSubtree)
      val execResult = mainExec.execute(completedSketch)
      val result = execResult.get
      if (expectedPieces.contains(newSubtree.toCode)) {
        if (seenValues.contains(result)) { //about to be discarded
          discardedSubtrees += newSubtree.toCode
        }
      }
      seenValues += result
      if (result == outexec) {
        println(completedSketch.toCode)
        println("outside equiv classes=" + seenValues.size)
        println("inside equiv classes=" + retainPolicy.values.size)
        println("#subtrees=" + expectedPieces.size)
        println("discarded subtrees=" + discardedSubtrees.mkString("{",",","}") )
        println("all subtrees=" + expectedPieces.mkString("{", ",", "}"))
        return completedSketch
      }
    }
    return sketch
  }
}
