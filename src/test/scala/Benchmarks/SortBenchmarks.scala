import SketchBenchmarksDriver.runBenchmark

object SortBenchmarks {
  val sortVocab = SketchBenchmarksDriver.vocabulary.filter(p => !Set("<",">","==","||","&&","+").contains(p.func))
  val sketch = {
    val sort = JSParser.deconstruct("input.sort((a,b) => e)")
    sort.removeChild(1)
    sort.asInstanceOf[AggregatorMethod]
  }
  def sortNumbers(): Unit = {

    val examples = List(
      "[1,3,2]" -> "[1,2,3]",
      "[700,80,700,9]" -> "[9,80,700,700]",
      "[-1,0,1]" -> "[-1,0,1]",
      "[4, 2, 5, 1, 3]" -> "[1,2,3,4,5]"
    )

    val res = runBenchmark(sketch, sortVocab, examples,"a - b")
    assert(res.toCode == "input.sort((a,b) => a - b)")

  }

  def sortLength(): Unit = {
    val examples = List(
      "['abc','ab','a']" -> "['a','ab','abc']",
      "[[],[1],[1,2]]" -> "[[],[1],[1,2]]",
      "['b','cde','fg']" -> "['b','fg','cde']",
      "['bgghadf','cde','fg']" -> "['fg','cde', 'bgghadf']"
    )
    val res = runBenchmark(sketch, sortVocab, examples,"a.length - b.length")
    assert(res.toCode == "input.sort((a,b) => a.length - b.length)")
  }

}
