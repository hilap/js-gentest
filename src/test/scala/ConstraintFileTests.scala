import ConstraintFileReader.ConstraintKey
import org.junit.Test
import org.junit.Assert._
import org.scalatest.junit.JUnitSuite

class ConstraintFileTests  extends JUnitSuite{
  @Test def readConstraintLine() : Unit = {
    val line = "+;BinOperator;2;0;boolean,obj,array,map"
    val res = ConstraintFileReader.process(line)
    assertEquals(ConstraintKey("+","BinOperator",2),res._1)
    assertEquals(0,res._2._1)
    assertEquals(Set(DataType.boolean,DataType.obj,DataType.array,DataType.map),res._2._2)
  }

  @Test def readConstraintFile() : Unit = {
    val fileData =
      """assignInitialized;Macro;3;2;number,boolean,string
        |+;BinOperator;2;0;boolean,obj,array,map
        |+;BinOperator;2;1;boolean,obj,array,map""".stripMargin

    val res = ConstraintFileReader.processAll(fileData)
    assertEquals(2,res.size)
    assertTrue(res.contains(ConstraintKey("+","BinOperator",2)))
    assertTrue(res.contains(ConstraintKey("assignInitialized","Macro",3)))
    assertEquals(List(Set(DataType.boolean,DataType.obj,DataType.array,DataType.map),Set(DataType.boolean,DataType.obj,DataType.array,DataType.map)),res(ConstraintKey("+","BinOperator",2)).args)
    assertEquals(List(Set(),Set(),Set(DataType.number,DataType.boolean,DataType.string)),res(ConstraintKey("assignInitialized","Macro",3)).args)
  }
}
