import org.antlr.v4.runtime.RecognitionException
import org.antlr.v4.runtime.misc.ParseCancellationException
import org.junit.Test
import org.junit.Assert._
import org.scalatest.junit.JUnitSuite

class DeconstructTests extends JUnitSuite{

  @Test def deconstructLiterals() : Unit = {
    val d1 = JSParser.deconstruct("2")
    assertTrue(d1.isInstanceOf[Literal])
    assertEquals("2",d1.toCode)
    val d2 = JSParser.deconstruct("'a'")
    assertEquals("'a'",d2.toCode)
    assertTrue(d2.isInstanceOf[Literal])
    val d3 = JSParser.deconstruct("[1,2,3,4,5]")
    assertTrue(d3.isInstanceOf[Literal])
    assertEquals("[1,2,3,4,5]",d3.toCode)
    val d3a = JSParser.deconstruct("[]")
    assertTrue(d3a.isInstanceOf[Literal])
    assertEquals("[]",d3a.toCode)
    val d4 = JSParser.deconstruct("true")
    assertTrue(d4.isInstanceOf[Literal])
    assertEquals("true",d4.toCode)
    val d5 = JSParser.deconstruct("x")
    assertTrue(d5.isInstanceOf[Literal])
    val d6 = JSParser.deconstruct("null")
    assertTrue(d6.isInstanceOf[Literal])
    assertEquals("null",d6.toCode)
  }

  @Test def deconstructExpr1() : Unit = {
    val d1 = JSParser.deconstruct("1+2")
    assertTrue(d1.isInstanceOf[BinOperator])
    assertEquals("1 + 2", d1.toCode)

    val d2 = JSParser.deconstruct("1*2+3")
    assertTrue(d2.isInstanceOf[BinOperator])
    assertEquals("(1 * 2) + 3", d2.toCode)

    val d3 = JSParser.deconstruct("!x")
    assertTrue(d3.isInstanceOf[UnPrefOperator])
    assertEquals("!(x)", d3.toCode)

    val d3a = JSParser.deconstruct("~x")
    assertTrue(d3a.isInstanceOf[UnPrefOperator])
    assertEquals("~(x)", d3a.toCode)

    val d4 = JSParser.deconstruct("input++")
    assertTrue(d4.isInstanceOf[UnPostOperator])
    assertEquals("(input)++",d4.toCode)

    val d5 = JSParser.deconstruct("typeof x")
    assertEquals("typeof(x)", d5.toCode)

    val d6 = JSParser.deconstruct("a[2+3]")
    assertEquals("a[2 + 3]",d6.toCode)

    val d7 = JSParser.deconstruct("a ? b + c : c")
    assertEquals("a ? (b + c) : c",d7.toCode)
  }

  @Test def deconstructExpr2() : Unit = {
    val d1 = JSParser.deconstruct("[3,4,5].length")
    assertTrue(d1.isInstanceOf[PropCall])
    assertEquals("length", d1.func)
    assertEquals("[3,4,5].length", d1.toCode)

    val d2 = JSParser.deconstruct("new Set")
    assertTrue(d2.isInstanceOf[Constructor])
    assertEquals("Set", d2.func)
    assertEquals(0, d2.arity)
    assertEquals("new Set", d2.toCode)

    val d3 = JSParser.deconstruct("new Set(['a','b'])")
    assertTrue(d3.isInstanceOf[Constructor])
    assertEquals(1, d3.arity)
    assertEquals("new Set(['a','b'])", d3.toCode)

    val d4 = JSParser.deconstruct("'a' in {'a' : 2}")
    assertTrue(d4.isInstanceOf[BinOperator])
    assertEquals("'a' in {'a':2}", d4.toCode)

    val d5 = JSParser.deconstruct("[{a:1},{a:2},{a:3}]")
    assertTrue(d5.isInstanceOf[ArrayInitializer])
    assertEquals("[{'a':1},{'a':2},{'a':3}]", d5.toCode)

    val d6 = JSParser.deconstruct("`Fifteen is ${a + b}`")
    assertTrue(d6.isInstanceOf[Literal])
  }

  @Test def deconstructStringTemplateExpr() = {
    val d7 = JSParser.deconstruct("myTag`that ${ person } is a ${ age }`")
    assertNotNull(d7) //Needs proper handling later, maybe.
  }

  @Test def deconstructObjectInitializer() : Unit = {
    val d00 = JSParser.deconstruct("({ name: \"foo\", val:  7 })")
    assertTrue(d00.isInstanceOf[ObjectInitializer])
    assertEquals(4,d00.arity)
    assertTrue(d00.children.forall(c => c.get.isInstanceOf[Literal]))
    assertEquals("'name'", d00.children(0).get.toCode)
    assertEquals("\"foo\"",d00.children(1).get.toCode)
    assertEquals("'val'", d00.children(2).get.toCode)
    assertEquals("7", d00.children(3).get.toCode)

    val d0 = JSParser.deconstruct("({'a' : 2, b : true})")
    assertTrue(d0.isInstanceOf[ObjectInitializer])
    assertEquals(4,d0.arity)
    assertEquals("{'a':2,'b':true}",d0.toCode)

    val d1 = JSParser.deconstruct("({a : isNaN(x)})")
    assertTrue(d1.isInstanceOf[ObjectInitializer])
    assertEquals(2, d1.arity)
    assertTrue(d1.children(0).get.isInstanceOf[Literal])
    assertEquals(classOf[ASTNode],d1.children(1).get.getClass)
    assertEquals("isNaN", d1.children(1).get.func)
    assertEquals("{'a':isNaN(x)}",d1.toCode)

    val d2 = JSParser.deconstruct("({x:y})")
    assertTrue(d2.isInstanceOf[ObjectInitializer])
    assertEquals(2, d2.arity)
    assertTrue(d2.children(1).get.isInstanceOf[Literal])

    val d3 = JSParser.deconstruct("{ key : 'value' }")
    assertTrue(d3.isInstanceOf[ObjectInitializer])
    assertTrue(d3.children.forall(c => c.get.isInstanceOf[Literal]))
    assertEquals("{'key':'value'}",d3.toCode)

    val d4 = JSParser.deconstruct("{ key : {value : 2}}")
    assertTrue(d4.isInstanceOf[ObjectInitializer])
    assertTrue(d4.children(1).get.isInstanceOf[ObjectInitializer])
    assertEquals(2, d4.children(1).get.arity)

    val emptyIsLiteral = JSParser.deconstruct("{}")
    assertTrue(emptyIsLiteral.isInstanceOf[Literal])
  }

  @Test def deconstructFuncCalls() : Unit = {
    val deconstructed = JSParser.deconstruct("isNaN(2)")
    assertEquals(classOf[ASTNode],deconstructed.getClass)
    assertEquals("isNaN(2)",deconstructed.toCode)

    val d2 = JSParser.deconstruct("parseInt(\"10\", 10)")
    assertEquals(classOf[ASTNode],d2.getClass)
    assertEquals("parseInt(\"10\",10)",d2.toCode)

    val d3 = JSParser.deconstruct("isFinite()")
    assertEquals(classOf[ASTNode],d3.getClass)
    assertEquals("isFinite",d3.func)
    assertEquals("isFinite()",d3.toCode)
  }

  @Test def deconstructMethodCall() : Unit = {
    val deconstructed0 = JSParser.deconstruct("a.toString()")
    assertTrue(deconstructed0.isInstanceOf[MethodCall])
    assertEquals("a.toString()",deconstructed0.toCode)

    val deconstructed1 = JSParser.deconstruct("[1,2,3].concat([4,5,6])")
    assertTrue(deconstructed1.isInstanceOf[MethodCall])
    assertEquals("[1,2,3].concat([4,5,6])",deconstructed1.toCode)

    val deconstructed2 = JSParser.deconstruct("[1,2,3].concat([4,5,6]).slice(2)")
    assertTrue(deconstructed2.isInstanceOf[MethodCall])
    assertTrue(deconstructed2.asInstanceOf[MethodCall].lhs.get.isInstanceOf[MethodCall])
    assertEquals("([1,2,3].concat([4,5,6])).slice(2)", deconstructed2.toCode)
  }

  @Test(expected = classOf[ParseCancellationException]) def deconstructSyntaxError() : Unit = {
    val deconstructed = JSParser.deconstruct("[1,2,")
  }

  @Test(expected = classOf[ParseCancellationException]) def deonstructLexError() : Unit = {
    val deconstructed = JSParser.deconstruct("0xG")
  }

  @Test def deconstructMap() : Unit = {
    val dec = JSParser.deconstruct("[1,2,].map(function(x) {return x + 1})")
    assertTrue(dec.isInstanceOf[AggregatorMethod])
    assertEquals("x + 1", dec.children(1).get.toCode)
    assertEquals("[1,2,].map(x => x + 1)",dec.toCode)

    val dec2 = JSParser.deconstruct("[].map(blah0 => blah0 - 2)")
    assertTrue(dec2.isInstanceOf[AggregatorMethod])
    assertEquals(List("blah0"),dec2.asInstanceOf[AggregatorMethod].innerVar)
    assertEquals("blah0 - 2", dec2.children(1).get.toCode)
    assertEquals("[].map(blah0 => blah0 - 2)",dec2.toCode)
  }

  @Test def deconstructReduce()  : Unit = {
    val dec = JSParser.deconstruct("[1,2,].reduce(function(x,y) {return x + 1})")
    assertTrue(dec.isInstanceOf[AggregatorMethod])
    assertEquals(List("x","y"), dec.asInstanceOf[AggregatorMethod].innerVar)
    assertEquals("x + 1", dec.children(1).get.toCode)

    val dec2 = JSParser.deconstruct("[1,2,3].reduce((a,b) => ~a)")
    assertTrue(dec2.isInstanceOf[AggregatorMethod])
    assertEquals(List("a","b"),dec2.asInstanceOf[AggregatorMethod].innerVar)
    assertEquals("~(a)",dec2.children(1).get.toCode)

    val dec3 = JSParser.deconstruct("input.reduce(((a, b) => a.indexOf(b) == -1 ? a.concat([b]) : a), [])")
    assertTrue(dec3.isInstanceOf[AggregatorMethod])
    assertEquals(List("a","b"), dec3.asInstanceOf[AggregatorMethod].innerVar)
    assertEquals(3, dec3.arity)
    assertEquals("[]", dec3.children(2).get.toCode)
    assertEquals("((a.indexOf(b)) == (-(1))) ? (a.concat([b])) : a", dec3.children(1).get.toCode)
  }

  @Test def deconstructTwoParamMap() : Unit = {
    val dec = JSParser.deconstruct("[1,2,3].map((e,i) => i)")
    assertTrue(dec.isInstanceOf[AggregatorMethod])
    assertEquals("[1,2,3].map((e,i) => i)",dec.toCode)
  }

  @Test def deconstructSortWithCback() : Unit = {
    val dec = JSParser.deconstruct("[1,2,3].sort((x,y) => x < y)")
    assertTrue(dec.isInstanceOf[AggregatorMethod])
    assertEquals("[1,2,3].sort((x,y) => x < y)",dec.toCode)
  }

  @Test def deconstructArraysOfThings() : Unit = {
    val dec = JSParser.deconstruct("[b]")
    assertTrue(dec.isInstanceOf[ArrayInitializer])
    assertEquals("[b]",dec.toCode)

    val dec2 = JSParser.deconstruct("[{key : 2}, {value : true}, {whatevs : 'false'}]")
    assertTrue(dec2.isInstanceOf[ArrayInitializer])
    assertTrue(dec2.children.forall(c => c.get.isInstanceOf[ObjectInitializer]))

    val dec3 = JSParser.deconstruct("[{key : 2, value : true}, [1,2,3], {},[x,y,z]]")
    assertTrue(dec3.isInstanceOf[ArrayInitializer])
    assertTrue(dec3.children(0).get.isInstanceOf[ObjectInitializer])
    assertEquals("{'key':2,'value':true}",dec3.children(0).get.toCode)
    assertTrue(dec3.children(1).get.isInstanceOf[Literal])
    assertEquals("[1,2,3]",dec3.children(1).get.toCode)
    assertTrue(dec3.children(2).get.isInstanceOf[Literal])
    assertEquals("{}", dec3.children(2).get.toCode)
    assertTrue(dec3.children(3).get.isInstanceOf[ArrayInitializer])
    assertEquals("[x,y,z]", dec3.children(3).get.toCode)
    assertTrue(dec3.children(3).get.children.forall(c => c.get.isInstanceOf[Literal]))
  }

  @Test def deconstructStaticMethodOfType() : Unit = {
    val dec = JSParser.deconstruct("Array.of(1)")
    assertEquals(classOf[ASTNode], dec.getClass)
    assertEquals("Array.of", dec.func)
    assertEquals(1, dec.arity)
    assertEquals("1", dec.children(0).get.toCode)

    val dec2 = JSParser.deconstruct("JSON.parse('1' + '2',2)")
    assertEquals(classOf[ASTNode],dec2.getClass)
    assertEquals("JSON.parse",dec2.func)
    assertEquals(2,dec2.arity)
    assertEquals("'1' + '2'", dec2.children(0).get.toCode)
    assertEquals("2", dec2.children(1).get.toCode)

    val deconstructed = JSParser.deconstruct("JSON.stringify(null)")
    assertEquals(classOf[ASTNode],deconstructed.getClass)
    assertEquals("JSON.stringify(null)",deconstructed.toCode)
  }
}
