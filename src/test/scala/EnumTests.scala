import org.junit.Test
import org.junit.Assert._
import org.scalatest.junit.JUnitSuite

/**
 * Created by hila on 06/12/2017.
 */

object KeepAllRetainPolicy extends RetainPolicy{
  val executor = new ExecutorNoVal
  override def shouldAdd(expr: ASTNode) : (Boolean,Set[DataType.DataType]) = (true,executor.execute(expr).get.map(x => x.getType).toSet)
  override def releaseValues(): Unit = {}
}
class EnumTests extends JUnitSuite{
  @Test def enumerateOverVocab() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |?+?
        |?-?
        |-?""".stripMargin)

    val enumerator = Enumerator(vocab, KeepAllRetainPolicy)
    assertTrue(enumerator.hasNext)
    assertEquals("0",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + 0",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + 1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + 0", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + 1", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 - 0", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 - 1", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 - 0", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 - 1", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("-(0)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("-(1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (0 + 0)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (0 + 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (1 + 0)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (1 + 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (0 - 0)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (0 - 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (1 - 0)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (1 - 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (-(0))", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 + (-(1))", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (0 + 0)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (0 + 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (1 + 0)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (1 + 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (0 - 0)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (0 - 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (1 - 0)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (1 - 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (-(0))", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (-(1))", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("(0 + 0) + 0", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("(0 + 0) + 1", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("(0 + 0) + (0 + 0)", enumerator.next().toCode)
  }



  @Test def enumerateUniqueValues() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |?+?
        |?-?
        |-?""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorNoVal,Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("0", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + 1", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 - 1", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + (1 + 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("(1 + 1) + (1 + 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("(0 - 1) + (0 - 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("(0 - 1) - (1 + 1)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + ((1 + 1) + (1 + 1))", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
  }

  @Test def enumerateBadFunction() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |?.concat(?)
        |?+?""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorNoVal,Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("0", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + 1", enumerator.next().toCode)
  }

  @Test def enumerateUniqueValuesList() : Unit = {
    val vocab = ASTNode.parseList(
      """[]
      |[1]
      |[2]
      |[3]
      |?.concat(?)
      |?.indexOf(?)
      |?.toString()""".stripMargin
    )

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorNoVal,Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("[]", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("[1]", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("[2]", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("[3]", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("[1].concat([1])", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("[1].concat([2])", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("[1].concat([3])", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("[2].concat([1])", enumerator.next().toCode)
  }


  @Test def enumerateUniqueValuesMakesUndefined : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |?[?]
        |?+?""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorNoVal,Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("0",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + 1",enumerator.next().toCode)
  }

  @Test def enumerateWithEvalErrors() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |?/?""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorNoVal,Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("0", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0 / 0", enumerator.next().toCode)
  }

  @Test def enumerateInputResults() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |input
        |?+?
        |?-?
        |-?""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("1")),VarInputs(List("2")))),Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("0",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + 1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + input", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input + input", enumerator.next().toCode)
  }

  @Test def enumerateInputBadFunction() : Unit = {
    val vocab = ASTNode.parseList(
      """input
        |?.concat(?)
        |?+?""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("1")))),Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("input", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input + input", enumerator.next().toCode)
  }

  @Test def enumerateInputMakesUndefined : Unit = {
    val vocab = ASTNode.parseList(
      """input
        |?[?]
        |?+?""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("1")))),Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input + input",enumerator.next().toCode)
  }

  @Test def enumerateInputMap : Unit = {
    AggregatorMethod.count = 0
    val vocab = ASTNode.parseList(
      """input
      |1
      |? + ?
      |?.map(? => ?)""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("[1,2,3]")),VarInputs(List("[4,5,6,7,8]")))),Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input + input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input + 1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + 1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.map(x1 => input)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.map(x1 => 1)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.map(x1 => input + input)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.map(x1 => input + 1)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.map(x1 => input + x1)",enumerator.next().toCode)
  }

  @Test def enumerateInputFilter : Unit = {
    AggregatorMethod.count = 0
    val vocab = ASTNode.parseList(
      """input
        |1
        |? - ?
        |?.filter(? => ?)""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("[1,2,3]")),VarInputs(List("[4,5,6,7,8]")))),Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input - input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 - 1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.filter(x1 => input - input)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.filter(x1 => 1 - x1)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.filter(x1 => 1 - (x1 - 1))",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("(1 - 1) - 1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
  }

  @Test def enumerateInputReduce : Unit = {
    AggregatorMethod.count = 0
    val vocab = ASTNode.parseList(
      """input
        |1
        |? + ?
        |?.reduce((?,?) => ?)""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("[1,2,3]")),VarInputs(List("[4,5,6,7,8]")),VarInputs(List("[9]")))),Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input + input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input + 1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + input",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1 + 1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => input)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => 1)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => x1)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => x2)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => input + input)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => input + 1)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => input + x1)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => input + x2)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => 1 + input)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => 1 + 1)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => 1 + x2)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => x1 + input)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => x1 + x1)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => x1 + x2)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => x2 + input)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => x2 + x2)",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => input + (input + input))",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("input.reduce((x1,x2) => input + (input + 1))",enumerator.next().toCode)
  }

  @Test def enumerateWithLenLimit()  : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |? + ?
        |?.map(?)""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorNoVal(),Nil),1)
    assertTrue(enumerator.hasNext)
    assertEquals("0",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1",enumerator.next().toCode)
    assertFalse(enumerator.hasNext)
  }
  @Test def enumerateWithLenLimit2() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |? + ?""".stripMargin)

    val enumerator = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorNoVal(),Nil),3)
    assertTrue(enumerator.hasNext)
    assertEquals("0",enumerator.next().toCode)
    assertFalse(enumerator.hasNext)
  }

  @Test def enumerateWithSubtreesInVocab() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |?+?
        |?-?
        |-?""".stripMargin) :+ BinOperator("*")
    vocab.last.addChild(0,Literal("1"))
    vocab.last.addChild(1,Literal("2"))

    val enumerator = Enumerator(vocab, KeepAllRetainPolicy)
    assertTrue(enumerator.hasNext)
    assertEquals("1 * 2", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("0",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("1",enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("(1 * 2) + (1 * 2)", enumerator.next().toCode)
    assertTrue(enumerator.hasNext)
    assertEquals("(1 * 2) + 0", enumerator.next().toCode)
  }

  @Test def enumerateWithOptionalParams() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |isNaN(?,??)""".stripMargin
    )

    val enumerator = Enumerator(vocab, KeepAllRetainPolicy)
    assertEquals("0",enumerator.next().toCode)
    assertEquals("1",enumerator.next().toCode)
    assertEquals("isNaN(0)",enumerator.next().toCode)
    assertEquals("isNaN(1)",enumerator.next().toCode)
    assertEquals("isNaN(0,0)",enumerator.next().toCode)
    assertEquals("isNaN(0,1)",enumerator.next().toCode)
    assertEquals("isNaN(1,0)",enumerator.next().toCode)

  }

  @Test def enumerateMethodWithOptionalParams() : Unit = {
    val vocab = ASTNode.parseList(
      """input
        |0
        |1
        |? < ?
        |?.sort(??)""".stripMargin
    )
    val enumerator = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("[1,2,3]")),VarInputs(List("[4,6,5]")))),Nil))
    assertEquals("input",enumerator.next().toCode)
    assertEquals("0", enumerator.next().toCode)
    assertEquals("1", enumerator.next().toCode)
    assertEquals("input < input", enumerator.next().toCode)
    assertEquals("0 < 1", enumerator.next().toCode)
    assertEquals("input.sort()",enumerator.next().toCode)
  }

  @Test def enumerateAggregatorMethodWithOptionalParams() : Unit = {
    val vocab = ASTNode.parseList(
      """input
        |0
        |1
        |? < ?
        |?.sort((?,?) => ??)""".stripMargin
    )
    val enumerator = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("[1,2,3]")),VarInputs(List("[4,6,5]")))),Nil))
    assertEquals("input",enumerator.next().toCode)
    assertEquals("0", enumerator.next().toCode)
    assertEquals("1", enumerator.next().toCode)
    assertEquals("input < input", enumerator.next().toCode)
    assertEquals("0 < 1", enumerator.next().toCode)
    assertEquals("input.sort()",enumerator.next().toCode)
  }

  @Test def enumerateFuncWithAllOptional() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |isNaN(??)""".stripMargin
    )
    val enumerator = Enumerator(vocab,KeepAllRetainPolicy)
    assertEquals("0",enumerator.next().toCode)
    assertEquals("1",enumerator.next().toCode)
    assertEquals("isNaN()",enumerator.next().toCode)
    assertEquals("isNaN(0)", enumerator.next().toCode)
  }

  @Test def enumerateFuncWithAllOptional2() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |isNaN(??,??)""".stripMargin
    )
    val enumerator = Enumerator(vocab,KeepAllRetainPolicy)
    assertEquals("0",enumerator.next().toCode)
    assertEquals("1",enumerator.next().toCode)
    assertEquals("isNaN()",enumerator.next().toCode)
    assertEquals("isNaN(0)", enumerator.next().toCode)
    assertEquals("isNaN(1)",enumerator.next().toCode)
    assertEquals("isNaN(0,0)",enumerator.next().toCode)
  }

  @Test def enumerateUnderRootWithPath() : Unit = {
    val vocab = ASTNode.parseList(
      """0
        |1
        |isNaN(?)""".stripMargin
    )
    val root = ASTNode("foo(?)")
    root.addChild(0,ASTNode("bar(?,?)"))
    root.children.head.get.addChild(0,Literal("x"))
    val path = List(0,1)

    val enumerator = new Enumerator(vocab,KeepAllRetainPolicy,3,Some(root),path)

    assertEquals("foo(bar(x,0))", enumerator.next().toCode)
    assertEquals("foo(bar(x,1))", enumerator.next().toCode)
    assertEquals("foo(bar(x,isNaN(0)))",enumerator.next().toCode)
    assertEquals("foo(bar(x,isNaN(1)))",enumerator.next().toCode)
    assertEquals("foo(bar(x,isNaN(isNaN(0))))",enumerator.next().toCode)
    assertEquals("foo(bar(x,isNaN(isNaN(1))))",enumerator.next().toCode)
    assertFalse(enumerator.hasNext)
  }

  @Test def enumerateUnderRootWithVals() : Unit = {
    val vocab = ASTNode.parseList(
      """input
        |0
        |1
        |? + ?
        |? / ?""".stripMargin
    )
    val isNaN = ASTNode("isNaN(?)")
    val enumerator = new Enumerator(vocab,new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("1")),VarInputs(List("0")))),Nil),3, Some(isNaN),List(0))
    assertEquals("isNaN(input)", enumerator.next().toCode)
    assertEquals("isNaN(input / input)",enumerator.next().toCode)
    assertFalse(enumerator.hasNext)
  }

  @Test def enumOneLiteral() : Unit = {
    val vocab = List(Literal("x"))
    val enumerator = Enumerator(vocab,KeepAllRetainPolicy)
    assertTrue(enumerator.hasNext)
    assertTrue(enumerator.hasNext)
    assertEquals("x",enumerator.next.toCode)
    assertFalse(enumerator.hasNext)
  }

  @Test def enumTwoParamAgg() : Unit = {
    AggregatorMethod.count = 0
    val vocab = List(Literal("[1,3]"), Literal("[1,2]"), ASTNode("?.reduce(? => ?, ?)"))
    assertEquals(classOf[AggregatorMethod],vocab.last.getClass)

    val enumerator = Enumerator(vocab,KeepAllRetainPolicy)
    assertEquals("[1,3]", enumerator.next.toCode)
    assertEquals("[1,2]", enumerator.next.toCode)
    assertEquals("[1,3].reduce(x1 => [1,3],[1,3])", enumerator.next.toCode)
    assertEquals("[1,3].reduce(x1 => [1,2],[1,3])", enumerator.next.toCode)
    //assertEquals("[1,3].reduce(x1 => x1,[1,3])", enumerator.next.toCode)
    assertEquals("[1,3].reduce(x1 => [1,3],[1,2])", enumerator.next.toCode)
    assertEquals("[1,3].reduce(x1 => [1,2],[1,2])", enumerator.next.toCode)
    //assertEquals("[1,3].reduce(x1 => x1,[1,2])", enumerator.next.toCode)
    assertEquals("[1,2].reduce(x1 => [1,3],[1,3])", enumerator.next.toCode)
  }

  @Test def enumTwoParamAggOptional() : Unit = {
    AggregatorMethod.count = 0
    val vocab = List(Literal("[1,3]"), Literal("[1,2]"), ASTNode("?.reduce(? => ?, ??)"))
    assertEquals(classOf[AggregatorMethod],vocab.last.getClass)
    assertEquals(1, vocab.last.optionalArgs)
    val enumerator = Enumerator(vocab,KeepAllRetainPolicy)
    assertEquals("[1,3]", enumerator.next.toCode)
    assertEquals("[1,2]", enumerator.next.toCode)
    assertEquals("[1,3].reduce(x1 => [1,3])", enumerator.next.toCode)
    assertEquals("[1,3].reduce(x1 => [1,2])", enumerator.next.toCode)
    assertEquals("[1,3].reduce(x1 => x1)", enumerator.next.toCode)

    assertEquals("[1,3].reduce(x1 => [1,3],[1,3])", enumerator.next.toCode)
    assertEquals("[1,3].reduce(x1 => [1,2],[1,3])", enumerator.next.toCode)
    //assertEquals("[1,3].reduce(x1 => x1,[1,3])", enumerator.next.toCode)


    assertEquals("[1,3].reduce(x1 => [1,3],[1,2])", enumerator.next.toCode)
    assertEquals("[1,3].reduce(x1 => [1,2],[1,2])", enumerator.next.toCode)
    //assertEquals("[1,3].reduce(x1 => x1,[1,2])", enumerator.next.toCode)

    assertEquals("[1,2].reduce(x1 => [1,3])", enumerator.next.toCode)
    assertEquals("[1,2].reduce(x1 => [1,2])", enumerator.next.toCode)
    assertEquals("[1,2].reduce(x1 => x1)", enumerator.next.toCode)
    assertEquals("[1,2].reduce(x1 => [1,3],[1,3])", enumerator.next.toCode)
    assertEquals("[1,2].reduce(x1 => [1,2],[1,3])", enumerator.next.toCode)

  }

  @Test def enumWithWeightedRetainSubrtree() : Unit = {
    val vocab = List(Literal("[1]"), Literal("[1,3]"), Literal("'1,31,3'"), JSParser.deconstruct("[1].concat([3])"), BinOperator("+"))
    //vocab(1).setRetain()
    val retainPredicates = List(RetainPredicate(vocab(1)))
    val enumerator : Enumerator = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorNoVal,retainPredicates))
    assertTrue(enumerator.hasNext)
    assertEquals("[1].concat([3])", enumerator.next().toCode)
    assertEquals("[1]", enumerator.next().toCode)
    assertEquals("[1,3]", enumerator.next().toCode)
    assertEquals("'1,31,3'", enumerator.next().toCode)
    //assertEquals("([1].concat([3])) + ([1].concat([3]))", enumerator.next().toCode)
    assertEquals("([1].concat([3])) + [1]", enumerator.next().toCode)
    assertEquals("([1].concat([3])) + [1,3]", enumerator.next().toCode)
    assertEquals("([1].concat([3])) + '1,31,3'", enumerator.next().toCode)
    assertEquals("[1] + ([1].concat([3]))", enumerator.next().toCode)
    assertEquals("[1] + [1]", enumerator.next().toCode)
    assertEquals("[1] + [1,3]", enumerator.next().toCode)
    assertEquals("[1] + '1,31,3'", enumerator.next().toCode)
    //assertEquals("[1,3] + ([1].concat([3]))", enumerator.next().toCode)
    assertEquals("[1,3] + [1]", enumerator.next().toCode)
  }

  @Test def enumWithRetainInLambda() : Unit = {
    val vocab = List(Literal("[1,2,3]"), Literal("input"),BinOperator("+"), AggregatorMethod("map",1,List("x")))
    val retains = List(RetainPredicate(Literal("input")))

    val enumerator : Enumerator = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs("[1,2,3]" :: Nil))),retains),Int.MaxValue,Nil,retains)
    assertTrue(enumerator.hasNext)
    assertEquals("[1,2,3]", enumerator.next().toCode)
    assertEquals("input", enumerator.next().toCode)
    assertEquals("[1,2,3] + [1,2,3]", enumerator.next().toCode)
    assertEquals("[1,2,3] + input", enumerator.next().toCode)
    //assertEquals("input + [1,2,3]", enumerator.next().toCode)
    //assertEquals("input + input", enumerator.next().toCode)
    assertEquals("[1,2,3].map(x => [1,2,3])", enumerator.next().toCode)
    assertEquals("[1,2,3].map(x => input)", enumerator.next().toCode)
    //assertEquals("[1,2,3].map(x => x)", enumerator.next().toCode) //outside OE
    assertEquals("[1,2,3].map(x => [1,2,3] + [1,2,3])", enumerator.next().toCode)
  }

  @Test def enumNonCallbackAllOptionalArg() : Unit = {
    val vocab = List(Literal("1"), Literal("' '"), ASTNode("new Map(??)"))
    assertTrue(vocab(2).isInstanceOf[Constructor])
    assertEquals(1,vocab(2).arity)
    assertEquals(1,vocab(2).optionalArgs)

    val enumerator = Enumerator(vocab,KeepAllRetainPolicy)
    assertEquals("1",enumerator.next.toCode)
    assertEquals("' '", enumerator.next.toCode)
    assertEquals("new Map()", enumerator.next.toCode)
    assertEquals("new Map(1)", enumerator.next.toCode)
  }

  @Test def enumNonCallbackAllOptionalArg2() : Unit = {
    val vocab = List(Literal("1"), Literal("' '"), ASTNode("new Map(??,??)"))
    assertTrue(vocab(2).isInstanceOf[Constructor])
    assertEquals(2,vocab(2).arity)
    assertEquals(2,vocab(2).optionalArgs)

    val enumerator = Enumerator(vocab,KeepAllRetainPolicy)
    assertEquals("1",enumerator.next.toCode)
    assertEquals("' '", enumerator.next.toCode)
    assertEquals("new Map()", enumerator.next.toCode)
    assertEquals("new Map(1)", enumerator.next.toCode)
    assertEquals("new Map(' ')", enumerator.next.toCode)
    assertEquals("new Map(1,1)", enumerator.next.toCode)
  }

  @Test def enumExcludeExcludesOnTheGo() : Unit = {
    val vocab = List(Literal("[1]"), Literal("[3]"), ASTNode("?.concat(?)"), BinOperator("+"))
    val excludes : List[ProgramPredicate] = List(ExcludePredicate(JSParser.deconstruct("[1].concat([1])")))
    val enumerator = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorNoVal,Nil),Int.MaxValue,excludes)
    assertTrue(enumerator.hasNext)
    assertEquals("[1]", enumerator.next().toCode)
    assertEquals("[3]", enumerator.next().toCode)
    assertEquals("[1].concat([3])", enumerator.next().toCode)
    assertEquals(0, enumerator.curr.count(p => p.prog.contains(JSParser.deconstruct("[1].concat([1])"))))
    assertEquals("[3].concat([1])", enumerator.next().toCode)
    assertEquals("[3].concat([3])", enumerator.next().toCode)
    assertEquals("[1] + [1]", enumerator.next().toCode)
    assertEquals("[1] + [3]", enumerator.next().toCode)
    assertEquals("[3] + [1]", enumerator.next().toCode)
    assertEquals("[3] + [3]", enumerator.next().toCode)
    assertEquals("[1].concat([1].concat([3]))",enumerator.next.toCode)
    assertEquals("[1].concat([3].concat([1]))",enumerator.next.toCode)
    assertEquals("[1].concat([3].concat([3]))",enumerator.next.toCode)
    assertEquals("[1].concat([1] + [1])",enumerator.next.toCode)
    assertEquals("[1].concat([1] + [3])",enumerator.next.toCode)
    assertEquals("[1].concat([3] + [1])",enumerator.next.toCode)
    assertEquals("[1].concat([3] + [3])",enumerator.next.toCode)
    assertEquals("[3].concat([1].concat([3]))",enumerator.next.toCode)

  }

  @Test def enumExcludeInLambda() : Unit = {
    AggregatorMethod.count = 0
    val vocab = List(Literal("[1]"), Literal("[3]"), ASTNode("?.map(? => ?)"), BinOperator("+"))
    val excludes : List[ProgramPredicate] = List(ExcludePredicate(JSParser.deconstruct("[1] + [3]")),ExcludePredicate(JSParser.deconstruct("[3] + [3]")))
    val enumerator = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorNoVal,Nil), Int.MaxValue,excludes)
    assertTrue(enumerator.hasNext)
    assertEquals("[1]", enumerator.next().toCode)
    assertEquals("[3]", enumerator.next().toCode)
    assertEquals("[1] + [1]", enumerator.next().toCode)
    assertEquals("[3] + [1]", enumerator.next().toCode)
    assertEquals("[1].map(x1 => [1])", enumerator.next().toCode)
    assertEquals("[1].map(x1 => [3])", enumerator.next().toCode)
    assertEquals("[1].map(x1 => [1] + [1])", enumerator.next().toCode)
    assertEquals("[1].map(x1 => [3] + [1])", enumerator.next().toCode)
    assertEquals(0, enumerator.paramsIter.asInstanceOf[AggregatorParamsIter].enumerator.curr.count(m => m.prog.contains(JSParser.deconstruct("[1] + [3]"))))
    assertEquals("[1].map(x1 => x1 + [3])", enumerator.next().toCode)
    assertEquals("[1].map(x1 => x1 + x1)", enumerator.next().toCode)
    assertEquals(0, enumerator.paramsIter.asInstanceOf[AggregatorParamsIter].enumerator.curr.count(m => m.prog.contains(JSParser.deconstruct("[3] + [3]"))))
    assertEquals("[1].map(x1 => [1] + ([1] + [1]))", enumerator.next().toCode)
  }

  @Test def enumTypes() : Unit = {
    val vocab = List(Literal("1"), Literal("' '"), ASTNode("new Map"), Literal("[]"), Literal("true"), Literal("{}"))
    val enumerator = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorNoVal,Nil))
    assertTrue(enumerator.hasNext)
    assertEquals("1", enumerator.next().toCode)
    assertEquals(Set(DataType.number), enumerator.curr.last.types)
    assertEquals("' '", enumerator.next().toCode)
    assertEquals(Set(DataType.string), enumerator.curr.last.types)
    assertEquals("new Map", enumerator.next().toCode)
    assertEquals(Set(DataType.map), enumerator.curr.last.types)
    assertEquals("[]", enumerator.next().toCode)
    assertEquals(Set(DataType.array), enumerator.curr.last.types)
    assertEquals("true", enumerator.next().toCode)
    assertEquals(Set(DataType.boolean), enumerator.curr.last.types)
    assertEquals("{}", enumerator.next().toCode)
    assertEquals(Set(DataType.obj), enumerator.allPrev.last.types)
  }

  @Test def enumTypesMultiple() : Unit = {
    val vocab = List(Literal("input"))
    val e1 = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("1")),VarInputs(List("'a'")))),Nil))
    assertTrue(e1.hasNext)
    assertEquals("input",e1.next.toCode)
    assertEquals(Set(DataType.number,DataType.string),e1.allPrev.last.types)

    val e2 = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("1")),VarInputs(List("8.0")))),Nil))
    assertTrue(e2.hasNext)
    assertEquals("input",e2.next.toCode)
    assertEquals(Set(DataType.number),e2.allPrev.last.types)

    val e3 = Enumerator(vocab,new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("{a: 'b'}")),VarInputs(List("[1,2,3]")))),Nil))
    assertTrue(e3.hasNext)
    assertEquals("input",e3.next.toCode)
    assertEquals(Set(DataType.obj, DataType.array),e3.allPrev.last.types)
  }

  @Test def enumWithConstraints() : Unit = {
    val vocab = List(
      Literal("1"),
      Literal("{}"),
      BinOperator("+")
    )
    val constraints = ConstraintFileReader.processAll("""+;BinOperator;2;0;boolean,obj,array,map
                                                        |+;BinOperator;2;1;boolean,obj,array,map""".stripMargin)
    val enum = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorNoVal,Nil),Int.MaxValue,Nil,Nil, constraints)
    assertTrue(enum.hasNext)
    assertEquals("1",enum.next.toCode)
    assertEquals("{}",enum.next.toCode)
    assertEquals("1 + 1",enum.next.toCode)
    //assertEquals("1 + {}",enum.next.toCode)
    //assertEquals("{} + 1",enum.next.toCode)
    //assertEquals("{} + {}",enum.next.toCode)
    assertEquals("1 + (1 + 1)",enum.next.toCode)
  }

  @Test def enumWithConstraintsInsideLambda() : Unit = {

    val vocab = List(
      Literal("1"),
      Literal("[1]"),
      BinOperator("+"),
      AggregatorMethod("map",1,List("x1"))
    )
    val constraints = ConstraintFileReader.processAll("""+;BinOperator;2;0;boolean,obj,array,map
                                                        |+;BinOperator;2;1;boolean,obj,array,map""".stripMargin)
    val enum = Enumerator(vocab, new ValuesRetainPolicy(new ExecutorNoVal,Nil),Int.MaxValue,Nil,Nil, constraints)
    assertTrue(enum.hasNext)
    assertEquals("1",enum.next.toCode)
    assertEquals("[1]",enum.next.toCode)
    assertEquals("1 + 1",enum.next.toCode)
    //assertEquals("1 + [1]",enum.next.toCode)
    //assertEquals("[1] + 1",enum.next.toCode)
    //assertEquals("[1] + [1]",enum.next.toCode)
    assertEquals("[1].map(x1 => [1])",enum.next.toCode)
    assertEquals("[1].map(x1 => 1 + 1)",enum.next.toCode)
    //assertEquals("[1].map(x1 => 1 + [1])",enum.next.toCode)
    assertEquals("[1].map(x1 => 1 + (1 + 1))",enum.next.toCode)
    //assertEquals("[1].map(x1 => 1 + (1 + [1]))",enum.next.toCode)
    //assertEquals("[1].map(x1 => [1] + (1 + 1))",enum.next.toCode)
    //assertEquals("[1].map(x1 => (1 + 1) + [1])",enum.next.toCode)
    assertEquals("[1].map(x1 => (1 + 1) + (1 + 1))",enum.next.toCode)
    //assertEquals("[1].map(x1 => (1 + 1) + (1 + [1]))",enum.next.toCode)
    //assertEquals("[1].map(x1 => (1 + [1]) + (1 + 1))",enum.next.toCode)
    //assertEquals("[1].map(x1 => (1 + [1]) + (1 + [1]))",enum.next.toCode)
    assertEquals("1 + (1 + 1)",enum.next.toCode)

  }
}
