import com.eclipsesource.v8.{V8, V8Array}
import org.junit.Test
import org.junit.Assert._
import org.scalatest.Fact.Binary_&
import org.scalatest.junit.JUnitSuite

/**
 * Created by hila on 06/12/2017.
 */
class ExecuteCodeTests extends JUnitSuite{
  val runtime = V8.createV8Runtime
  @Test def execNumLiteral() : Unit = {
    val code = Literal("2")
    val result = runtime.executeScript(code.toCode);
    assertEquals(2,result)
  }

  @Test def execBoolLiteral() : Unit = {
    val code = Literal("true")
    val result = runtime.executeScript(code.toCode);
    assertEquals(true,result)
  }

  @Test def execListLiteral() : Unit = {
    val code = Literal("[]")
    val result = runtime.executeScript(code.toCode)
    assertEquals(classOf[V8Array],result.getClass)
    assertEquals(0,result.asInstanceOf[V8Array].length())
  }

  @Test def execNumBinop() : Unit = {
    val code = BinOperator("+")
    code.addChild(0,Literal("2"))
    code.addChild(1,Literal("3"))
    val result = runtime.executeScript(code.toCode)
    assertEquals(5,result)
  }

  @Test def execStringBinop() : Unit = {
    val code = BinOperator("*")
    code.addChild(0,Literal("'a'"))
    code.addChild(1,Literal("3"))
    val result = runtime.executeScript(code.toCode)
    assertEquals(Double.NaN,result)
  }

  @Test def execStringMethod() : Unit = {
    val code = ASTNode("?.concat(?)")
    code.addChild(0,Literal("'a'"))
    code.addChild(1,Literal("'b'"))
    val result = runtime.executeScript(code.toCode)
    assertEquals("ab",result)
  }

  @Test def execArrayDeref() : Unit = {
    val code = ArrayDeref()
    code.addChild(0,ASTNode("[1,2,3]"))
    val inner = BinOperator("+")
    inner.addChild(0,Literal("1"))
    inner.addChild(1,Literal("1"))
    code.addChild(1,inner)
    val result = runtime.executeScript(code.toCode)
    assertEquals(3,result)
  }

  @Test def execTrenOp() : Unit = {
    val code = TernaryOperator()
    val cond = BinOperator("<")
    cond.addChild(0,Literal("2"))
    cond.addChild(1,Literal("3"))
    code.addChild(0,cond)
    code.addChild(1,Literal("'abc'"))
    val rhs = UnPrefOperator("!")
    rhs.addChild(0,Literal("true"))
    code.addChild(2,rhs)
    val result = runtime.executeScript(code.toCode)
    assertEquals("abc",result)
  }

  @Test def execPropertyCall() : Unit = {
    val code = PropCall("length")
    code.addChild(0,Literal("[]"))
    val result = runtime.executeScript(code.toCode)
    assertEquals(0,result)
  }

  @Test def execOneVarAggregators() : Unit = {
    val mapCode = AggregatorMethod("map",1,2)
    mapCode.addChild(0,Literal("[1,2,3]"))
    mapCode.addChild(1,Literal(mapCode.innerVar.head))
    val result = runtime.executeScript(mapCode.toCode)
    assertTrue(result.isInstanceOf[V8Array])
    assertEquals(1,result.asInstanceOf[V8Array].get(0))
    assertEquals(2,result.asInstanceOf[V8Array].get(1))
    assertEquals(3,result.asInstanceOf[V8Array].get(2))

    val filterCode = AggregatorMethod("filter",1,2)
    filterCode.addChild(0,Literal("[0,1,2,3,4,5]"))
    val innerFilter = BinOperator("==")
    val filterLhs = BinOperator("%")
    filterLhs.addChild(0,Literal(filterCode.innerVar.head))
    filterLhs.addChild(1,Literal("2"))
    innerFilter.addChild(0,filterLhs)
    innerFilter.addChild(1,Literal("0"))
    filterCode.addChild(1,innerFilter)
    val result2 = runtime.executeScript(filterCode.toCode)
    assertTrue(result2.isInstanceOf[V8Array])
    assertEquals(3,result2.asInstanceOf[V8Array].length())
  }

  @Test def execReduce() : Unit = {
    val code = AggregatorMethod("reduce",1,2)
    assertEquals(2,code.innerVar.length)
    code.addChild(0,Literal("[0,1,2,3,4,5]"))
    val innerReducer = BinOperator("+")
    innerReducer.addChild(0,Literal(code.innerVar(0)))
    innerReducer.addChild(1,Literal(code.innerVar(1)))
    code.addChild(1,innerReducer)
    val result = runtime.executeScript(code.toCode)
    assertEquals(15,result)
  }

  @Test def execMaths() : Unit = {
    val code = BinOperator("/")
    code.addChild(0,Literal("5"))
    code.addChild(1,Literal("2"))
    val result = runtime.executeScript(code.toCode)
    assertEquals(2.5,result)
  }

  @Test def contextVars() : Unit = {
    val cbValues = AggregatorParamsIter.getCallbackValues(List(JSArrayWrapper("[1,2,3]")),List(VarInputs(List("null"))),List("inner"),"map",1)
    assertEquals(Set(VarInputs(List("null", "2")), VarInputs(List("null", "1")), VarInputs(List("null", "3"))), cbValues.toSet)

    val foundInner = try {
      V8Runtime.runtime.executeScript("inner")
      true
    } catch {
      case e : Exception => {
        assertTrue(e.isInstanceOf[com.eclipsesource.v8.V8ScriptExecutionException])
        assertEquals("undefined:1: ReferenceError: inner is not defined", e.getMessage)
        false
      }
    }
    assertFalse(foundInner)

//    val foundOut = try {
//      val outVal = V8Runtime.runtime.executeScript("__out")
//      assertEquals(null,outVal)
//    } catch {
//      case e : Exception => {
//        assertTrue(e.isInstanceOf[com.eclipsesource.v8.V8ScriptExecutionException])
//        assertEquals("undefined:1: ReferenceError: out is not defined", e.getMessage)
//      }
//    }

    val enum = Enumerator(List(Literal("x"),Literal("y")),new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("1","2"))),List("x","y")),Nil))
    assertTrue(enum.hasNext)
    while (enum.hasNext) {
      enum.next()
    }

    val foundX = try {
      V8Runtime.runtime.executeScript("x")
      true
    } catch {
      case e : Exception => {
        assertTrue(e.isInstanceOf[com.eclipsesource.v8.V8ScriptExecutionException])
        assertEquals("undefined:1: ReferenceError: x is not defined", e.getMessage)
        false
      }
    }
    assertFalse(foundX)

    val foundY = try {
      V8Runtime.runtime.executeScript("y")
      true
    } catch {
      case e : Exception => {
        assertTrue(e.isInstanceOf[com.eclipsesource.v8.V8ScriptExecutionException])
        assertEquals("undefined:1: ReferenceError: y is not defined", e.getMessage)
        false
      }
    }
    assertFalse(foundY)

  }

  @Test def numberifyABoolean(): Unit = {
    var result = runtime.executeScript("Number(true)")
    assertEquals(1,result)
    result = runtime.executeScript("Number(false)")
    assertEquals(0,result)
    result = runtime.executeScript("out = []; [4, 2, 5, 1, 3].sort((a, b) => {out.push([a,b,Number(b < a)]); return b < a}); out;")
    println(result)
  }
}
