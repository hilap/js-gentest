import org.junit.Test
import org.junit.Assert._
import org.scalatest.junit.JUnitSuite

class ExprToVocabTests extends JUnitSuite{
  @Test def literalToVocab() : Unit = {
    val node = Literal("[1,2,3]")
    val nl = node.toNodeList
    assertEquals(1,nl.size)
    assertTrue(nl.head.isInstanceOf[Literal])
    assertEquals("[1,2,3]",nl.head.func)
  }

  @Test def binopToVocab() : Unit = {
    val node = BinOperator("%")
    node.addChild(0,Literal("x"))
    node.addChild(1,BinOperator("in"))
    val nl = node.toNodeList
    assertEquals(3,nl.size)
    assertEquals(1,nl.filter(n => n.isInstanceOf[Literal] && n.func == "x").size)
    assertEquals(1,nl.filter(n => n.isInstanceOf[BinOperator] && n.func == "%" && n.toCode == "? % ?").size)
    assertEquals(1,nl.filter(n => n.isInstanceOf[BinOperator] && n.func == "in" && n.toCode == "? in ?").size)
  }

  @Test def astNodeToVocab() : Unit = {
    val node = new ASTNode("isNaN",1)
    node.addChild(0,new ASTNode("foo",2))
    node.children(0).get.addChild(1,Literal("y"))
    val nl = node.toNodeList
    assertEquals(3,nl.size)
    assertEquals(1,nl.filter(n => n.isInstanceOf[Literal] && n.func == "y").size)
    assertEquals(1,nl.filter(n => n.func == "isNaN" && n.toCode == "isNaN(?)").size)
    assertEquals(1,nl.filter(n => n.func == "foo" && n.toCode == "foo(?,?)").size)
  }

  @Test def methodCallToVocab() : Unit = {
    val node = new MethodCall("bar",2)
    node.addChild(0,UnPrefOperator("typeof"))
    node.addChild(2,BinOperator("+"))
    node.children(2).get.addChild(1,Literal("y"))
    val nl = node.toNodeList
    assertEquals(4,nl.size)
    assertEquals(1,nl.filter(n => n.isInstanceOf[MethodCall] && n.func == "bar" && n.toCode == "?.bar(?,?)").size)
  }

  @Test def arrayDerefToVocab() : Unit = {
    val node = ArrayDeref()
    node.addChild(0,Literal("x"))
    node.addChild(1,Literal("y"))
    val nl = node.toNodeList
    assertEquals(3,nl.size)
    assertEquals(1,nl.filter(n => n.isInstanceOf[ArrayDeref] && n.toCode == "?[?]").size)
  }

  @Test def propCallToVocab() : Unit = {
    val node = PropCall("length")
    node.addChild(0,Literal("x"))
    val nl = node.toNodeList
    assertEquals(2,nl.size)
    assertEquals(1,nl.filter(n => n.isInstanceOf[PropCall] && n.toCode == "?.length").size)
    assertEquals(1,nl.filter(n => n.isInstanceOf[Literal] && n.func == "x").size)
  }

  @Test def aggMethodToVocab() : Unit = {
    AggregatorMethod.count = 0
    val node = AggregatorMethod("map",1,1)
    node.addChild(0,Literal(node.innerVar(0)))
    val nl = node.toNodeList
    assertEquals(1,nl.size)
    assertTrue(nl.head.isInstanceOf[AggregatorMethod])
    assertEquals("?.map(x1 => ?)",nl.head.toCode)

    val node2 = AggregatorMethod("reduce",1,2)
    node2.addChild(0,Literal("a"))
    val func = BinOperator("+")
    node2.innerVar.zipWithIndex.foreach(v => func.addChild(v._2,Literal(v._1)))
    node2.addChild(1,func)
    val nl2 = node2.toNodeList
    assertEquals(3,nl2.size)
    assertEquals(Set("a","+","reduce"),nl2.map(n => n.func).toSet)
  }

  @Test def ctorToVocab() : Unit = {
    val node = Constructor("List",3)
    List(0,1,2).foreach(x => node.addChild(x,Literal(x.toString)))
    val nl = node.toNodeList
    assertEquals(4,nl.size)
    assertEquals(3,(nl & List(0,1,2).map(x => Literal(x.toString)).toSet).size)
    assertEquals(1,nl.filter(n => n.isInstanceOf[Constructor] && n.func == "List").size)
  }

  @Test def dupToVocab() : Unit = {
    val node = BinOperator("+")
    node.addChild(0,BinOperator("+"))
    assertEquals(1,node.toNodeList.size)
  }

  @Test def exprToFullNodesList() : Unit = {
    val expr = JSParser.deconstruct("isNaN(1 + 2) == true")
    val list = expr.toListOfFullNodes
    assertEquals(6,list.size)
    assertEquals("1",list.head.toCode)
    assertEquals("2",list(1).toCode)
    assertEquals("1 + 2", list(2).toCode)
    assertEquals("isNaN(1 + 2)", list(3).toCode)
    assertEquals("true", list(4).toCode)
    assertEquals("(isNaN(1 + 2)) == true", list.last.toCode)
  }
}
