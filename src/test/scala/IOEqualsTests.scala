import com.eclipsesource.v8.V8Value
import org.junit.Test
import org.scalatest.junit.JUnitSuite
import org.junit.Assert._

class IOEqualsTests extends JUnitSuite{

  @Test def twoArrayEquals() : Unit = {
    val inputOutputs = List(("[1,2,3,4]","[2,3,4,5,2,3,4,5]"))
    val executor = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val executorNoV = new ExecutorNoVal
    val resexec = executor.execute("input.map(x1 => x1 + 1).concat(input.map(x1 => x1 + 1))")
    val res : List[FromJSWrapper] = resexec.get
    val compexec = inputOutputs.map(io => executorNoV.execute(io._2).get.head)
    val comp : List[FromJSWrapper] = compexec
    resexec.release()
    compexec.foreach(o => if (o.isInstanceOf[V8Value]) o.asInstanceOf[V8Value].release())
    assertTrue(res == comp)
  }


  @Test def twoNaNs() : Unit = {
    val executor = new ExecutorWithVal(List(VarInputs(List("[1,2,3,4]"))))
    val resexec1 = executor.execute("(input.map(x1 => (1 + 1) + 1)) - (input.map(x1 => (1 - x1) + (1 + [])))")
    val resexec2 = executor.execute("(input.map(x1 => (1 + 1) + 1)) - (input.map(x1 => (1 - 1) + ([] + x1)))")
    val res1 : List[FromJSWrapper] = resexec1.get
    val res2 : List[FromJSWrapper] = resexec2.get
    assertTrue(res1 == res2)
    resexec1.release()
    resexec2.release()
  }

  @Test def infinities() : Unit = {
    val executor = new ExecutorNoVal
    val resexec1 = executor.execute("1/0")
    val resexec2 = executor.execute("Infinity")
    val resexec3 = executor.execute("-1/0")

    val res1 : List[FromJSWrapper] = resexec1.get
    val res2 : List[FromJSWrapper] = resexec2.get
    val res3 : List[FromJSWrapper] = resexec3.get

    assertTrue(res1 == res2)
    assertFalse(res2 == res3)
    assertFalse(res1 == res3)
    resexec1.release()
    resexec2.release()
    resexec3.release()

  }


}
