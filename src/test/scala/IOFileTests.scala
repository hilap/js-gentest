import SubtreeSnipMain.hookAllValues
import org.junit.Test
import org.scalatest.junit.JUnitSuite
import org.junit.Assert._

class IOFileTests extends JUnitSuite{

  @Test def ioNumbers() : Unit = {
    val fileContent =
      "'1'\t'2'\n" +
        "'3'\t'4'"

    val inputOutputs = MainUtils.parseIOLines(fileContent.lines)
    assertEquals(List(("1","2"), ("3", "4")), inputOutputs)

    //get outputs
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    assertEquals(2, outexec.length)
    assertTrue(outexec.forall(res => res.isInstanceOf[NonV8AnyRef]))
    assertEquals(2, outexec.head.value)
    assertEquals(4, outexec.last.value)


    val mainExec = new ExecutorWithVal(inputOutputs.map(io => VarInputs(List(io._1))))
    val parent = ASTNode("isNaN(?)")
    val child = Literal("3")
    val (vars,varValues) = hookAllValues(parent,child,List(0), mainExec)
    assertEquals(2, varValues.size)
    assertEquals(List("input"), vars)
    assertTrue(varValues.contains(VarInputs(List("1"))))
    assertTrue(varValues.contains(VarInputs(List("3"))))
  }

  @Test def ioStrings : Unit = {
    val fileContent =
      "'\"abc\"'\t'\"def\"'"

    val inputOutputs = MainUtils.parseIOLines(fileContent.lines)
    assertEquals(List(("\"abc\"","\"def\"")), inputOutputs)

    //get outputs
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    assertEquals(1, outexec.length)
    assertTrue(outexec.forall(res => res.isInstanceOf[NonV8AnyRef]))
    assertEquals("def", outexec.head.value)


    val mainExec = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val parent = ASTNode("isNaN(?)")
    val child = Literal("3")
    val (vars,varValues) = hookAllValues(parent,child,List(0), mainExec)
    assertEquals(List("input"),vars)
    assertEquals(1, varValues.size)
    assertEquals(1,varValues.head.inputs.length)
    assertEquals("'abc'",varValues.head.inputs.head)
  }

  @Test def ioArraysOfNums() : Unit = {
    val fileContent =
      "'[1,2,3,4]'\t'[1,2,3,4,5,6]'\n" +
      "'[100,200]'\t'[-80,-910]'\n" +
      "'[1.2,0]'\t'[-5.5,-3.333]'"

    val inputOutputs = MainUtils.parseIOLines(fileContent.lines)
    assertEquals(List(("[1,2,3,4]","[1,2,3,4,5,6]"), ("[100,200]","[-80,-910]"), ("[1.2,0]","[-5.5,-3.333]")), inputOutputs)

    //get outputs
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    assertEquals(3, outexec.length)
    assertTrue(outexec.forall(res => res.isInstanceOf[JSArrayWrapper]))
    assertEquals("[1,2,3,4,5,6]", outexec.head.value)
    assertEquals("[-80,-910]", outexec(1).value)
    assertEquals("[-5.5,-3.333]", outexec(2).value)


    val mainExec = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val parent = ASTNode("isNaN(?)")
    val child = Literal("3")
    val (vars,varValues) = hookAllValues(parent,child,List(0), mainExec)
    assertEquals(3, varValues.size)
    assertTrue(varValues.contains(VarInputs(List("JSON.parse('[1,2,3,4]')"))))
    assertTrue(varValues.contains(VarInputs(List("JSON.parse('[100,200]')"))))
    assertTrue(varValues.contains(VarInputs(List("JSON.parse('[1.2,0]')"))))

    val inputs = mainExec.execute("input")
    val cbValues = AggregatorParamsIter.getCallbackValues(inputs.get.map(_.asInstanceOf[JSArrayWrapper]),inputs.get.map(i => VarInputs(List(i.toCode))),mainExec.varNames,"map",1)
    assertEquals(8, cbValues.length)
    assertEquals(Set("1","2","3","4","100","200","1.2","0"), cbValues.map(v => v.inputs(1)).toSet)
  }

  @Test def ioArraysOfStrings() : Unit = {
    val fileContent =
      "'[\"That dog there\",\"what a human cat\"]'\t'[\"dog\", \"human\", \"cat\" ]'\n" +
      "'[\"woe betide the human human cat dog\",\"what is love\"]'\t'[ \"human\", \"human\", \"cat\", \"dog\"  ]'"

    val inputOutputs = MainUtils.parseIOLines(fileContent.lines)
    assertEquals(List(("[\"That dog there\",\"what a human cat\"]","[\"dog\", \"human\", \"cat\" ]"), ("[\"woe betide the human human cat dog\",\"what is love\"]","[ \"human\", \"human\", \"cat\", \"dog\"  ]")), inputOutputs)

    //get outputs
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    assertEquals(2, outexec.length)
    assertTrue(outexec.forall(res => res.isInstanceOf[JSArrayWrapper]))
    assertEquals("[\"dog\",\"human\",\"cat\"]", outexec.head.value)
    assertEquals("[\"human\",\"human\",\"cat\",\"dog\"]", outexec(1).value)

    val mainExec = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val parent = ASTNode("isNaN(?)")
    val child = Literal("3")
    val (vars,varValues) = hookAllValues(parent,child,List(0), mainExec)
    assertEquals(2, varValues.size)
    assertTrue(varValues.map(v => v.inputs(0)).contains("JSON.parse('[\"That dog there\",\"what a human cat\"]')"))
    assertTrue(varValues.map(v => v.inputs(0)).contains("JSON.parse('[\"woe betide the human human cat dog\",\"what is love\"]')"))

    val inputs = mainExec.execute("input")
    val cbValues = AggregatorParamsIter.getCallbackValues(inputs.get.map(_.asInstanceOf[JSArrayWrapper]),inputs.get.map(i => VarInputs(List(i.toCode))),mainExec.varNames,"map",1)
    assertEquals(4, cbValues.length)
    assertEquals(Set("'That dog there'","'what a human cat'","'woe betide the human human cat dog'","'what is love'"), cbValues.map(v => v.inputs(1)).toSet)
  }
  @Test def ioArraysOfStringsWithEscaping() : Unit = {
    val fileContent = "'[\"baby don\\'t hurt me\"]'\t'[\"don\\'t hurt me no more\"]'"
    val inputOutputs = MainUtils.parseIOLines(fileContent.lines)
    assertEquals(List(("[\"baby don\\'t hurt me\"]","[\"don\\'t hurt me no more\"]")), inputOutputs)

    //get outputs
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    assertEquals(1, outexec.length)
    assertTrue(outexec.forall(res => res.isInstanceOf[JSArrayWrapper]))
    assertEquals("[\"don't hurt me no more\"]", outexec.head.value)

    val mainExec = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val parent = ASTNode("isNaN(?)")
    val child = Literal("3")
    val (vars,varValues) = hookAllValues(parent,child,List(0), mainExec)
    assertEquals(List("input"), vars)
    assertEquals(1, varValues.size)
    assertEquals(VarInputs(List("JSON.parse('[\"baby don\\'t hurt me\"]')")),varValues.head)

    val inputs = mainExec.execute("input")
    val cbValues = AggregatorParamsIter.getCallbackValues(inputs.get.map(_.asInstanceOf[JSArrayWrapper]),mainExec.inputVals,mainExec.varNames,"map",1)
    assertEquals(1, cbValues.length)
    assertEquals(2, cbValues.head.inputs.length)
    assertEquals("[\"baby don\\'t hurt me\"]",cbValues.head.inputs.head)
    assertEquals("'baby don\\'t hurt me'",cbValues.head.inputs.last)
  }
  @Test def ioArraysOfStringsWithEscaping2() : Unit = {
    val fileContent = "'[\"abc\",\"d\\\"f\"]'\t'[1,2,\"\\\"\"]'"
    val inputOutputs = MainUtils.parseIOLines(fileContent.lines)
    assertEquals(List(("[\"abc\",\"d\\\"f\"]","[1,2,\"\\\"\"]")), inputOutputs)

    //get outputs
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    assertEquals(1, outexec.length)
    assertTrue(outexec.forall(res => res.isInstanceOf[JSArrayWrapper]))
    assertEquals("[1,2,\"\\\"\"]", outexec.head.value)

    val mainExec = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val parent = ASTNode("isNaN(?)")
    val child = Literal("3")
    val (vars,varValues) = hookAllValues(parent,child,List(0), mainExec)
    assertEquals(1, varValues.size)
    assertEquals(List("JSON.parse('[\"abc\",\"d\\\"f\"]')"),varValues.head.inputs)

    val inputs = mainExec.execute("input")

    val cbValues = AggregatorParamsIter.getCallbackValues(inputs.get.map(_.asInstanceOf[JSArrayWrapper]),inputs.get.map(i => VarInputs(List(i.toCode))),mainExec.varNames,"map",1)
    assertEquals(1, cbValues.length)
    assertEquals(2, cbValues.head.inputs.length)
    assertEquals(Set("'abc'","'d\"f'"), cbValues.head.inputs.toSet)
  }
  @Test def ioObject() : Unit = {
    val fileContent =
      "'{\"a\":2,\"b\":\"c\",\"d\":[1,2,\"f\"]}'\t'{\"x\":\"a\",\"y\":[1,2,\"g\"]}'\n" +
      "'{}'\t'{\"don\\'t\":\"cry\"}'\n" +
      "'{\"don\\'t\":\"cry\"}'\t'{}'"

    val inputOutputs = MainUtils.parseIOLines(fileContent.lines)
    assertEquals(3, inputOutputs.length)
    assertEquals(List(("{\"a\":2,\"b\":\"c\",\"d\":[1,2,\"f\"]}","{\"x\":\"a\",\"y\":[1,2,\"g\"]}"),
                      ("{}", "{\"don\\'t\":\"cry\"}"),
                      ("{\"don\\'t\":\"cry\"}","{}")), inputOutputs)

    //get outputs
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    assertEquals(3, outexec.length)
    assertTrue(outexec.forall(res => res.isInstanceOf[JSValueWrapper]))
    assertEquals("{\"x\":\"a\",\"y\":[1,2,\"g\"]}", outexec.head.value)
    assertEquals("{\"don't\":\"cry\"}", outexec(1).value)
    assertEquals("{}", outexec(2).value)


    val mainExec = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val parent = ASTNode("isNaN(?)")
    val child = Literal("3")
    val (vars,varValues) = hookAllValues(parent,child,List(0), mainExec)
    assertEquals(3, varValues.size)
    assertTrue(varValues.contains(VarInputs(List("JSON.parse('{}')"))))
    assertTrue(varValues.contains(VarInputs(List("JSON.parse('{\"don\\'t\":\"cry\"}')"))))
    assertTrue(varValues.contains(VarInputs(List("JSON.parse('{\"a\":2,\"b\":\"c\",\"d\":[1,2,\"f\"]}')"))))
  }
  @Test def ioArrayOfObjects() : Unit = {
    val fileContent = "'[{\"firstName\":\"Sofia\",\"lastName\":\"I.\",\"country\":\"Argentina\",\"continent\":\"Americas\",\"age\":35,\"language\":\"Java\"},{\"firstName\":\"Lukas\",\"lastName\":\"X.\",\"country\":\"Croatia\",\"continent\":\"Europe\",\"age\":35,\"language\":\"Python\"},{\"firstName\":\"Madison\",\"lastName\":\"U.\",\"country\":\"United States\",\"continent\":\"Americas\",\"age\":32,\"language\":\"Ruby\"}]'\t'[{\"firstName\":\"Sofia\",\"lastName\":\"I.\",\"country\":\"Argentina\",\"continent\":\"Americas\",\"age\":35,\"language\":\"Java\",\"greeting\":\"Hi Sofia, what do you like the most about Java?\"},{\"firstName\":\"Lukas\",\"lastName\":\"X.\",\"country\":\"Croatia\",\"continent\":\"Europe\",\"age\":35,\"language\":\"Python\",\"greeting\":\"Hi Lukas, what do you like the most about Python?\"},{\"firstName\":\"Madison\",\"lastName\":\"U.\",\"country\":\"United States\",\"continent\":\"Americas\",\"age\":32,\"language\":\"Ruby\",\"greeting\":\"Hi Madison, what do you like the most about Ruby?\"}]'"

    val inputOutputs = MainUtils.parseIOLines(fileContent.lines)
    assertEquals(List(("[{\"firstName\":\"Sofia\",\"lastName\":\"I.\",\"country\":\"Argentina\",\"continent\":\"Americas\",\"age\":35,\"language\":\"Java\"},{\"firstName\":\"Lukas\",\"lastName\":\"X.\",\"country\":\"Croatia\",\"continent\":\"Europe\",\"age\":35,\"language\":\"Python\"},{\"firstName\":\"Madison\",\"lastName\":\"U.\",\"country\":\"United States\",\"continent\":\"Americas\",\"age\":32,\"language\":\"Ruby\"}]",
      "[{\"firstName\":\"Sofia\",\"lastName\":\"I.\",\"country\":\"Argentina\",\"continent\":\"Americas\",\"age\":35,\"language\":\"Java\",\"greeting\":\"Hi Sofia, what do you like the most about Java?\"},{\"firstName\":\"Lukas\",\"lastName\":\"X.\",\"country\":\"Croatia\",\"continent\":\"Europe\",\"age\":35,\"language\":\"Python\",\"greeting\":\"Hi Lukas, what do you like the most about Python?\"},{\"firstName\":\"Madison\",\"lastName\":\"U.\",\"country\":\"United States\",\"continent\":\"Americas\",\"age\":32,\"language\":\"Ruby\",\"greeting\":\"Hi Madison, what do you like the most about Ruby?\"}]")), inputOutputs)

    //get outputs
    val executorNoVal = new ExecutorNoVal
    val outexec = inputOutputs.map(io => executorNoVal.execute(io._2).get.head)
    assertEquals(1, outexec.length)
    assertTrue(outexec.forall(res => res.isInstanceOf[JSArrayWrapper]))
    //assertEquals("[{\"firstName\":\"Sofia\",\"lastName\":\"I.\",\"country\":\"Argentina\",\"continent\":\"Americas\",\"age\":35,\"language\":\"Java\",\"greeting\":\"Hi Sofia, what do you like the most about Java?\"},{\"firstName\":\"Lukas\",\"lastName\":\"X.\",\"country\":\"Croatia\",\"continent\":\"Europe\",\"age\":35,\"language\":\"Python\",\"greeting\":\"Hi Lukas, what do you like the most about Python?\"},{\"firstName\":\"Madison\",\"lastName\":\"U.\",\"country\":\"United States\",\"continent\":\"Americas\",\"age\":32,\"language\":\"Ruby\",\"greeting\":\"Hi Madison, what do you like the most about Ruby?\"}]", outexec.head.value)
    //values now sorted
    assertEquals("""[{"age":35,"continent":"Americas","country":"Argentina","firstName":"Sofia","greeting":"Hi Sofia, what do you like the most about Java?","language":"Java","lastName":"I."},{"age":35,"continent":"Europe","country":"Croatia","firstName":"Lukas","greeting":"Hi Lukas, what do you like the most about Python?","language":"Python","lastName":"X."},{"age":32,"continent":"Americas","country":"United States","firstName":"Madison","greeting":"Hi Madison, what do you like the most about Ruby?","language":"Ruby","lastName":"U."}]""", outexec.head.value)

    val mainExec = new ExecutorWithVal(inputOutputs.map(x => VarInputs(List(x._1))))
    val parent = ASTNode("isNaN(?)")
    val child = Literal("3")
    val (vars,varValues) = hookAllValues(parent,child,List(0), mainExec)
    assertEquals(1, varValues.size)
    assertEquals(VarInputs(List("JSON.parse('[{\"age\":35,\"continent\":\"Americas\",\"country\":\"Argentina\",\"firstName\":\"Sofia\",\"language\":\"Java\",\"lastName\":\"I.\"},{\"age\":35,\"continent\":\"Europe\",\"country\":\"Croatia\",\"firstName\":\"Lukas\",\"language\":\"Python\",\"lastName\":\"X.\"},{\"age\":32,\"continent\":\"Americas\",\"country\":\"United States\",\"firstName\":\"Madison\",\"language\":\"Ruby\",\"lastName\":\"U.\"}]')")),varValues.head)

    val inputs = mainExec.execute("input")
    val cbValues = AggregatorParamsIter.getCallbackValues(inputs.get.map(_.asInstanceOf[JSArrayWrapper]),mainExec.inputVals,mainExec.varNames,"map",1)
    assertEquals(3, cbValues.length)
    assertTrue(cbValues.contains(VarInputs(List("[{\"firstName\":\"Sofia\",\"lastName\":\"I.\",\"country\":\"Argentina\",\"continent\":\"Americas\",\"age\":35,\"language\":\"Java\"},{\"firstName\":\"Lukas\",\"lastName\":\"X.\",\"country\":\"Croatia\",\"continent\":\"Europe\",\"age\":35,\"language\":\"Python\"},{\"firstName\":\"Madison\",\"lastName\":\"U.\",\"country\":\"United States\",\"continent\":\"Americas\",\"age\":32,\"language\":\"Ruby\"}]","JSON.parse('{\"age\":35,\"continent\":\"Americas\",\"country\":\"Argentina\",\"firstName\":\"Sofia\",\"language\":\"Java\",\"lastName\":\"I.\"}')"))))
    assertTrue(cbValues.contains(VarInputs(List("[{\"firstName\":\"Sofia\",\"lastName\":\"I.\",\"country\":\"Argentina\",\"continent\":\"Americas\",\"age\":35,\"language\":\"Java\"},{\"firstName\":\"Lukas\",\"lastName\":\"X.\",\"country\":\"Croatia\",\"continent\":\"Europe\",\"age\":35,\"language\":\"Python\"},{\"firstName\":\"Madison\",\"lastName\":\"U.\",\"country\":\"United States\",\"continent\":\"Americas\",\"age\":32,\"language\":\"Ruby\"}]","JSON.parse('{\"age\":35,\"continent\":\"Europe\",\"country\":\"Croatia\",\"firstName\":\"Lukas\",\"language\":\"Python\",\"lastName\":\"X.\"}')"))))
    assertTrue(cbValues.contains(VarInputs(List("[{\"firstName\":\"Sofia\",\"lastName\":\"I.\",\"country\":\"Argentina\",\"continent\":\"Americas\",\"age\":35,\"language\":\"Java\"},{\"firstName\":\"Lukas\",\"lastName\":\"X.\",\"country\":\"Croatia\",\"continent\":\"Europe\",\"age\":35,\"language\":\"Python\"},{\"firstName\":\"Madison\",\"lastName\":\"U.\",\"country\":\"United States\",\"continent\":\"Americas\",\"age\":32,\"language\":\"Ruby\"}]","JSON.parse('{\"age\":32,\"continent\":\"Americas\",\"country\":\"United States\",\"firstName\":\"Madison\",\"language\":\"Ruby\",\"lastName\":\"U.\"}')"))))
  }

}
