import com.eclipsesource.v8.{V8, V8Array}
import org.junit.Test
import org.junit.Assert._
import org.scalatest.junit.JUnitSuite

class JSWrapperTests  extends JUnitSuite{
  val runtime = V8Runtime.runtime
  @Test def execJSArrayEquality() : Unit = {
//    val arr1 = new V8Array(runtime).push(1).push(2).push(3)
//    val arr2 = new V8Array(runtime).push(1).push(2).push(3)
//    assertFalse(arr1 == arr2)
//    val arr1Wrapped : FromJSWrapper = FromJSWrapper(arr1)
//    val arr2Wrapped : FromJSWrapper = FromJSWrapper(arr2)
//    arr1.release()
//    arr2.release()
//    assertTrue(arr1Wrapped.hashCode() == arr2Wrapped.hashCode())
//    assertTrue(arr1Wrapped == arr2Wrapped)
  }

  @Test def execListEquality() : Unit = {
    val l1 : List[AnyRef] = List(new java.lang.Double(-0.0))
    val l1Wrapped : List[FromJSWrapper] = l1.map(x => NonV8AnyRef(x))
    val l2 : List[AnyRef] = List(new java.lang.Integer(0))
    val l2Wrapped : List[FromJSWrapper] = l2.map(x => NonV8AnyRef(x))
    assertTrue(l1Wrapped.hashCode() == l2Wrapped.hashCode())
    assertTrue(l1Wrapped == l2Wrapped)
  }

  @Test def numberFromV8ToV8() : Unit = {
    val exec = new ExecutorNoVal
    val res = exec.execute("2.6").get.head
    assertTrue(res.isInstanceOf[NonV8AnyRef])
    assertEquals(2.6, res.value)
    val vexec = new ExecutorWithVal(List(VarInputs(List(res.toCode))))
    val res2 = vexec.execute("input")
    assertTrue(res == res2.get.head)
  }

  @Test def boolFromV8ToV8() : Unit = {
    val exec = new ExecutorNoVal
    val res = exec.execute("true").get.head
    assertTrue(res.isInstanceOf[NonV8AnyRef])
    assertEquals(true,res.value)
    val vexec = new ExecutorWithVal(List(VarInputs(List(res.toCode))))
    val res2 = vexec.execute("input")
    assertTrue(res == res2.get.head)
  }

  @Test def stringFromV8ToV8() : Unit = {
    val exec = new ExecutorNoVal
    val res = exec.execute("'abc'").get.head
    assertTrue(res.isInstanceOf[NonV8AnyRef])
    assertEquals("abc",res.value)
    val vexec = new ExecutorWithVal(List(VarInputs(List(res.toCode))))
    val res2 = vexec.execute("input")
    assertTrue(res == res2.get.head)
  }


  @Test def stringWithEscapingFromV8ToV8() : Unit = {
    val exec = new ExecutorNoVal
    val res = exec.execute("'ab\"c'").get.head
    assertTrue(res.isInstanceOf[NonV8AnyRef])
    assertEquals("ab\"c",res.value)
    val vexec = new ExecutorWithVal(List(VarInputs(List(res.toCode))))
    val res2 = vexec.execute("input")
    assertTrue(res == res2.get.head)
  }

  @Test def stringWithEscapingFromV8ToV8_2() : Unit = {
    val exec = new ExecutorNoVal
    val res = exec.execute("'ab\\'c'").get.head
    assertTrue(res.isInstanceOf[NonV8AnyRef])
    assertEquals("ab'c",res.value)
    val vexec = new ExecutorWithVal(List(VarInputs(List(res.toCode))))
    val res2 = vexec.execute("input")
    assertTrue(res == res2.get.head)
  }

  @Test def objectFromV8ToV8() : Unit = {
    val exec = new ExecutorNoVal
    val res = exec.execute("{a: 8}").get.head
    assertTrue(res.isInstanceOf[JSValueWrapper])
    assertEquals("{\"a\":8}",res.value)
    val vexec = new ExecutorWithVal(List(VarInputs(List(res.toCode))))
    val res2 = vexec.execute("input")
    assertTrue(res == res2.get.head)
  }

  @Test def arrayFromV8ToV8() : Unit = {
    val exec = new ExecutorNoVal
    val res = exec.execute("[1,2,3]").get.head
    assertTrue(res.isInstanceOf[JSArrayWrapper])
    assertEquals("[1,2,3]",res.value)
    val vexec = new ExecutorWithVal(List(VarInputs(List(res.toCode))))
    val res2 = vexec.execute("input")
    assertTrue(res == res2.get.head)
  }

  @Test def mapFromV8ToV8() : Unit = {
    val exec = new ExecutorNoVal
    val res = exec.execute("new Map([[1,2]])").get.head
    assertTrue(res.isInstanceOf[JSMapWrapper])
    assertEquals("[[1,2]]",res.value)
    val vexec = new ExecutorWithVal(List(VarInputs(List(res.toCode))))
    val res2 = vexec.execute("input.get(1)")
    assertTrue(res2.get.head.isInstanceOf[NonV8AnyRef])
    assertEquals(2,res2.get.head.value)
  }

  @Test def dataTypeInt() : Unit = {
    val exec = new ExecutorNoVal
    val r1 = exec.execute("1").get.head
    assertTrue(r1.isInstanceOf[NonV8AnyRef])
    assertEquals(DataType.number,r1.getType)
  }

  @Test def dataTypeDouble() : Unit = {
    val exec = new ExecutorNoVal
    val r1 = exec.execute("1.0").get.head
    assertTrue(r1.isInstanceOf[NonV8AnyRef])
    assertEquals(DataType.number,r1.getType)
  }

  @Test def dataTypeBoolean() : Unit = {
    val exec = new ExecutorNoVal
    val r1 = exec.execute("true").get.head
    assertTrue(r1.isInstanceOf[NonV8AnyRef])
    assertEquals(DataType.boolean,r1.getType)
  }

  @Test def dataTypeString() : Unit = {
    val exec = new ExecutorNoVal
    val r1 = exec.execute("'a'").get.head
    assertTrue(r1.isInstanceOf[NonV8AnyRef])
    assertEquals(DataType.string,r1.getType)
  }

  @Test def dataTypeUndefined() : Unit = {
    val exec = new ExecutorNoVal
    val r1 = exec.execute("undefined").get.head
    assertTrue(r1.isInstanceOf[JSUndefined])
    assertEquals(DataType.undefined,r1.getType)
  }

  @Test def dataTypeObj() : Unit = {
    val exec = new ExecutorNoVal
    val r1 = exec.execute("{}").get.head
    assertTrue(r1.isInstanceOf[JSValueWrapper])
    assertEquals(DataType.obj,r1.getType)
  }

  @Test def dataTypeArray() : Unit = {
    val exec = new ExecutorNoVal
    val r1 = exec.execute("[]").get.head
    assertTrue(r1.isInstanceOf[JSArrayWrapper])
    assertEquals(DataType.array,r1.getType)
  }

  @Test def dataTypeMap() : Unit = {
    val exec = new ExecutorNoVal
    val r1 = exec.execute("new Map").get.head
    assertTrue(r1.isInstanceOf[JSMapWrapper])
    assertEquals(DataType.map,r1.getType)
  }
}
