import org.junit.Test
import org.scalatest.junit.JUnitSuite
import org.junit.Assert._

class NodeCloneTests  extends JUnitSuite{
  @Test def deepCloneUntilASTNode : Unit = {
    val node = ASTNode("foo(?)")
    val res = node.deepCloneUntil(node)
    assertEquals((None,Nil),res)

    node.addChild(0,Literal("a"))
    val res2 = node.deepCloneUntil(node)
    assertEquals((None,Nil),res2)

    val res3 = node.deepCloneUntil(node.children.head.get)
    assertEquals("foo(?)", res3._1.get.toCode)
    assertEquals(List(0),res3._2)
  }

  @Test def shallowCloneASTNode : Unit = {
    val node = ASTNode("foo(?)")
    node.addChild(0,Literal("a"))
    val res = node.shallowClone
    assertEquals("foo(?)",res.toCode)
  }

  @Test def shallowCloneLiteral() : Unit = {
    val node = Literal("[]")
    val res = node.shallowClone
    assertFalse(node eq res)
    assertEquals("[]",res.toCode)
  }

  @Test def deepCloneUntilLiteral() : Unit = {
    val node = Literal("[]")
    val res : (Option[ASTNode],List[Int]) = node.deepCloneUntil(node)
    assertTrue(res._1.isEmpty)
    assertTrue(res._2.isEmpty)

    val node2 = ASTNode("foo(?)")
    node2.addChild(0,node)
    val res2 = node2.deepCloneUntil(node)
    assertEquals("foo(?)",res2._1.get.toCode)
    assertEquals(List(0),res2._2)
  }

  @Test def shallowCloneMethodCall() : Unit = {
    val node = new MethodCall("foo",2,1)
    val res = node.shallowClone

    assertFalse(node eq res)
    assertEquals("foo",res.func)
    assertEquals(3,res.arity)
    assertEquals(1,res.optionalArgs)
  }

  @Test def deepCloneUntilMethodCall() : Unit = {
    val node = new MethodCall("foo", 3, 1)
    node.addChild(0, Literal("a"))
    val foo = ASTNode("foo(?)")
    foo.addChild(0, Literal("b"))
    node.addChild(2, foo)
    val res = node.deepCloneUntil(foo)
    assertFalse(res._1.get eq node)
    assertEquals("a.foo(?,?)", res._1.get.toCode)
    assertEquals(List(2),res._2)
  }

  @Test def shallowCloneBinOperator() : Unit = {
    val node = BinOperator("+")
    node.addChild(0,Literal("x"))
    val res = node.shallowClone
    assertEquals("? + ?",res.toCode)
    assertTrue(res.isInstanceOf[BinOperator])
  }

  @Test def shallowCloneArrayDeref() : Unit = {
    val node = ArrayDeref()
    val res = node.shallowClone
    assertFalse(node eq res)
    assertTrue(res.isInstanceOf[ArrayDeref])
    assertTrue(res.asInstanceOf[ArrayDeref].lhs.isEmpty && res.asInstanceOf[ArrayDeref].rhs.isEmpty)
  }

  @Test def deepCloneUntilArrayDeref() : Unit = {
    val node = ArrayDeref()
    val child = BinOperator("+")
    child.addChild(0,Literal("a"))
    child.addChild(1,Literal("b"))
    node.addChild(0,child)
    node.addChild(1,Literal("8"))
    val res = node.deepCloneUntil(child.children.last.get)
    assertEquals("(a + ?)[8]",res._1.get.toCode)
    assertEquals(List(0,1),res._2)
  }

  @Test def shallowCloneProp() : Unit = {
    val node = PropCall("length")
    node.addChild(0,Literal("x"))
    val res = node.shallowClone
    assertTrue(res.isInstanceOf[PropCall])
    assertEquals("?.length",res.toCode)
  }

  @Test def deepCloneUntilProp() : Unit = {
    val node = PropCall("length")
    val child = ASTNode("concat(?,?)")
    child.addChild(0,Literal("x"))
    node.addChild(0,child)
    val res = node.deepCloneUntil(child.children.head.get)
    assertEquals("(concat(?,?)).length",res._1.get.toCode)
    assertEquals(List(0,0),res._2)
  }

  @Test def shallowCloneAggregatorMethod() : Unit = {
    val node = JSParser.deconstruct("input.map((x,y) => x + y)")
    val res = node.shallowClone
    assertTrue(res.isInstanceOf[AggregatorMethod])
    assertFalse(res eq node)
    assertEquals("?.map((x,y) => ?)",res.toCode)
  }

  @Test def deepCloneUntilAggregatorMethod() : Unit = {
    val node = JSParser.deconstruct("input.map((x,y) => x + y)")
    val res = node.deepCloneUntil(node.children(1).get.children(0).get)
    assertTrue(res._1.get.isInstanceOf[AggregatorMethod])
    assertFalse(res._1.get eq node)
    assertEquals("input.map((x,y) => ? + y)",res._1.get.toCode)
  }

  @Test def shallowCloneCtor() : Unit = {
    val node = Constructor("Set",3)
    node.addChild(0,Literal("1"))
    node.addChild(1,Literal("2"))
    node.addChild(2,Literal("3"))
    val res = node.shallowClone
    assertTrue(res.isInstanceOf[Constructor])
    assertFalse(res eq node)
    assertEquals("new Set(?,?,?)",res.toCode)
  }

  @Test def deepCloneUntilCtor() : Unit = {
    val node = Constructor("Set",3)
    node.addChild(0,Literal("1"))
    node.addChild(1,Literal("2"))
    node.addChild(2,Literal("3"))
    val res = node.deepCloneUntil(null)
    assertTrue(res._1.get.isInstanceOf[Constructor])
    assertFalse(res._1.get eq node)
    assertEquals("new Set(1,2,3)",res._1.get.toCode)
    assertEquals(Nil,res._2)
  }

  @Test def shallowCloneObjectInit() : Unit = {
    val node = ObjectInitializer(2)
    node.addChild(0,Literal("'x'"))
    node.addChild(3,Literal("true"))
    val res = node.shallowClone
    assertTrue(res.isInstanceOf[ObjectInitializer])
    assertEquals(4,res.arity)
    assertEquals("{?:?,?:?}",res.toCode)
  }
  @Test def deepCloneObjectInit() : Unit = {
    val node = ObjectInitializer(2)
    node.addChild(0,Literal("'x'"))
    node.addChild(3,Literal("true"))
    val res = node.deepClone
    assertTrue(res.isInstanceOf[ObjectInitializer])
    assertEquals(4,res.arity)
    assertTrue(res.children(0).get.isInstanceOf[Literal])
    assertTrue(res.children(3).get.isInstanceOf[Literal])
    assertTrue(res.children(1).isEmpty)
    assertTrue(res.children(2).isEmpty)
    assertEquals("{'x':?,?:true}",res.toCode)
  }
  @Test def shallowCloneArrayInit() : Unit = {
    val node = ArrayInitializer(2)
    node.addChild(0,Literal("1"))
    node.addChild(1,Literal("x"))
    val res = node.shallowClone
    assertEquals(2, res.arity)
    assertEquals("[,]",res.func)
    assertTrue(res.children.forall(c => c.isEmpty))
  }
  @Test def deepCloneArrayInit() : Unit = {
    val node = ArrayInitializer(3)
    node.addChild(1,Literal("x"))
    val res = node.deepClone
    assertEquals(3, res.arity)
    assertEquals("[,]",res.func)
    assertTrue(res.children(0).isEmpty)
    assertFalse(res.children(1).isEmpty)
    assertTrue(res.children(2).isEmpty)
    assertEquals("[?,x,?]",res.toCode)
  }
}
