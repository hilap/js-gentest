import vocabconfig.lexer._
import vocabconfig.parser._
import org.junit.Test
import org.junit.Assert._
import org.scalatest.junit.JUnitSuite

class NodeParserTests extends JUnitSuite{

  @Test def recognizeTokens() : Unit = {
    val t = VocabLexer("foo")
    assertTrue(t.isRight)
    assertEquals(1,t.right.get.length)
    assertTrue(t.right.get.head.isInstanceOf[IDENTIFIER])
    assertEquals("foo",t.right.get.head.asInstanceOf[IDENTIFIER].str)

    val t2 = VocabLexer("foo()")
    assertTrue(t2.isRight)
    assertEquals(3,t2.right.get.length)

    val t3 = VocabLexer("(? x ??) new ., => = >")
    assertTrue(t3.isRight)
    assertEquals(11,t3.right.get.length)
    assertTrue(t3.right.get(5).isInstanceOf[NEW])
    assertTrue(t3.right.get(6).isInstanceOf[DOT])
    assertTrue(t3.right.get(7).isInstanceOf[COMMA])
    assertTrue(t3.right.get(8).isInstanceOf[ARROW])
    assertTrue(t3.right.get(9).isInstanceOf[OP])
    assertTrue(t3.right.get(10).isInstanceOf[OP])
  }

  @Test def parseLiterals() : Unit = {
    val p1 = VocabParser("x")
    assertTrue(p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[LiteralNode])

    val p2 = VocabParser("2")
    assertTrue(p2.left.toString(), p2.isRight)
    assertTrue(p2.right.get.isInstanceOf[LiteralNode])

    val p3 = VocabParser("'abc'")
    assertTrue(p3.left.toString(), p3.isRight)
    assertTrue(p3.right.get.isInstanceOf[LiteralNode])

    val p4 = VocabParser("[]")
    assertTrue(p4.left.toString(), p4.isRight)
    assertTrue(p4.right.get.isInstanceOf[LiteralNode])
    assertEquals("[]",p4.right.get.asInstanceOf[LiteralNode].str)

    val p5 = VocabParser("[1,2,3]")
    assertTrue(p5.left.toString(), p5.isRight)
    assertTrue(p5.right.get.isInstanceOf[LiteralNode])
    assertEquals("[1,2,3]",p5.right.get.asInstanceOf[LiteralNode].str)
  }

  @Test def parseCtors() : Unit = {
    val p1 = VocabParser("new Set")
    assertTrue(p1.left.toString(), p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[NewNode])
    assertEquals("Set", p1.right.get.asInstanceOf[NewNode].clsName)

    val p2 = VocabParser("new Set()")
    assertTrue(p2.left.toString, p2.isRight)
    assertTrue(p2.right.get.isInstanceOf[NewNode])
    assertEquals("Set", p2.right.get.asInstanceOf[NewNode].clsName)
    assertEquals(List(),p2.right.get.asInstanceOf[NewNode].args.params)

    val p3 = VocabParser("new Set(?)")
    assertTrue(p3.left.toString, p3.isRight)
    assertTrue(p3.right.get.isInstanceOf[NewNode])
    assertEquals("Set", p3.right.get.asInstanceOf[NewNode].clsName)
    assertEquals(List(ReqParamNode()),p3.right.get.asInstanceOf[NewNode].args.params)

    val p4 = VocabParser("new Set(?,?)")
    assertTrue(p4.isRight)
    assertTrue(p4.right.get.isInstanceOf[NewNode])
    assertEquals("Set",p4.right.get.asInstanceOf[NewNode].clsName)
    assertEquals(List(ReqParamNode(),ReqParamNode()),p4.right.get.asInstanceOf[NewNode].args.params)

    val p5 = VocabParser("new Set(?,??)")
    assertTrue(p5.left.toString(), p5.isRight)
    assertTrue(p5.right.get.isInstanceOf[NewNode])
    assertEquals("Set",p5.right.get.asInstanceOf[NewNode].clsName)
    assertEquals(List(ReqParamNode(),OptParamNode()),p5.right.get.asInstanceOf[NewNode].args.params)

    val p6 = VocabParser("new Set(??)")
    assertTrue(p6.left.toString(),p6.isRight)
    assertTrue(p6.right.get.isInstanceOf[NewNode])
    assertEquals("Set",p6.right.get.asInstanceOf[NewNode].clsName)
    assertEquals(List(OptParamNode()),p6.right.get.asInstanceOf[NewNode].args.params)
  }

  @Test def parseCall() : Unit = {
    val p1 = VocabParser("Number()")
    assertTrue(p1.left.toString, p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[FuncNode])
    assertEquals("Number",p1.right.get.asInstanceOf[FuncNode].fname)
    assertEquals(0,p1.right.get.asInstanceOf[FuncNode].args.params.length)

    val p2 = VocabParser("isNaN(?)")
    assertTrue(p2.left.toString, p2.isRight)
    assertTrue(p2.right.get.isInstanceOf[FuncNode])
    assertEquals("isNaN",p2.right.get.asInstanceOf[FuncNode].fname)
    assertEquals(List(ReqParamNode()),p2.right.get.asInstanceOf[FuncNode].args.params)

    val p3 = VocabParser("parseInt(?,??)")
    assertTrue(p3.left.toString, p3.isRight)
    assertTrue(p3.right.get.isInstanceOf[FuncNode])
    assertEquals("parseInt",p3.right.get.asInstanceOf[FuncNode].fname)
    assertEquals(List(ReqParamNode(),OptParamNode()),p3.right.get.asInstanceOf[FuncNode].args.params)
  }

  @Test def parseMethodCall() : Unit = {
    val p1 = VocabParser("?.sort()")
    assertTrue(p1.left.toString,p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[McallNode])
    assertEquals("sort",p1.right.get.asInstanceOf[McallNode].fname)
    assertEquals(0,p1.right.get.asInstanceOf[McallNode].args.params.length)

    val p2 = VocabParser("?.map(?)")
    assertTrue(p2.left.toString,p2.isRight)
    assertTrue(p2.right.get.isInstanceOf[McallNode])
    assertEquals("map",p2.right.get.asInstanceOf[McallNode].fname)
    assertEquals(List(ReqParamNode()),p2.right.get.asInstanceOf[McallNode].args.params)

    val p3 = VocabParser("?.foo(??)")
    assertTrue(p3.left.toString,p3.isRight)
    assertTrue(p3.right.get.isInstanceOf[McallNode])
    assertEquals("foo",p3.right.get.asInstanceOf[McallNode].fname)
    assertEquals(List(OptParamNode()),p3.right.get.asInstanceOf[McallNode].args.params)
  }

  @Test def parseBinop() : Unit = {
    val p1 = VocabParser("? == ?")
    assertTrue(p1.left.toString,p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[BinopNode])
    assertEquals("==", p1.right.get.asInstanceOf[BinopNode].op)

    val p2 = VocabParser("? + ?")
    assertTrue(p2.left.toString,p2.isRight)
    assertTrue(p2.right.get.isInstanceOf[BinopNode])
    assertEquals("+", p2.right.get.asInstanceOf[BinopNode].op)
  }

  @Test def parseUnop() : Unit = {
    val p1 = VocabParser("-?")
    assertTrue(p1.left.toString,p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[PrefopNode])
    assertEquals("-",p1.right.get.asInstanceOf[PrefopNode].op)

    val p2 = VocabParser("?++")
    assertTrue(p2.left.toString,p2.isRight)
    assertTrue(p2.right.get.isInstanceOf[PostopNode])
    assertEquals("++",p2.right.get.asInstanceOf[PostopNode].op)
  }

  @Test def parseArrayDeref() : Unit = {
    val p1 = VocabParser("?[?]")
    assertTrue(p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[ArrDerefNode])
  }

  @Test def parsePropCall() : Unit = {
    val p1 = VocabParser("?.length")
    assertTrue(p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[PropCallNode])
    assertEquals("length",p1.right.get.asInstanceOf[PropCallNode].propName)
  }

  @Test def parseFuncWithCback() : Unit = {
    val p1 = VocabParser("foo(? => ?)")
    assertTrue(p1.left.toString,p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[FuncNode])
    assertEquals(1,p1.right.get.asInstanceOf[FuncNode].args.params.length)
    assertTrue(p1.right.get.asInstanceOf[FuncNode].args.params.head.isInstanceOf[CallbackParamNode])

    val p2 = VocabParser("foo(?,? => ?)")
    assertTrue(p2.left.toString,p2.isRight)
    assertTrue(p2.right.get.isInstanceOf[FuncNode])
    assertEquals(2,p2.right.get.asInstanceOf[FuncNode].args.params.length)
    assertTrue(p2.right.get.asInstanceOf[FuncNode].args.params.head.isInstanceOf[ReqParamNode])
    assertTrue(p2.right.get.asInstanceOf[FuncNode].args.params.last.isInstanceOf[CallbackParamNode])

    val p3 = VocabParser("foo((?,?) => ?)")
    assertTrue(p3.isRight)
    assertTrue(p3.right.get.isInstanceOf[FuncNode])
    assertTrue(p3.right.get.asInstanceOf[FuncNode].args.params.head.isInstanceOf[CallbackParamNode])
    assertEquals(List(ReqParamNode(),ReqParamNode()),p3.right.get.asInstanceOf[FuncNode].args.params.head.asInstanceOf[CallbackParamNode].args.params)

    val p4 = VocabParser("foo((?,??,??) => ?)")
    assertTrue(p4.isRight)
    assertTrue(p4.right.get.isInstanceOf[FuncNode])
    assertTrue(p4.right.get.asInstanceOf[FuncNode].args.params.head.isInstanceOf[CallbackParamNode])
    assertEquals(List(ReqParamNode(),OptParamNode(),OptParamNode()),p4.right.get.asInstanceOf[FuncNode].args.params.head.asInstanceOf[CallbackParamNode].args.params)
  }

  @Test def parseOptionalCallback() : Unit = {
    val p1 = VocabParser("?.sort((?,?) => ??)")
    assertTrue(p1.isRight)
    assertTrue(p1.right.get.isInstanceOf[McallNode])
    assertTrue(p1.right.get.asInstanceOf[McallNode].args.params.head.isInstanceOf[OptCallbackParamNode])
    assertEquals(List(ReqParamNode(),ReqParamNode()),p1.right.get.asInstanceOf[McallNode].args.params.head.asInstanceOf[OptCallbackParamNode].args.params)
  }
}
