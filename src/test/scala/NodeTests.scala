import org.junit.runner.RunWith
import org.scalatest.junit.JUnitSuite
import org.junit.Assert._
import org.junit.Test
/**
 * Created by hila on 14/11/2017.
 */

class NodeTests extends JUnitSuite {

  @Test def loadLiteral() { // Uses JUnit-style assertions
    val emptyArr = ASTNode("[]")
    assertTrue(emptyArr.isInstanceOf[Literal])
    assertEquals(emptyArr.toCode, "[]");

    val negNumber = ASTNode("-12.5")
    assertTrue(negNumber.isInstanceOf[Literal])
    assertEquals("-12.5", negNumber.toCode)
  }

  @Test def loadFunc(): Unit = {
    val func = ASTNode("require(?)")
    assertEquals("require(?)",func.toCode)
    val func2 = ASTNode("isNan(?)")
    assertEquals("isNan(?)",func2.toCode)
  }

  @Test def loadFQFunction() : Unit = {
    val func = ASTNode("Array.of(?,?)")
    assertEquals("Array.of(?,?)", func.toCode)
    assertEquals(2, func.arity)
  }

  @Test def loadFunc2Params()  : Unit = {
    val func = ASTNode("parseInt(?,?)")
    assertEquals(classOf[ASTNode],func.getClass)
    assertEquals("parseInt(?,?)",func.toCode)
  }

  @Test def loadFuncOptional(): Unit = {
    val func = ASTNode("isNaN(?,??)")
    assertEquals("isNaN(?)",func.toCode) //don't print an unassigned optional
    assertEquals(2,func.arity)
    assertEquals(1,func.optionalArgs)
  }

  @Test def loadCall() : Unit = {
    val func = ASTNode("?.concat(?)")
    assertEquals("?.concat(?)",func.toCode)
  }

  @Test def loadCallWithOptional() : Unit = {
    val func = ASTNode("?.sort(??)")
    assertTrue(func.isInstanceOf[MethodCall])
    assertEquals(2,func.arity)
    assertEquals(1,func.optionalArgs)
    assertEquals("?.sort()",func.toCode)
  }

  @Test def loadAggregators() : Unit = {
    AggregatorMethod.count = 0
    val map = ASTNode("?.map((?,??) => ?)")
    assertTrue(map.isInstanceOf[AggregatorMethod])
    assertEquals("?.map(x1 => ?)",map.toCode)
    val filter = ASTNode("?.filter((?,??) => ?)")
    assertTrue(filter.isInstanceOf[AggregatorMethod])
    assertEquals("?.filter(x3 => ?)",filter.toCode)
    val reduce = ASTNode("?.reduce((?,?) => ?)")
    assertTrue(reduce.isInstanceOf[AggregatorMethod])
    assertEquals("?.reduce((x5,x6) => ?)",reduce.toCode)
  }

  @Test def loadProperty() : Unit = {
    val prop = ASTNode("?.length")
    assertEquals("?.length",prop.toCode)
  }

  @Test def loadDeref() : Unit = {
    val prop = ASTNode("?[?]")
    assertFalse(prop.isInstanceOf[Literal])
    assertEquals("?[?]",prop.toCode)
  }

  @Test def parseVocabList() : Unit = {
    val vocab = ASTNode.parseList(
      """[]
        |require(?)
        |?.concat(?)
        |isNan(?)
        |3"""
      .stripMargin)
    assertEquals(5,vocab.length)
    assertTrue(vocab(0).isInstanceOf[Literal])
    assertEquals("[]",vocab(0).toCode)
    assertEquals("require(?)",vocab(1).toCode)
    assertEquals("?.concat(?)",vocab(2).toCode)
    assertEquals("isNan(?)",vocab(3).toCode)
    assertEquals("3",vocab(4).toCode)
  }

  @Test def callAddChildren() : Unit = {
    val func = ASTNode("parseInt(?,?)")
    func.addChild(0,Literal("'20'"))
    assertEquals("parseInt('20',?)",func.toCode)
    func.addChild(1,Literal("16"))
    assertEquals("parseInt('20',16)",func.toCode)
  }

  @Test def callOptionalAddChildren() : Unit = {
    val func = ASTNode("parseInt(?,??)")
    func.addChild(0,Literal("'20'"))
    assertEquals("parseInt('20')",func.toCode)
    func.addChild(1,Literal("true"))
    assertEquals("parseInt('20',true)",func.toCode)

    val func2 = ASTNode("parse(?,??,??)")
    func2.addChild(2,Literal("x"))
    assertEquals("parse(?,??,x)",func2.toCode)
  }

  @Test def methodAddChildren() : Unit = {
    val func = ASTNode("?.indexOf(?,?)")
    func.addChild(0,Literal("'abc'"))
    assertEquals("'abc'.indexOf(?,?)",func.toCode)
  }

  @Test def methodOptionalAddChildren() : Unit = {
    val func = ASTNode("?.indexOf(??,??)")
    assertEquals("?.indexOf()",func.toCode)
    func.addChild(2,Literal("a"))
    assertEquals("?.indexOf(??,a)",func.toCode)
  }

  @Test def loadBinOperator() : Unit = {
    val op = ASTNode("? + ?")
    assertEquals(classOf[BinOperator],op.getClass)
    assertEquals("? + ?", op.toCode)
  }

  @Test def binOpAddChildren() : Unit = {
    val op = ASTNode("?-?")
    op.addChild(1,Literal("2"))
    assertEquals("? - 2", op.toCode)
    op.addChild(0,ASTNode("?+?"))
    assertEquals("(? + ?) - 2", op.toCode)
  }

  @Test def loadUnOpPrefix() : Unit = {
    val op = ASTNode("!?")
    assertEquals(classOf[UnPrefOperator],op.getClass)
    assertEquals("!(?)",op.toCode)
  }

  @Test def unOpPrefixAddChildren() : Unit = {
    val op = ASTNode("++?")
    op.addChild(0,Literal("x"))
    assertEquals("++(x)",op.toCode)
  }

  @Test def loadUnOpPostfix() : Unit = {
    val op = ASTNode("?++")
    assertEquals(classOf[UnPostOperator],op.getClass)
    assertEquals("(?)++",op.toCode)
  }

  @Test def unOpPostfixAddChildren() : Unit = {
    val op = ASTNode("?--")
    op.addChild(0,Literal("2"))
    assertEquals("(2)--",op.toCode)
  }

  @Test def arrayAddChildren() : Unit = {
    val arr = ASTNode("?[ ?]")
    arr.addChild(0,Literal("x"))
    arr.addChild(1,BinOperator("+"))
    assertEquals("x[? + ?]",arr.toCode)

    val arr2 = ASTNode("?[ ?]")
    assertEquals(2,arr2.arity)
    arr2.addChild(0,BinOperator("+"))
    arr2.addChild(1,Literal("2"))
    assertEquals("(? + ?)[2]",arr2.toCode)
  }

  @Test def loadPropCall() : Unit = {
    val prop = ASTNode("?.prop")
    assertTrue(prop.isInstanceOf[PropCall])
    assertEquals("?.prop", prop.toCode)

  }

  @Test def propCallAddChildren : Unit = {
    val prop = new PropCall("length")
    prop.addChild(0,Literal("x"))
    assertEquals("x.length", prop.toCode)

    val prop2 = new PropCall("a")
    prop2.addChild(0,UnPrefOperator("!"))
    assertEquals("(!(?)).a",prop2.toCode)
  }

  @Test def loadTrenary() : Unit = {
    val trn = ASTNode("?? ? : ?")
    assertTrue(trn.isInstanceOf[TernaryOperator])
  }

  @Test def trnaryAddChildren : Unit = {
    val trn = ASTNode("???:?")
    trn.addChild(0,BinOperator("=="))
    trn.addChild(1,BinOperator("*"))
    trn.addChild(2,BinOperator("+"))
    assertEquals("(? == ?) ? (? * ?) : (? + ?)",trn.toCode)
  }

  @Test def loadConstructor : Unit = {
    val ctor = ASTNode("new Set")
    assertTrue(ctor.isInstanceOf[Constructor])
    assertEquals("Set", ctor.func)
    assertEquals(0,ctor.arity)
    val ctor0 = ASTNode("new Object()")
    assertTrue(ctor0.isInstanceOf[Constructor])
    assertEquals("Object", ctor0.func)
    assertEquals(0,ctor0.arity)
    val ctor1 = ASTNode("new String(?)")
    assertTrue(ctor1.isInstanceOf[Constructor])
    assertEquals("String", ctor1.func)
    assertEquals(1,ctor1.arity)
    val ctor3 = ASTNode("new Set(?,?,?)")
    assertTrue(ctor3.isInstanceOf[Constructor])
    assertEquals("Set", ctor3.func)
    assertEquals(3,ctor3.arity)
  }

  @Test def ctorAddChildren : Unit = {
    val ctor : Constructor = Constructor("Set",0)
    assertEquals("new Set",ctor.toCode)

    val ctor2 : Constructor = Constructor("Set",1)
    ctor2.addChild(0,Literal("'a'"))
    assertEquals("new Set('a')",ctor2.toCode)
  }

  @Test def literalIsFull : Unit = {
    val lit = Literal("x")
    assertTrue(lit.isFull)
  }

  @Test def binopIsFull : Unit = {
    val binOp = BinOperator("*")
    assertFalse(binOp.isFull)
    binOp.addChild(0,Literal("x"))
    assertFalse(binOp.isFull)
    binOp.addChild(1,Literal("y"))
    assertTrue(binOp.isFull)
  }

  @Test def methodCallIsFull : Unit = {
    val mcall = new MethodCall("foo",1)
    assertFalse(mcall.isFull)
    mcall.addChild(1,Literal("x"))
    assertFalse(mcall.isFull)
    mcall.addChild(0,Literal("x"))
    assertTrue(mcall.isFull)
  }

  @Test def ternaryIsFull : Unit = {
    val t = TernaryOperator()
    assertFalse(t.isFull)
    t.addChild(2,Literal("x"))
    assertFalse(t.isFull)
    t.addChild(1,Literal("y"))
    assertFalse(t.isFull)
    t.addChild(0,Literal("w"))
    assertTrue(t.isFull)
  }

  @Test def unaryOpsIsFull : Unit = {
    val u1 = UnPrefOperator("!")
    assertFalse(u1.isFull)
    u1.addChild(0,Literal("x"))
    assertTrue(u1.isFull)

    val node = new ASTNode("bar",2)
    assertFalse(node.isFull)
    node.addChild(0,u1)
    assertFalse(node.isFull)

    val u2 = UnPostOperator("++")
    assertFalse(u2.isFull)

    node.addChild(1,u2)
    assertFalse(node.isFull)

    u2.addChild(0,Literal("y"))
    assertTrue(u2.isFull)
    assertTrue(node.isFull)
  }

  @Test def derefIsFull() : Unit = {
    val arr = ArrayDeref()
    assertFalse(arr.isFull)
    arr.addChild(0,Literal("x"))
    assertFalse(arr.isFull)
    arr.addChild(1,Literal("y"))
    assertTrue(arr.isFull)
  }

  @Test def propIsFull() : Unit = {
    val prop = PropCall("length")
    assertFalse(prop.isFull)
    prop.addChild(0,PropCall("arr"))
    assertFalse(prop.isFull)
    prop.lhs.get.addChild(0,Literal("x"))
    assertTrue(prop.isFull)
  }

  @Test def aggIsFull() : Unit = {
    val agg = AggregatorMethod("map",1,1)
    assertFalse(agg.isFull)
    agg.addChild(0,Literal("x"))
    assertFalse(agg.isFull)
    agg.addChild(1,Literal("input"))
    assertTrue(agg.isFull)
  }

  @Test def ctorIsFull() : Unit = {
    val ctor = Constructor("B",3)
    assertFalse(ctor.isFull)
    ctor.addChild(0,Literal("x"))
    assertFalse(ctor.isFull)
    ctor.addChild(1,Literal("yu"))
    assertFalse(ctor.isFull)
    ctor.addChild(2,Literal("y"))
    assertTrue(ctor.isFull)
  }

  @Test def nodeEquals() : Unit = {
    val n1 = ASTNode("?.foo()")
    val n2 = n1.shallowClone
    n2.addChild(0,Literal("x"))
    assertFalse(n1 == n2)

    val s1 = Set(n1)
    val s2 = Set(n2)
    assertEquals(2,(s1 ++ s2).size)
  }

  @Test def getContextVars() : Unit = {
    val n1 = AggregatorMethod("map",2,List("e","i"))
    assertTrue(n1.accumulateContextVars(List(0)).isEmpty)

    val res = n1.accumulateContextVars(List(1))
    assertEquals(2,res.size)
    assertEquals("e",res.head)
    assertEquals("i",res.last)

    val n2 = AggregatorMethod("filter",1,1)
    n1.addChild(1,n2)
    val res2 = n1.accumulateContextVars(List(1,0))
    assertEquals(List("e","i"),res2)

    val res3 = n1.accumulateContextVars(List(1,1))
    assertEquals(List("e","i",n2.innerVar(0)),res3)
  }

  @Test def heightNoLambdas() : Unit = {
    val p1 = Literal("[]")
    assertEquals(1, p1.heightNoLambdas)

    val p2 = PropCall("length")
    p2.addChild(0,p1)
    assertEquals(2,p2.heightNoLambdas)

    val p3 = AggregatorMethod("map", 2, 1)
    assertEquals(1, p3.heightNoLambdas)

    p3.addChild(1, p2)
    p3.addChild(0, Literal("x"))
    assertEquals(2, p3.heightNoLambdas)

    val p4 = AggregatorMethod("reduce", 2, 2)
    p4.addChild(1,p2)
    p4.addChild(2,Literal("y"))
    assertEquals(2,p4.heightNoLambdas)
  }

  @Test def getAt() : Unit = {
    val p1 = Literal("[]")
    assertEquals("[]", p1.getAt(Nil).get.toCode)
    assertTrue(p1.getAt(List(0)).isEmpty)

    val p2 = new MethodCall(toString,0,0)
    assertTrue(p2.getAt(List(0)).isEmpty)
    assertTrue(p2.getAt(List(0,1,2)).isEmpty)
  }

  @Test def loadObjectInitializer() : Unit = {
    val p1 = ASTNode("{?:?}")
    assertTrue(p1.isInstanceOf[ObjectInitializer])
    assertEquals(2,p1.arity)
    assertEquals("{?:?}",p1.toCode)

    val p2 = ASTNode("{? : ?, ? : ?, ? : ?, ? : ?}")
    assertTrue(p2.isInstanceOf[ObjectInitializer])
    assertEquals(8,p2.arity)
    assertEquals("{?:?,?:?,?:?,?:?}", p2.toCode)
  }

  @Test def objectInitializerAddChildren() : Unit = {
    val p1 = new ObjectInitializer(2)
    assertEquals("{?:?}", p1.toCode)
    p1.addChild(0,Literal("x"))
    assertEquals("{x:?}",p1.toCode)
    p1.addChild(1,BinOperator("+"))
    assertEquals("{x:? + ?}",p1.toCode)

    val p2 = new ObjectInitializer(6)
    assertEquals("{?:?,?:?,?:?}",p2.toCode)
    p2.addChild(2,Literal("key"))
    p2.addChild(3,Literal("'boo'"))
    assertEquals("{?:?,key:'boo',?:?}",p2.toCode)
  }

  @Test def subtreeIsContained() : Unit = {
    val program = JSParser.deconstruct("JSON.stringify(list1.map(x => Object.assign(x, {greeting : (((\"Hi \" + (x.firstName)) + \", what do you like the most about \") + (x.language)) + \"?\"}))[0])")

    assertTrue(program.contains(Literal("x")))
    assertFalse(program.contains(Literal("JSON")))
    assertFalse(program.contains(Literal("y")))
    assertFalse(program.contains(Literal("[0]")))
    assertTrue(program.contains(Literal("0")))

    val subtree = BinOperator("+")
    subtree.addChild(0, Literal("\"Hi \""))
    subtree.addChild(1, PropCall("firstName"))
    subtree.children(1).get.addChild(0,Literal("x"))
    assertTrue(program.contains(subtree))

    val subtree2 = BinOperator("+")
    subtree2.addChild(0,Literal("\"Hi \""))
    subtree2.addChild(1,Literal("x"))
    assertFalse(program.contains(subtree2))
  }

  @Test def loadMacro() : Unit = {
    val p = ASTNode("MACRO foo => Object.assign({? : ?},?)")
    assertTrue(p.isInstanceOf[ASTMacro])
    assertEquals(3,p.arity)
    assertEquals("foo", p.func)
    assertEquals("Object.assign({? : ?},?)",p.toCode)
  }

  @Test def macroAddChildren() : Unit = {
    val p = ASTNode("MACRO bar=>?+1")
    assertTrue(p.isInstanceOf[ASTMacro])
    assertEquals(1,p.arity)
    assertEquals("bar",p.func)
    assertEquals("?+1", p.toCode)

    p.addChild(0,Literal("x"))
    assertEquals("x+1", p.toCode)

    p.removeChild(0)
    val sub = BinOperator("+")
    sub.addChild(0,Literal("x"))
    sub.addChild(1,Literal("2"))
    p.addChild(0,sub)

    assertEquals("(x + 2)+1",p.toCode)
  }
}
