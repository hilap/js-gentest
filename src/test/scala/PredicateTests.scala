import org.scalatest.junit.JUnitSuite
import org.junit.Assert._
import org.junit.Test


class PredicateTests extends JUnitSuite {
  @Test def retainPredicateOnRoot(): Unit = {
    val m = Literal("x")
    val p = RetainPredicate(m.deepClone)
    assertTrue(p.isInstanceOf[ProgramPredicate])
    assertTrue(p(m))

    val m2 = JSParser.deconstruct("x.foo(y)")
    val p2 = RetainPredicate(m2.deepClone)
    assertTrue(p2(m2))

    assertTrue(p(m2))
    assertFalse(p2(m))
  }

  @Test def retainPredicateDeep() : Unit = {
    val m = JSParser.deconstruct("input.sort().reduce((x,y) => x.indexOf(y) == (-1) ? x.concat([y]) : x,[]).join('')")
    val p = RetainPredicate(JSParser.deconstruct("x.indexOf(y)"))
    assertTrue(p(m))

    val p2 = RetainPredicate(JSParser.deconstruct("x.concat(y)"))
    assertFalse(p2(m))
  }

  @Test def excludePredicateOnRoot() : Unit = {
    val m = Literal("x")
    val p = ExcludePredicate(m.deepClone)
    assertTrue(p.isInstanceOf[ProgramPredicate])
    assertFalse(p(m))

    val p2 = ExcludePredicate(Literal("y"))
    assertTrue(p2(m))
  }

  @Test def excludePredicateDeep() : Unit = {
    val m = JSParser.deconstruct("input.sort().reduce((x,y) => x.indexOf(y) == (-1) ? x.concat([y]) : x,[]).join('')")
    val p = ExcludePredicate(JSParser.deconstruct("x.indexOf(y)"))
    assertFalse(p(m))

    val p2 = ExcludePredicate(JSParser.deconstruct("x.concat(y)"))
    assertTrue(p2(m))
  }

//  @Test def inputThrowsExceptionRoot(): Unit = {
//    val m = JSParser.deconstruct("2/input")
//    val p1 = InputThrowsException("0")
//    assertTrue(p1(m))
//
//    val p2 = InputThrowsException("1")
//    assertTrue(p2(m))
//  }

  @Test def subexprDataPredicateRoot(): Unit = {
    val m = JSParser.deconstruct("input + 1")
    val p1 = SubexpressionEquals(VarInputs(List("1")),List("input"),"2")
    assertTrue(p1(m))

    val m2 = JSParser.deconstruct("input.toString()")
    val p2 = NoSubexpressionOfType(VarInputs(List("[]")),List("input"),DataType.string)
    assertFalse(p2(m2))
  }

  @Test def subexprDataPredicateDeep(): Unit = {
    val m1 = JSParser.deconstruct("input + (1 + 2)")
    val p1 = NoSubexpressionEquals(VarInputs(List("1")),List("input"), "3")
    assertFalse(p1(m1))

    val m2 = JSParser.deconstruct("input.split('').join(',')")
    val p2 = SubexpressionOfType(VarInputs(List("'123'")), List("input"), DataType.array)
    assertTrue(p2(m2))
  }
}
