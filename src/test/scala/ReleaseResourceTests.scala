import com.eclipsesource.v8.V8Value
import org.junit.{Before, BeforeClass, Test}
import org.scalatest.junit.JUnitSuite
import org.junit.Assert._

class ReleaseResourceTests extends JUnitSuite{

  @Before def releaseAll() : Unit = V8Runtime.release()
  @Test def wrapperReleaseObjects() : Unit = {
    val aWrapped : FromJSWrapper = NonV8AnyRef(new java.lang.Integer(2))
    val b = V8Runtime.runtime.executeScript("2")
    val bWrapped : FromJSWrapper = NonV8AnyRef(b)
    val c = V8Runtime.runtime.executeScript("[2]")
    //val cWrapped : FromJSWrapper = JSArrayWrapper(c)
    //a.release()
    if (b.isInstanceOf[V8Value]) b.asInstanceOf[V8Value].release()
    if (c.isInstanceOf[V8Value]) c.asInstanceOf[V8Value].release()
    V8Runtime.release()
  }

//  @Test def retainPolicyReleaseObjects() : Unit = {
//    val retainPolicy : ValuesRetainPolicy = new ValuesRetainPolicy(null)
//    val in : List[AnyRef] = List(V8Runtime.runtime.executeScript("1"),
//      V8Runtime.runtime.executeScript("'1'"),
//      V8Runtime.runtime.executeScript("[1]"))
//    retainPolicy.values.add(in.map(x => FromJSWrapper(x)))
//
//    val in2 : List[AnyRef] = List(V8Runtime.runtime.executeScript("[2]"),
//      V8Runtime.runtime.executeScript("'2'"),
//      V8Runtime.runtime.executeScript("2"))
//    retainPolicy.values.add(in2.map(x => FromJSWrapper(x)))
//
//    in.foreach(o => if (o.isInstanceOf[V8Value]) o.asInstanceOf[V8Value].release())
//    in2.foreach(o => if (o.isInstanceOf[V8Value]) o.asInstanceOf[V8Value].release())
//    retainPolicy.releaseValues()
//
//    V8Runtime.release()
//  }

  @Test def executorExceptionRuntimeClean() : Unit = {
    val executor = new ExecutorWithVal(List(VarInputs(List("[1,2,3]")),VarInputs(List("[a,b,c]"))))
    val retainPolicy : ValuesRetainPolicy = new ValuesRetainPolicy(executor,Nil)
    //val reses = executor.execute("input.map(x => x + 1)")
    val code = AggregatorMethod("map",1,1)
    code.addChild(0,Literal("input"))
    val innerMap = BinOperator("+")
    innerMap.addChild(0,Literal(code.innerVar.head))
    innerMap.addChild(1,Literal("1"))
    code.addChild(1,innerMap)
    retainPolicy.shouldAdd(code)
    V8Runtime.release()
  }

  @Test def shouldAddReleaseUnused() : Unit = {
    val executor = new ExecutorNoVal
    val retainPolicy : ValuesRetainPolicy = new ValuesRetainPolicy(executor,Nil)
    retainPolicy.shouldAdd(Literal("[1,2,3]"))
    retainPolicy.shouldAdd(Literal("[1,2,3]"))
    retainPolicy.releaseValues()
    V8Runtime.release()
  }

  @Test def resetInputsPolicyResetObjects() : Unit = {
    val executor = new ExecutorNoVal
    val resetInputsRetainPolicy : ResetInputsRetainPolicy = new ResetInputsRetainPolicy(List("a","b"),List(),List(),Nil)
    resetInputsRetainPolicy.reset(List(VarInputs(List("[1]","[2]"))))
    val in = executor.execute("[1,2,3]")
    resetInputsRetainPolicy.innerPolicy.values.add(in.get)
    resetInputsRetainPolicy.reset(List(VarInputs(List("1","2"))))
    in.release()
    V8Runtime.release()
  }

  @Test def aggregatorParamsReleaseObjects() : Unit = {
    val aggParams : AggregatorParamsIter = new AggregatorParamsIter(
      "sort",
        List(Enumerator.SeenProgram(Literal("[1,2,3]"),Set()),Enumerator.SeenProgram(Literal("1"),Set())),
        List(Literal("a"),Literal("b"),Literal("1"), BinOperator("+"), BinOperator("-")),
        new ResetInputsRetainPolicy(List("a","b"),List(),List(),Nil),
        new ExecutorWithVal(List(VarInputs(List("1","1")),VarInputs(List("2","1")),VarInputs(List("3","1"))),List("a","b")),
      2,
      0,
      0,
      Nil,
      Nil,
      Map.empty
    )
    while(aggParams.hasNext) {
      aggParams.next()
    }
    aggParams.releaseValues()
    V8Runtime.release()
  }

  @Test def enumeratorReleaseObjects() : Unit = {
    val vocab = ASTNode.parseList(
      """input
        |1
        |[]
        |? + ?
        |? - ?
        |?.concat(?)
        |?.map(?)
        |?.reduce(?)
        |?.filter(?)""".stripMargin)
    val enumerator = Enumerator(
        vocab,
        new ValuesRetainPolicy(new ExecutorWithVal(List(VarInputs(List("[1,2,3,4]")))),Nil))

    for(i <- 0 to 1000) {
      enumerator.hasNext
      enumerator.next().toCode
    }

    enumerator.releaseValues()
    V8Runtime.release()
  }
}
