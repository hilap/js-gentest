import org.junit.Test
import org.junit.Assert._
import org.scalatest.junit.JUnitSuite

class RemoveChildrenTests extends JUnitSuite{

  @Test(expected = classOf[ArrayIndexOutOfBoundsException]) def literalRemoveChild() : Unit = {
    val lit = Literal("x")
    lit.removeChild(0)
  }

  @Test def binOpRemoveChild() : Unit = {
    val node = JSParser.deconstruct("1 + 2")
    assertTrue(node.isInstanceOf[BinOperator])
    node.removeChild(0)
    assertEquals("? + 2", node.toCode)
    node.removeChild(1)
    assertEquals("? + ?", node.toCode)
  }

  @Test def methodCallRemoveChild() : Unit = {
    val node = JSParser.deconstruct("x.stringify(1,2,3)")
    assertTrue(node.isInstanceOf[MethodCall])
    node.removeChild(0)
    assertEquals(None,node.asInstanceOf[MethodCall].lhs)
    assertEquals("?.stringify(1,2,3)",node.toCode)
    node.removeChild(2)
    assertEquals(None,node.children(2))
    assertEquals("?.stringify(1,?,3)",node.toCode)
  }

  @Test def ternaryOperatorRemoveChild() : Unit = {
    val node = JSParser.deconstruct("x ? y : z")
    assertTrue(node.isInstanceOf[TernaryOperator])
    node.removeChild(0)
    assertEquals("? ? y : z",node.toCode)
    node.removeChild(1)
    assertEquals("? ? ? : z",node.toCode)
    node.removeChild(2)
    assertEquals("? ? ? : ?",node.toCode)
  }

  @Test def arrayDerefRemoveChild() : Unit = {
    val node = JSParser.deconstruct("abc[8]")
    assertTrue(node.isInstanceOf[ArrayDeref])
    node.removeChild(0)
    assertEquals("?[8]",node.toCode)
    node.removeChild(1)
    assertEquals("?[?]",node.toCode)
  }

  @Test def propCallRemoveChild() : Unit = {
    val node = JSParser.deconstruct("foo().blah")
    assertTrue(node.isInstanceOf[PropCall])
    node.removeChild(0)
    assertEquals("?.blah",node.toCode)
  }

  @Test def constructorRemoveChild() : Unit = {
    val node = JSParser.deconstruct("new Object(1,2,3)")
    assertTrue(node.isInstanceOf[Constructor])
    node.removeChild(1)
    assertEquals("new Object(1,?,3)",node.toCode)
  }

}
