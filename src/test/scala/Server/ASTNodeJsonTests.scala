import ASTNodeJson._
import spray.json._
import org.junit.Test
import org.junit.Assert._
import org.scalatest.junit.JUnitSuite

class ASTNodeJsonTests  extends JUnitSuite{

  @Test def JsonifyLiteral() : Unit = {
    val prog = Literal("x")
    val wrapped : TreeNode = ASTNodeJson.construct(prog)
    assertEquals("x",wrapped.subProg)
    assertEquals(0, wrapped.ord)
    assertEquals(0, wrapped.children.length)
  }

  @Test def JsonifyCtor() : Unit = {
    val prog = Constructor("Set",1)
    prog.addChild(0,Literal("2"))
    val wrapped = ASTNodeJson.construct(prog)
    assertEquals("new Set(2)",wrapped.subProg)
    assertEquals(3, wrapped.children.length)
    assertEquals("new Set(", wrapped.children.head.subProg)
    assertEquals(-1, wrapped.children.head.ord)
    assertTrue(wrapped.children.head.children.isEmpty)
    assertEquals("2", wrapped.children(1).subProg)
    assertEquals(0, wrapped.children(1).ord)
    assertTrue(wrapped.children(1).children.isEmpty)
    assertEquals(")", wrapped.children(2).subProg)
    assertEquals(-1, wrapped.children(2).ord)
    assertTrue(wrapped.children(2).children.isEmpty)
  }

  @Test def JsonifyFunc() : Unit = {
    val prog = new ASTNode("isNaN",2)
    prog.addChild(0,Literal("1"))
    prog.addChild(1,Constructor("Map",0))
    val wrapped = ASTNodeJson.construct(prog)
    assertEquals("isNaN(1,new Map)",wrapped.subProg)
    assertEquals(0, wrapped.ord)
    assertEquals(5,wrapped.children.length)

    assertEquals("isNaN(", wrapped.children(0).subProg)
    assertEquals(-1, wrapped.children(0).ord)
    assertTrue(wrapped.children(0).children.isEmpty)

    assertEquals("1", wrapped.children(1).subProg)
    assertEquals(0, wrapped.children(1).ord)
    assertTrue(wrapped.children(1).children.isEmpty)

    assertEquals(",", wrapped.children(2).subProg)
    assertEquals(-1, wrapped.children(2).ord)
    assertTrue(wrapped.children(2).children.isEmpty)

    assertEquals("new Map", wrapped.children(3).subProg)
    assertEquals(1, wrapped.children(3).ord)
    assertTrue(wrapped.children(3).children.isEmpty)

    assertEquals(")", wrapped.children(4).subProg)
    assertEquals(-1, wrapped.children(4).ord)
    assertTrue(wrapped.children(4).children.isEmpty)
  }

  @Test def JsonifyFuncNoParams() : Unit = {
    val  prog = new ASTNode("foo",0)
    val wrapped = ASTNodeJson.construct(prog)
    assertEquals("foo()",wrapped.subProg)
    assertEquals(0, wrapped.children.length)
    assertEquals(0, wrapped.ord)
  }

  @Test def JsonifyBinop() : Unit = {
    val prog = JSParser.deconstruct("foo(x) + 123 * 2")
    val wrapped = ASTNodeJson.construct(prog)
    assertEquals("foo(x) + (123 * 2)", wrapped.subProg)
    assertEquals(0, wrapped.ord)
    assertEquals(3, wrapped.children.length)

    assertEquals("foo(x)", wrapped.children(0).subProg)
    assertEquals(0, wrapped.children(0).ord)
    assertEquals(3, wrapped.children(0).children.length)

    assertEquals(" + ", wrapped.children(1).subProg)
    assertEquals(-1, wrapped.children(1).ord)
    assertTrue(wrapped.children(1).children.isEmpty)

    assertEquals("(123 * 2)", wrapped.children(2).subProg)
    assertEquals(1, wrapped.children(2).ord)
    assertEquals(5, wrapped.children(2).children.length)

    assertEquals("(", wrapped.children(2).children(0).subProg)
    assertEquals(-1, wrapped.children(2).children(0).ord)
    assertTrue(wrapped.children(2).children(0).children.isEmpty)

    assertEquals("123", wrapped.children(2).children(1).subProg)
    assertEquals(0, wrapped.children(2).children(1).ord)
    assertTrue(wrapped.children(2).children(1).children.isEmpty)

    assertEquals(" * ", wrapped.children(2).children(2).subProg)
    assertEquals(-1, wrapped.children(2).children(2).ord)
    assertTrue(wrapped.children(2).children(2).children.isEmpty)

    assertEquals("2", wrapped.children(2).children(3).subProg)
    assertEquals(1, wrapped.children(2).children(3).ord)
    assertTrue(wrapped.children(2).children(3).children.isEmpty)

    assertEquals(")", wrapped.children(2).children(4).subProg)
    assertEquals(-1, wrapped.children(2).children(4).ord)
    assertTrue(wrapped.children(2).children(4).children.isEmpty)
  }

  @Test def JsonifyTernary() : Unit = {
    val prog = JSParser.deconstruct("foo(x) ? 1 + 2 : bar(3/4)")
    val wrapped = ASTNodeJson.construct(prog)
    assertEquals("foo(x) ? 1 + 2 : bar(3 / 4)", wrapped.subProg)
    assertEquals(0, wrapped.ord)
    assertEquals(5, wrapped.children.length)

    assertEquals("foo(x)", wrapped.children(0).subProg)
    assertEquals(0, wrapped.children(0).ord)
    assertEquals(3, wrapped.children(0).children.length)

    assertEquals(" ? ", wrapped.children(1).subProg)
    assertEquals(-1, wrapped.children(1).ord)
    assertTrue(wrapped.children(1).children.isEmpty)

    assertEquals("1 + 2", wrapped.children(2).subProg)
    assertEquals(1, wrapped.children(2).ord)
    assertEquals(3, wrapped.children(2).children.length)

    assertEquals(" : ", wrapped.children(3).subProg)
    assertEquals(-1, wrapped.children(3).ord)
    assertTrue(wrapped.children(3).children.isEmpty)

    assertEquals("bar(3 / 4)", wrapped.children(4).subProg)
    assertEquals(2, wrapped.children(4).ord)
    assertEquals(3, wrapped.children(4).children.length)
  }

  @Test def JsonifyPrefOp() : Unit = {
    val prog = JSParser.deconstruct("++x")
    val w1 = ASTNodeJson.construct(prog)
    assertEquals("++x", w1.subProg)
    assertEquals(0, w1.ord)
    assertEquals(2,w1.children.length)

    assertEquals("++", w1.children(0).subProg)
    assertEquals(-1, w1.children(0).ord)
    assertTrue(w1.children(0).children.isEmpty)

    assertEquals("x", w1.children(1).subProg)
    assertEquals(0, w1.children(1).ord)
    assertTrue(w1.children(1).children.isEmpty)

    val prog2 = JSParser.deconstruct("--(x+y)")
    val w2 = ASTNodeJson.construct(prog2)
    assertEquals("--(x + y)", w2.subProg)
    assertEquals(0, w2.ord)
    assertEquals(2,w2.children.length)

    assertEquals("--", w2.children(0).subProg)
    assertEquals(-1, w2.children(0).ord)
    assertTrue(w2.children(0).children.isEmpty)

    assertEquals("(x + y)", w2.children(1).subProg)
    assertEquals(0, w2.children(1).ord)
    assertEquals(5, w2.children(1).children.length)
  }

  @Test def JsonifyPostOp() : Unit = {
    val prog = JSParser.deconstruct("(x)++")
    val w1 = ASTNodeJson.construct(prog)
    assertEquals("x++", w1.subProg)
    assertEquals(0, w1.ord)
    assertEquals(2,w1.children.length)

    assertEquals("x", w1.children(0).subProg)
    assertEquals(0, w1.children(0).ord)
    assertTrue(w1.children(0).children.isEmpty)

    assertEquals("++", w1.children(1).subProg)
    assertEquals(-1, w1.children(1).ord)
    assertTrue(w1.children(1).children.isEmpty)

    val prog2 = JSParser.deconstruct("(x+y)--")
    val w2 = ASTNodeJson.construct(prog2)
    assertEquals("(x + y)--", w2.subProg)
    assertEquals(0, w2.ord)
    assertEquals(2,w2.children.length)

    assertEquals("(x + y)", w2.children(0).subProg)
    assertEquals(0, w2.children(0).ord)
    assertEquals(5, w2.children(0).children.length)

    assertEquals("--", w2.children(1).subProg)
    assertEquals(-1, w2.children(1).ord)
    assertTrue(w2.children(1).children.isEmpty)
  }

  @Test def JsonifyArrayDeref() : Unit = {
    val prog = JSParser.deconstruct("x[y]")
    val w1 = ASTNodeJson.construct(prog)
    assertEquals("x[y]", w1.subProg)
    assertEquals(0, w1.ord)
    assertEquals(4,w1.children.length)

    assertEquals("x", w1.children(0).subProg)
    assertEquals(0, w1.children(0).ord)
    assertTrue(w1.children(0).children.isEmpty)

    assertEquals("[", w1.children(1).subProg)
    assertEquals(-1, w1.children(1).ord)
    assertTrue(w1.children(1).children.isEmpty)

    assertEquals("y", w1.children(2).subProg)
    assertEquals(1, w1.children(2).ord)
    assertTrue(w1.children(2).children.isEmpty)

    assertEquals("]", w1.children(3).subProg)
    assertEquals(-1, w1.children(3).ord)
    assertTrue(w1.children(3).children.isEmpty)

    val prog2 = ArrayDeref()
    prog2.addChild(0,JSParser.deconstruct("!x"))
    prog2.addChild(1,Literal("0"))
    val w2 = ASTNodeJson.construct(prog2)
    assertEquals("(!x)[0]", w2.subProg)
    assertEquals(0, w2.ord)
    assertEquals(4,w2.children.length)

    assertEquals("(!x)", w2.children(0).subProg)
    assertEquals(0, w2.children(0).ord)
    assertEquals(4, w2.children(0).children.length)

    assertEquals("[", w2.children(1).subProg)
    assertEquals(-1, w2.children(1).ord)
    assertTrue(w2.children(1).children.isEmpty)

    assertEquals("0", w2.children(2).subProg)
    assertEquals(1, w2.children(2).ord)
    assertTrue(w2.children(2).children.isEmpty)

    assertEquals("]", w2.children(3).subProg)
    assertEquals(-1, w2.children(3).ord)
    assertTrue(w2.children(3).children.isEmpty)
  }

  @Test def JsonifyPropCall() : Unit = {
    val prog = JSParser.deconstruct("foo(x,y).bar")
    val w = ASTNodeJson.construct(prog)
    assertEquals("foo(x,y).bar", w.subProg)
    assertEquals(0, w.ord)
    assertEquals(2,w.children.length)

    assertEquals("foo(x,y)", w.children(0).subProg)
    assertEquals(0, w.children(0).ord)
    assertEquals(5, w.children(0).children.length)

    assertEquals(".bar", w.children(1).subProg)
    assertEquals(-1, w.children(1).ord)
    assertTrue(w.children(1).children.isEmpty)
  }

  @Test def JsonifyMethodCall() : Unit = {
    val prog = JSParser.deconstruct("input.foo()")
    val w = ASTNodeJson.construct(prog)
    assertEquals("input.foo()", w.subProg)
    assertEquals(0, w.ord)
    assertEquals(3, w.children.length)

    assertEquals("input", w.children(0).subProg)
    assertEquals(0, w.children(0).ord)
    assertEquals(".foo(", w.children(1).subProg)
    assertEquals(-1, w.children(1).ord)
    assertEquals(")", w.children(2).subProg)
    assertEquals(-1, w.children(1).ord)
  }

  @Test def JsonifyMethodCallParams() : Unit = {
    val prog = JSParser.deconstruct("(x+y).of(1,y,z+0)")
    val w = ASTNodeJson.construct(prog)
    assertEquals("(x + y).of(1,y,z + 0)", w.subProg)
    assertEquals(0, w.ord)
    assertEquals(8, w.children.length)

    assertEquals("(x + y)", w.children(0).subProg)
    assertEquals(0, w.children(0).ord)

    assertEquals(".of(", w.children(1).subProg)
    assertEquals(-1, w.children(1).ord)

    assertEquals("1", w.children(2).subProg)
    assertEquals(1, w.children(2).ord)

    assertEquals(",", w.children(3).subProg)
    assertEquals(-1,w.children(3).ord)

    assertEquals("y", w.children(4).subProg)
    assertEquals(2, w.children(4).ord)

    assertEquals(",", w.children(5).subProg)
    assertEquals(-1, w.children(5).ord)

    assertEquals("z + 0", w.children(6).subProg)
    assertEquals(3, w.children(6).ord)

    assertEquals(")", w.children(7).subProg)
    assertEquals(-1, w.children(7).ord)
  }

  @Test def JsonifyAggMethod1Param() : Unit = {
    val prog1 = JSParser.deconstruct("input.map(x => x + 1)")
    val w1 = ASTNodeJson.construct(prog1)
    assertEquals("input.map(x => x + 1)", w1.subProg)
    assertEquals(0, w1.ord)
    assertEquals(5, w1.children.length)

    assertEquals("input", w1.children(0).subProg)
    assertEquals(0, w1.children(0).ord)

    assertEquals(".map(", w1.children(1).subProg)
    assertEquals(-1, w1.children(1).ord)

    assertEquals("x => ", w1.children(2).subProg)
    assertEquals(-2, w1.children(2).ord)

    assertEquals("x + 1", w1.children(3).subProg)
    assertEquals(1, w1.children(3).ord)

    assertEquals(")", w1.children(4).subProg)
    assertEquals(-1, w1.children(4).ord)

    val prog2 = JSParser.deconstruct("input.map(x => x + 2,[])")
    val w2 = ASTNodeJson.construct(prog2)
    assertEquals("input.map(x => x + 2,[])", w2.subProg)
    assertEquals(0, w2.ord)
    assertEquals(7, w2.children.length)

    assertEquals("input", w2.children(0).subProg)
    assertEquals(0, w2.children(0).ord)

    assertEquals(".map(", w2.children(1).subProg)
    assertEquals(-1, w2.children(1).ord)

    assertEquals("x => ", w2.children(2).subProg)
    assertEquals(-2, w2.children(2).ord)

    assertEquals("x + 2", w2.children(3).subProg)
    assertEquals(1, w2.children(3).ord)

    assertEquals(",", w2.children(4).subProg)
    assertEquals(-1, w2.children(4).ord)

    assertEquals("[]", w2.children(5).subProg)
    assertEquals(2, w2.children(5).ord)

    assertEquals(")", w2.children(6).subProg)
    assertEquals(-1, w2.children(6).ord)
  }

  @Test def JsonifyMethod2Param() : Unit = {
    val prog = JSParser.deconstruct("(x+y).sort((a,b) => a < b)")
    val w = ASTNodeJson.construct(prog)

    assertEquals("(x + y).sort((a,b) => a < b)",w.subProg)
    assertEquals(0,w.ord)
    assertEquals(5, w.children.length)

    assertEquals("(x + y)", w.children(0).subProg)
    assertEquals(0, w.children(0).ord)

    assertEquals(".sort(", w.children(1).subProg)
    assertEquals(-1, w.children(1).ord)

    assertEquals("(a,b) => ", w.children(2).subProg)
    assertEquals(-2, w.children(2).ord)

    assertEquals("a < b", w.children(3).subProg)
    assertEquals(1, w.children(3).ord)

    assertEquals(")", w.children(4).subProg)
    assertEquals(-1, w.children(4).ord)
  }

  @Test def JsonifyObjectInit() : Unit = {
    val prog = JSParser.deconstruct("({a : x, b : 0})")
    val w = ASTNodeJson.construct(prog)

    assertEquals(0,w.ord)
    assertEquals("{'a' : x,'b' : 0}",w.subProg)
    assertEquals(9, w.children.length)

    assertEquals("{", w.children(0).subProg)
    assertEquals("'a'", w.children(1).subProg)
    assertEquals(" : ", w.children(2).subProg)
    assertEquals("x", w.children(3).subProg)
    assertEquals(",", w.children(4).subProg)
    assertEquals("'b'", w.children(5).subProg)
    assertEquals(" : ", w.children(6).subProg)
    assertEquals("0", w.children(7).subProg)
    assertEquals("}", w.children(8).subProg)

    assertEquals(-1, w.children(0).ord)
    assertEquals(0, w.children(1).ord)
    assertEquals(-1, w.children(2).ord)
    assertEquals(1, w.children(3).ord)
    assertEquals(-1, w.children(4).ord)
    assertEquals(2, w.children(5).ord)
    assertEquals(-1, w.children(6).ord)
    assertEquals(3, w.children(7).ord)
    assertEquals(-1, w.children(8).ord)
  }

  @Test def JsonifyArrayInit() : Unit = {
    val prog = JSParser.deconstruct("[x,y,12]")
    val w = ASTNodeJson.construct(prog)
    assertEquals(0, w.ord)
    assertEquals("[x,y,12]", w.subProg)
    assertEquals(7, w.children.length)

    assertEquals("[",w.children(0).subProg)
    assertEquals("x",w.children(1).subProg)
    assertEquals(",",w.children(2).subProg)
    assertEquals("y",w.children(3).subProg)
    assertEquals(",",w.children(4).subProg)
    assertEquals("12",w.children(5).subProg)
    assertEquals("]",w.children(6).subProg)

    assertEquals(-1, w.children(0).ord)
    assertEquals(0, w.children(1).ord)
    assertEquals(-1, w.children(2).ord)
    assertEquals(1, w.children(3).ord)
    assertEquals(-1, w.children(4).ord)
    assertEquals(2, w.children(5).ord)
    assertEquals(-1, w.children(6).ord)
  }

  @Test def jsonifyOptionalArgs() : Unit = {
    val prog = ASTNode("foo(?,??)")
    prog.addChild(0,JSParser.deconstruct("1+2*3"))
    assertEquals(2,prog.arity)
    assertEquals(1,prog.optionalArgs)
    val w = ASTNodeJson.construct(prog)

    assertEquals(0, w.ord)
    assertEquals("foo(1 + (2 * 3))",w.subProg)
    assertEquals(3, w.children.length)

    assertEquals("foo(", w.children(0).subProg)
    assertEquals(-1, w.children(0).ord)
    assertTrue(w.children(0).children.isEmpty)

    assertEquals("1 + (2 * 3)", w.children(1).subProg)
    assertEquals(0, w.children(1).ord)
    assertEquals(3, w.children(1).children.length)

    assertEquals(")", w.children(2).subProg)
    assertEquals(-1, w.children(2).ord)
    assertTrue(w.children(2).children.isEmpty)
  }
  @Test def jsonifyAggregateOptionalArgs() : Unit = {
    AggregatorMethod.count = 0
    val prog = AggregatorMethod("map", 2, 2, 0, 0, 1)
    assertEquals(List("x1","x2"), prog.innerVar)
    prog.addChild(0,Literal("input"))
    prog.addChild(1, JSParser.deconstruct("x1 + 1"))
    prog.addChild(2,Literal("[]"))
    assertEquals("input.map(x1 => x1 + 1,[])", prog.toCode)

    val w = ASTNodeJson.construct(prog)

    assertEquals("input.map(x1 => x1 + 1,[])", w.subProg)
  }
}
