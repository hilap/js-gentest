import org.junit.Test
import org.scalatest.junit.JUnitSuite
import org.junit.Assert._
import DebugInfoJson._

class DebugInfoJSonTests   extends JUnitSuite{

  @Test def jsonifyExecLiteral() : Unit = {
    val p = Literal("6")
    val d = DebugInfoJson.accumulate(p, List("null"))
    assertTrue(d.isInstanceOf[DebugInfo])

    assertEquals(1, d.current.contextResults.length)
    assertEquals(List("input"),d.current.vars)
    assertEquals(List("null"), d.current.contextResults.head.inputVals)
    assertEquals("6", d.current.contextResults.head.output)
    assertTrue(d.children.isEmpty)
  }

  @Test def jsonifyExecIdentifier() : Unit = {
    val p = Literal("input")
    val d = DebugInfoJson.accumulate(p, List("6", "[]"))

    assertEquals(2, d.current.contextResults.length)
    assertEquals(List("input"),d.current.vars)
    assertEquals(ContextResult(List("6"),"6"),d.current.contextResults(0))
    assertEquals(ContextResult(List("[]"),"[]"),d.current.contextResults(1))
    assertTrue(d.children.isEmpty)
  }

  @Test def jsonifyBinOp() : Unit = {
    val p = BinOperator("+")
    p.addChild(0,Literal("input"))
    p.addChild(1,Literal("1"))
    val d = DebugInfoJson.accumulate(p,List("6","[]"))

    assertEquals(2,d.current.contextResults.length)
    assertEquals(List("input"), d.current.vars)
    assertEquals(ContextResult(List("6"),"7"),d.current.contextResults(0))
    assertEquals(ContextResult(List("[]"),"'1'"),d.current.contextResults(1))
    assertEquals(2, d.children.length)

    assertEquals(2, d.children(0).current.contextResults.length)
    assertEquals(List("input"), d.children(0).current.vars)
    assertEquals(ContextResult(List("6"),"6"),d.children(0).current.contextResults(0))
    assertEquals(ContextResult(List("[]"),"[]"),d.children(0).current.contextResults(1))
    assertEquals(0,d.children(0).children.length)

    assertEquals(2, d.children(1).current.contextResults.length)
    assertEquals(List("input"), d.children(1).current.vars)
    assertEquals(ContextResult(List("6"),"1"),d.children(1).current.contextResults(0))
    assertEquals(ContextResult(List("[]"),"1"),d.children(1).current.contextResults(1))
    assertEquals(0,d.children(1).children.length)
  }

  @Test def jsonifyExecLambda() : Unit = {
    val p = JSParser.deconstruct("input.filter((e,i) => input.indexOf(e) == i)")
    val d = DebugInfoJson.accumulate(p,List("[1,2,3]","[1,3,1]","[1,1,1]"))

    assertEquals(List("input"), d.current.vars)
    assertEquals(3,d.current.contextResults.length)
    assertEquals(ContextResult(List("[1,2,3]"),"[1,2,3]"), d.current.contextResults(0))
    assertEquals(ContextResult(List("[1,3,1]"), "[1,3]"), d.current.contextResults(1))
    assertEquals(ContextResult(List("[1,1,1]"),"[1]"), d.current.contextResults(2))
    assertEquals(2,d.children.length)

    assertEquals(3, d.children(0).current.contextResults.length)
    assertEquals(List("input"), d.children(0).current.vars)
    assertEquals(ContextResult(List("[1,2,3]"), "[1,2,3]"), d.children(0).current.contextResults(0))
    assertEquals(ContextResult(List("[1,3,1]"),"[1,3,1]"), d.children(0).current.contextResults(1))
    assertEquals(ContextResult(List("[1,1,1]"),"[1,1,1]"), d.children(0).current.contextResults(2))
    assertTrue(d.children(0).children.isEmpty)

    assertEquals(9, d.children(1).current.contextResults.length)
    assertEquals(List("input","e","i"), d.children(1).current.vars)
    assertEquals(ContextResult(List("[1,2,3]","1","0"),"true"),d.children(1).current.contextResults(0))
    assertEquals(ContextResult(List("[1,2,3]","2","1"),"true"),d.children(1).current.contextResults(1))
    assertEquals(ContextResult(List("[1,2,3]","3","2"),"true"),d.children(1).current.contextResults(2))
    assertEquals(ContextResult(List("[1,3,1]","1","0"),"true"),d.children(1).current.contextResults(3))
    assertEquals(ContextResult(List("[1,3,1]","3","1"),"true"),d.children(1).current.contextResults(4))
    assertEquals(ContextResult(List("[1,3,1]","1","2"),"false"),d.children(1).current.contextResults(5))
    assertEquals(ContextResult(List("[1,1,1]","1","0"),"true"),d.children(1).current.contextResults(6))
    assertEquals(ContextResult(List("[1,1,1]","1","1"),"false"),d.children(1).current.contextResults(7))
    assertEquals(ContextResult(List("[1,1,1]","1","2"),"false"),d.children(1).current.contextResults(8))

    assertEquals(2, d.children(1).children.length)
    assertEquals(9, d.children(1).children(1).current.contextResults.length)
    assertEquals(List("input","e","i"), d.children(1).children(1).current.vars)
    assertEquals(ContextResult(List("[1,2,3]","1","0"),"0"),d.children(1).children(1).current.contextResults(0))
    assertEquals(ContextResult(List("[1,2,3]","2","1"),"1"),d.children(1).children(1).current.contextResults(1))
    assertEquals(ContextResult(List("[1,2,3]","3","2"),"2"),d.children(1).children(1).current.contextResults(2))
    assertEquals(ContextResult(List("[1,3,1]","1","0"),"0"),d.children(1).children(1).current.contextResults(3))
    assertEquals(ContextResult(List("[1,3,1]","3","1"),"1"),d.children(1).children(1).current.contextResults(4))
    assertEquals(ContextResult(List("[1,3,1]","1","2"),"2"),d.children(1).children(1).current.contextResults(5))
    assertEquals(ContextResult(List("[1,1,1]","1","0"),"0"),d.children(1).children(1).current.contextResults(6))
    assertEquals(ContextResult(List("[1,1,1]","1","1"),"1"),d.children(1).children(1).current.contextResults(7))
    assertEquals(ContextResult(List("[1,1,1]","1","2"),"2"),d.children(1).children(1).current.contextResults(8))
  }

  @Test def jsonifyLambdaInLambdaExec() : Unit = {
    val p = JSParser.deconstruct("input.map(e => e.reduce((x,y) => x+y,0))")
    val d = DebugInfoJson.accumulate(p,List("[[1,2,3],[2,2,2],[1,2,3]]"))

    assertEquals(1,d.current.contextResults.length)
    assertEquals(List("input"), d.current.vars)
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]"),"[6,6,6]"), d.current.contextResults.head)

    assertEquals(3, d.children(1).current.contextResults.length)
    assertEquals(List("input","e"),d.children(1).current.vars)
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]"),"6"),d.children(1).current.contextResults(0))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[2,2,2]"),"6"),d.children(1).current.contextResults(1))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]"),"6"),d.children(1).current.contextResults(2))

    assertEquals(3, d.children(1).children.length)
    assertEquals(3, d.children(1).children(2).current.contextResults.length)
    assertEquals(List("input","e"), d.children(1).children(2).current.vars)

    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]"),"0"),d.children(1).children(2).current.contextResults(0))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[2,2,2]"),"0"),d.children(1).children(2).current.contextResults(1))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]"),"0"),d.children(1).children(2).current.contextResults(2))

    assertEquals(9, d.children(1).children(1).current.contextResults.length)
    assertEquals(List("input","e","x","y"),d.children(1).children(1).current.vars)
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]","0","1"),"1"),d.children(1).children(1).current.contextResults(0))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]","1","2"),"3"),d.children(1).children(1).current.contextResults(1))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]","3","3"),"6"),d.children(1).children(1).current.contextResults(2))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[2,2,2]","0","2"),"2"),d.children(1).children(1).current.contextResults(3))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[2,2,2]","2","2"),"4"),d.children(1).children(1).current.contextResults(4))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[2,2,2]","4","2"),"6"),d.children(1).children(1).current.contextResults(5))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]","0","1"),"1"),d.children(1).children(1).current.contextResults(6))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]","1","2"),"3"),d.children(1).children(1).current.contextResults(7))
    assertEquals(ContextResult(List("[[1,2,3],[2,2,2],[1,2,3]]","[1,2,3]","3","3"),"6"),d.children(1).children(1).current.contextResults(8))
  }
  @Test def jsonifyErrorExec() : Unit = {
    val p = JSParser.deconstruct("input.foo.bar")
    val d = DebugInfoJson.accumulate(p,List("[1,2,3]"))

    assertEquals(1,d.current.contextResults.length)
    assertEquals(List("input"), d.current.vars)
    assertEquals(ContextResult(List("[1,2,3]"),"ERROR"), d.current.contextResults.head)
    assertEquals(1, d.children.length)
    assertEquals(1,d.children.head.current.contextResults.length)
    assertEquals(List("input"), d.children.head.current.vars)
    assertEquals(ContextResult(List("[1,2,3]"),"undefined"),d.children.head.current.contextResults.head)
    assertEquals(1, d.children.head.children.length)
    assertEquals(1, d.children.head.children.head.current.contextResults.length)
    assertEquals(List("input"),d.children.head.children.head.current.vars)
    assertEquals(ContextResult(List("[1,2,3]"),"[1,2,3]"), d.children.head.children.head.current.contextResults.head)
    assertEquals(0, d.children.head.children.head.children.length)
  }
  @Test def jsonifyExecLambdaLhsError() : Unit = {
    val p = JSParser.deconstruct("input.err().map(x => x + 1)")
    val d = DebugInfoJson.accumulate(p,List("[1,2,3]"))

    assertEquals(1, d.current.contextResults.length)
    assertEquals(List("input"),d.current.vars)
    assertEquals(ContextResult(List("[1,2,3]"),"ERROR"), d.current.contextResults.head)

    assertEquals(2, d.children.length)
    assertEquals(1,d.children(1).current.contextResults.length)
    assertEquals(List("input","x"),d.children(1).current.vars)
    assertEquals(ContextResult(List("[1,2,3]","undefined"),"NaN"),d.children(1).current.contextResults.head)
    assertEquals(2, d.children(1).children.length)
  }
  @Test def jsonifyExecOptionalParams() : Unit = {
    val p = ASTNode("isNaN(?,??)")
    assertEquals(2, p.arity)
    assertEquals(1, p.optionalArgs)
    p.addChild(0,Literal("input"))

    val d = DebugInfoJson.accumulate(p,List("6", "NaN"))

    assertEquals(2, d.current.contextResults.length)
    assertEquals(List("input"),d.current.vars)
    assertEquals(ContextResult(List("6"),"false"), d.current.contextResults(0))
    assertEquals(ContextResult(List("NaN"),"true"), d.current.contextResults(1))
    assertEquals(1, d.children.length)
    assertEquals(2, d.children(0).current.contextResults.length)
    assertEquals(List("input"),d.children(0).current.vars)
    assertEquals(List(ContextResult(List("6"),"6"),ContextResult(List("NaN"),"NaN")), d.children(0).current.contextResults)
  }

  @Test def jsonifyExecStringValue() : Unit = {
    val p = Literal("'abc'")
    val d = DebugInfoJson.accumulate(p, List("null"))

    assertEquals(List("input"),d.current.vars)
    assertEquals(1,d.current.contextResults.length)
    assertEquals("'abc'",d.current.contextResults.head.output)
  }

  @Test def jsonifyExecStringFromInput() : Unit = {
    val p = Literal("input")
    val d = DebugInfoJson.accumulate(p, List("'6'"))

    assertEquals(List("input"),d.current.vars)
    assertEquals(1,d.current.contextResults.length)
    assertEquals("'6'",d.current.contextResults.head.output)
  }

  @Test def errorInsideLambda() : Unit = {
    val p = JSParser.deconstruct("input.reduce((x,y) => x.indexOf(y) != (-1) ? x : x.concat([y]))")
    val d = DebugInfoJson.accumulate(p,List("[1,3,1]","[5,7,5,9,5]"))

    assertEquals(2,d.children.length)
    assertEquals(List("input","x","y"),d.children(1).current.vars)
    assertEquals(List(ContextResult(List("[1,3,1]","1","3"),"ERROR"),
                      ContextResult(List("[1,3,1]","undefined","1"),"ERROR"),
                      ContextResult(List("[5,7,5,9,5]","5","7"),"ERROR"),
                      ContextResult(List("[5,7,5,9,5]","undefined","5"),"ERROR"),
                      ContextResult(List("[5,7,5,9,5]","undefined","9"),"ERROR"),
                      ContextResult(List("[5,7,5,9,5]","undefined","5"),"ERROR")
                  ), d.children(1).current.contextResults)

  }
}
