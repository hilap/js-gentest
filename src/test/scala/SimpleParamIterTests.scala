import org.junit.Test
import org.scalatest.junit.JUnitSuite
import org.junit.Assert._

class SimpleParamIterTests extends JUnitSuite{

  @Test def enumParams() : Unit = {
    val prevList = List(Enumerator.SeenProgram(Literal("1"),Set()), Enumerator.SeenProgram(JSParser.deconstruct("1 + 2"), Set()))
    val pi = new SimpleParamsIter(List(prevList, prevList, prevList),0, 2,List(Set(),Set(),Set()))
    assertTrue(pi.hasNext)
    assertEquals("1,1,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1 + 2,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1 + 2,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1 + 2,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1 + 2,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertFalse(pi.hasNext)
  }

  @Test def enumParamsWithTypeConstraints() : Unit = {
    val prevList = List(
      Enumerator.SeenProgram(Literal("1"),Set(DataType.number)),
      Enumerator.SeenProgram(Literal("{}"),Set(DataType.obj)),
      Enumerator.SeenProgram(JSParser.deconstruct("1 + 2"), Set(DataType.number))
    )
    val pi = new SimpleParamsIter(List(prevList, prevList, prevList),0, 2, List(Set(),Set(DataType.obj,DataType.array),Set(DataType.array)))
    assertTrue(pi.hasNext)

    assertEquals("1,1,1 + 2", pi.next.map(_.toCode).mkString(","))
    //assertEquals("1,{},1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1 + 2,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1 + 2,{}", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1 + 2,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("{},1,1 + 2", pi.next.map(_.toCode).mkString(","))
    //assertEquals("{},{},1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("{},1 + 2,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("{},1 + 2,{}", pi.next.map(_.toCode).mkString(","))
    assertEquals("{},1 + 2,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1,{}", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1,1 + 2", pi.next.map(_.toCode).mkString(","))
    //assertEquals("1 + 2,{},1", pi.next.map(_.toCode).mkString(","))
    //assertEquals("1 + 2,{},{}", pi.next.map(_.toCode).mkString(","))
    //assertEquals("1 + 2,{},1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1 + 2,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1 + 2,{}", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1 + 2,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertFalse(pi.hasNext)
  }

  @Test def enumParamsWithTypeConstraintsOptionalParams() : Unit = {
    val prevList = List(
      Enumerator.SeenProgram(Literal("1"),Set(DataType.number)),
      Enumerator.SeenProgram(Literal("{}"),Set(DataType.obj)),
      Enumerator.SeenProgram(JSParser.deconstruct("1 + 2"), Set(DataType.number))
    )
    val pi = new SimpleParamsIter(List(prevList, prevList, prevList),2, 2, List(Set(),Set(DataType.obj,DataType.array),Set(DataType.array)))
    assertTrue(pi.hasNext)

    assertEquals("1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("{},1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1", pi.next.map(_.toCode).mkString(","))
    //assertEquals("1 + 2,{}", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1,1 + 2", pi.next.map(_.toCode).mkString(","))
    //assertEquals("1,{},1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1 + 2,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1 + 2,{}", pi.next.map(_.toCode).mkString(","))
    assertEquals("1,1 + 2,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("{},1,1 + 2", pi.next.map(_.toCode).mkString(","))
    //assertEquals("{},{},1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("{},1 + 2,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("{},1 + 2,{}", pi.next.map(_.toCode).mkString(","))
    assertEquals("{},1 + 2,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1,{}", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1,1 + 2", pi.next.map(_.toCode).mkString(","))
    //assertEquals("1 + 2,{},1", pi.next.map(_.toCode).mkString(","))
    //assertEquals("1 + 2,{},{}", pi.next.map(_.toCode).mkString(","))
    //assertEquals("1 + 2,{},1 + 2", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1 + 2,1", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1 + 2,{}", pi.next.map(_.toCode).mkString(","))
    assertEquals("1 + 2,1 + 2,1 + 2", pi.next.map(_.toCode).mkString(","))
    assertFalse(pi.hasNext)

  }

}
