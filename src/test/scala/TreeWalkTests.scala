import org.junit.Test
import org.scalatest.junit.JUnitSuite
import org.junit.Assert._

class TreeWalkTests extends JUnitSuite{

  @Test def treeWalkLiteral() : Unit = {
    val lit = Literal("'a'")
    val walk : Iterable[ASTNode] = lit.walkAllNodes()
    assertEquals(1,walk.size)
    assertEquals(lit,walk.head)
  }

  @Test def treeWalkNode() : Unit = {
    val node = ASTNode("isNaN(?,?)")
    val walk = node.walkAllNodes()
    assertFalse(walk.isEmpty)
    assertEquals(1, walk.size)
    assertEquals(walk.head, node)

    node.addChild(0, ASTNode("foo(?)"))
    node.addChild(1, Literal("2"))
    val walk2 = node.walkAllNodes()

    assertEquals(3, walk2.size)
    assertEquals(node.children(0).get, walk2.head)
    assertEquals("2",walk2.drop(1).head.func)
    assertEquals(node, walk2.last)
  }


  @Test def treeWalkMethodCall() : Unit = {
    val meth = new MethodCall("stringify",1)
    meth.addChild(0,ASTNode("foo(?)"))
    meth.addChild(1,ASTNode("bar(?)"))
    val walk = meth.walkAllNodes()
    assertEquals(3,walk.size)
    assertEquals(meth.lhs.get,walk.head)
    assertEquals(meth.children(1).get,walk.drop(1).head)
    assertEquals(meth,walk.last)

  }

  @Test def treeWalkArrayDeref() : Unit = {
    val arr = ArrayDeref()
    arr.addChild(0,ASTNode("foo(?)"))
    arr.addChild(1,ASTNode("bar(?)"))
    val walk = arr.walkAllNodes()
    assertEquals(3,walk.size)
    assertEquals(arr.lhs.get,walk.head)
    assertEquals(arr.rhs.get,walk.drop(1).head)
    assertEquals(arr,walk.last)
  }

  @Test def treeWalkPropCall() : Unit = {
    val prop = PropCall("foo")
    prop.addChild(0,ASTNode("bar(?)"))
    val walk = prop.walkAllNodes()
    assertEquals(2,walk.size)
    assertEquals(prop.lhs.get,walk.head)
    assertEquals(prop,walk.last)
  }

  @Test def treeWalkAggregatorMethodp() : Unit = {
    val agg = AggregatorMethod("map",1,1)
    val lhs = ASTNode("foo(?)")
    lhs.addChild(0,ASTNode("bar(?)"))
    lhs.children(0).get.addChild(0,Literal("a"))
    agg.addChild(0,lhs)
    agg.addChild(1,ASTNode("ni(?)"))
    val walk = agg.walkAllNodes()
    assertEquals(5,walk.size)
    assertEquals("a",walk.head.func)
    assertEquals("ni",walk.drop(1).head.func)
    assertEquals("bar",walk.drop(2).head.func)
    assertEquals("foo",walk.drop(3).head.func)
    assertEquals(agg,walk.last)
  }

  @Test def treeWalkBottomUpLeftToRight() : Unit = {
    val expr = JSParser.deconstruct("[5, 7, 5, 9, 7].sort().filter((e,i) => i %2 == 1).reduce((x,y) => x.toString())")
    val walk = expr.walkAllNodes()
    assertEquals(11,walk.size)
    assertEquals("[5,7,5,9,7]", walk.head.func)
    assertEquals("i",walk.drop(1).head.func)
    assertEquals("2", walk.drop(2).head.func)
    assertEquals("1", walk.drop(3).head.func)
    assertEquals("x", walk.drop(4).head.func)
    assertEquals("sort", walk.drop(5).head.func)
    assertEquals("%",walk.drop(6).head.func)
    assertEquals("toString",walk.drop(7).head.func)
    assertEquals("==",walk.drop(8).head.func)
    assertEquals("filter",walk.drop(9).head.func)
    assertEquals("reduce",walk.drop(10).head.func)
  }

}
